//
//  ModuleCellController.swift
//  6one5Manager
//
//  Created by Sumit More on 12/19/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation
import UIKit

class ModuleCellController: UICollectionViewCell {
    @IBOutlet weak var modelCellImageView: UIImageView!
    @IBOutlet weak var moduleCellHeadingLabel: UILabel!
    @IBOutlet weak var moduleRating: CosmosView!
}
