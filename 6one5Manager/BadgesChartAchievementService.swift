//
//  BadgesChartAchievementService.swift
//  6one5Manager
//
//  Created by enyotalearning on 03/12/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation
class BadgesChartAchivementService
{
    //using Singleton pattern for single object handling...
    class var sharedInstance: BadgesChartAchivementService
    {
        //2
        struct Singleton
        {
            //3
            static let instance = BadgesChartAchivementService()
        }
        //4
        return Singleton.instance
    }
    
    func loadData(completionHandler:@escaping (BadgesChartAchievementData)->())
    {
        let headers = [
            "cache-control": "no-cache",
            ]
        let login = Login.sharedInstance
        let apiURL: String =  WebAPI.GETACHIVEMENTBADGESLIST + login.getManagerId()
        let request = NSMutableURLRequest(url: NSURL(string: apiURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("Error: \(error)")
                let badgesChartAchievementData = BadgesChartAchievementData.sharedInstance
                completionHandler(badgesChartAchievementData)
            } else
            {
                do
                {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                    {
                        let badgesChartAchievementData = BadgesChartAchievementData.sharedInstance
                        
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        if statusVal == nil
                        {
                            print("status val is null")
                            let statusVal = convertedJsonIntoDict["status"] as? String
                            print("status valule is \(statusVal)")
                            let login = Login.sharedInstance
                            login.status = String(describing: convertedJsonIntoDict["status"] as? Bool)
                            login.message = convertedJsonIntoDict["message"] as? String
                            
                            
                        }
                        else
                        {
                            
                            if(statusVal)!
                            {
                                let json = JSON(convertedJsonIntoDict)
                                var badgesAchievmentData:BadgesAchievmentData
                                for item in json["data"].arrayValue
                                {
                                    badgesAchievmentData = BadgesAchievmentData()
                                    
                                    if item["id"].exists()
                                    {
                                        badgesAchievmentData.id = item["id"].stringValue
                                    }
                                    if item["cnt"].exists()
                                    {
                                        badgesAchievmentData.cnt = item["cnt"].stringValue
                                    }
                                    
                                    badgesChartAchievementData.addBadgesAchievmentArray(badgeAchievmentData: badgesAchievmentData)
                                }
                                
                            }//end of if
                        }
                        completionHandler(badgesChartAchievementData)
                    }
                    
                } catch let error as NSError
                {
                    print(error)
                }
                
                
            }
            
        })
        
        dataTask.resume()
        
    }
    
    
    
}
