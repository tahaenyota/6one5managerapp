//
//  AddNotesVC.swift
//  6one5Manager
//
//  Created by enyotalearning on 30/11/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit

class AddNotesVC: UIViewController, UITextViewDelegate  {
    
    @IBOutlet weak var NotesTextField: UITextView!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    
    var tempNoteText:String!
    var notesText = [String]()
    var repId = [String]()
    
    let learnerDetails = LearnerDetails.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationItem.setHidesBackButton(true, animated: false)
        let borderColor : UIColor = UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 1.0)
        NotesTextField.layer.borderWidth = 0.5
        NotesTextField.layer.borderColor = borderColor.cgColor
        NotesTextField.layer.cornerRadius = 5.0
        
        
        NotesTextField.delegate = self
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        NotesTextField.setContentOffset(CGPoint.zero, animated:false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        notesText = [String]()
        repId = [String]()
        getNoteForRepresentative(representativeId: learnerDetails.selectedLearnerId)
        
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func cancelBtnTapped(_ sender: AnyObject)
    {
        _ = navigationController?.popViewController(animated: true)
        //NotesTextField.text = tempNoteText
    }
    
    @IBAction func saveBtnTapped(_ sender: AnyObject)
    {
        NotesTextField.resignFirstResponder()
        tempNoteText = NotesTextField.text
        NotesTextField.text = tempNoteText
        
        submitNotes(representativeId: learnerDetails.selectedLearnerId)
    }
    
    @IBAction func DismissKeyboard(sender: AnyObject) {
        NotesTextField.resignFirstResponder()
    }
    
    func getNoteForRepresentative(representativeId:String)
    {
        let progressHUD = ProgressHUD()
        self.view.addSubview(progressHUD)
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            ]
        
        let apiURL: String = "http://key2train.in/admin/api/v1/get_notes/" + representativeId
        
        let request = NSMutableURLRequest(url: NSURL(string: apiURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("Network Error: \(error)")
                let alert = UIAlertController(title: "addnotes", message: "errordetails".localized() + (error?.localizedDescription)!, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "ok".localized(), style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
                
            } else {
                
                
                let httpResponse = response as? HTTPURLResponse
                print("HTTP Response: \(httpResponse)")
                
                do {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        print("Print converted dictionary ",convertedJsonIntoDict)
                        
                        // Get value by key
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        print(statusVal!)
                        DispatchQueue.main.async()
                            {
                                if(statusVal! == true)
                                {
                                    
                                    
                                    let json = JSON(convertedJsonIntoDict)
                                    print("Json Data \(json["data"].arrayValue.count)")
                                    for item in json["data"].arrayValue //for item in json["data"].arrayValue
                                    {
                                        self.notesText.append(item["notes"].stringValue)
                                        self.repId.append(item["id"].stringValue)
                                        print("Text added to array \(item["notes"].stringValue)")
                                    }
                                    progressHUD.hide()
                                    if self.notesText.count > 0
                                    {
                                        self.tempNoteText = self.notesText[0]
                                        self.NotesTextField.text = self.tempNoteText
                                    }
                                    progressHUD.hide()
                                    UIApplication.shared.endIgnoringInteractionEvents()
                                    
                                } else {
                                    var error_msg:NSString
                                    if convertedJsonIntoDict["message"] as? NSString != nil
                                    {
                                        error_msg = convertedJsonIntoDict["message"] as! NSString
                                    } else {
                                        error_msg = "Unknown Error"
                                    }
                                    print("error_msg",error_msg)
                                    
                                    
                                    progressHUD.hide()
                                    UIApplication.shared.endIgnoringInteractionEvents()
                                    
                                }
                        }
                        
                        
                    }
                } catch let error as NSError {
                    print(error)
                    
                    progressHUD.hide()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    
                    
                }
                
                
            }
        })
        
        dataTask.resume()
    }
    
    
    func submitNotes(representativeId:String)
    {
        let progressHUD = ProgressHUD()
        self.view.addSubview(progressHUD)
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        //course_id = 2
        
        let headers = [
            "cache-control": "no-cache"
        ]
        
        let login = Login.sharedInstance
        
        let representative_id = representativeId
        let manager_id = login.getManagerId()
        let notes:String = NotesTextField.text
        
        let dataStr: String!
        
        dataStr = "data={ \"representative_id\":\"\(representative_id as NSString)\",\"manager_id\":\"\(String(manager_id) as NSString)\",\"notes\":\"\(String(notes) as NSString)\"}"
        
        print("Data string is \(dataStr)")
        
        let postData = NSMutableData(data: dataStr.data(using: String.Encoding.utf8)!)
        
        let apiString = "http://key2train.in/admin/api/v1/add_notes/" + representativeId //Constants.managerSubmitRatingURL + manager_id
        let request = NSMutableURLRequest(url: NSURL(string: apiString)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            //print("Magic Data is  \(data!)")
            if (error != nil) {
                print("Error: \(error)")
                let alertController = UIAlertController(title: "addnotes".localized(), message: "connectionerror".localized() , preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "ok".localized(), style: .default) { (action:UIAlertAction!) in
                    self.navigationController?.popViewController(animated: true)
                    
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion:nil)
                
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
                
                
            } else {
                let httpResponse = response as? HTTPURLResponse
                print("Response: \(httpResponse)")
                do {
                    
                    //print("Magic Data is  \(data!)")
                    
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        print("Print converted dictionary",convertedJsonIntoDict)
                        
                        // Get value by key
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        print("Status: \(statusVal!)")
                        DispatchQueue.main.async()
                            {
                                
                                progressHUD.hide()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                if(statusVal! == true)
                                {
                                    let alertController = UIAlertController(title: "addnotes", message: "notessubmission".localized() , preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: "ok".localized(), style: .default) { (action:UIAlertAction!) in
                                        alertController.dismiss(animated: true, completion: nil)
                                    }
                                    alertController.addAction(OKAction)
                                    self.present(alertController, animated: true, completion:nil)
                                    
                                    
                                } else {
                                    var error_msg:NSString
                                    if convertedJsonIntoDict["message"] as? NSString != nil {
                                        error_msg = convertedJsonIntoDict["message"] as! NSString
                                    } else {
                                        error_msg = "Unknown Error"
                                    }
                                    print("error_msg",error_msg)
                                    let alertController = UIAlertController(title: "addnotes".localized(), message: "unresponsiveserver".localized() , preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: "ok".localized(), style: .default) { (action:UIAlertAction!) in
                                        alertController.dismiss(animated: true, completion: nil)
                                    }
                                    alertController.addAction(OKAction)
                                    self.present(alertController, animated: true, completion:nil)
                                    
                                    
                                    
                                }
                                
                        }
                    }
                } catch let error as NSError {
                    print(error)
                    let alertController = UIAlertController(title: "addnotes".localized(), message: "unresponsiveserver".localized() , preferredStyle: .alert)
                    let OKAction = UIAlertAction(title: "ok".localized(), style: .default) { (action:UIAlertAction!) in
                        self.navigationController?.isNavigationBarHidden = false
                    }
                    alertController.addAction(OKAction)
                    self.present(alertController, animated: true, completion:nil)
                    
                    progressHUD.hide()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    
                    
                }
                
                
                
                print("Data: \(data)")
            }
        })
        
        dataTask.resume()
    }
    
    
}
