//
//  FAQDetailView.swift
//  LearningApp
//
//  Created by Sumit More on 12/30/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import Foundation
import UIKit

class FAQDetailView: UITableViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var detailBGView: UIView!
    @IBOutlet var detailLabel: UILabel!
    // override func layoutSubviews() {
    //        super.layoutSubviews()
    //        reload()
    //    }
    //    func reload() {
    //        if isSelected {
    //            contentView.backgroundColor = .red
    //        } else {
    //            contentView.backgroundColor = .white
    //        }
    //    }
}
