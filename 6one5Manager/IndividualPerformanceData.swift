//
//  IndividualPerformanceData.swift
//  6one5Manager
//
//  Created by enyotalearning on 01/10/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation

public class IndividualPerformanceData: NSObject
{
    var quizDataPoints = [String]()
    var reviewDataPoints = [String]()
    var moduleStatusPoints = [String]()
    
    var quizDataValues = [Double]()
    var reviewDataValues = [Double]()
    var moduleStatusValues = [Double]()
    
    
    /*class var sharedInstance: IndividualPerformanceData
     {
     //2
     struct Singleton {
     //3
     static let instance = IndividualPerformanceData()
     }
     //4
     return Singleton.instance
     }*/
    
    func addQuizDataPoints(dataPoint:String)
    {
        quizDataPoints.append(dataPoint)
    }
    
    func addQuizDataValues(dataValue:Double)
    {
        quizDataValues.append(dataValue)
    }
    
    func addReviewDataPoints(dataPoint:String)
    {
        reviewDataPoints.append(dataPoint)
    }
    
    func addReviewDataValues(dataValue:Double)
    {
        reviewDataValues.append(dataValue)
    }
    
    func addModuleStatusPoints(dataPoint:String)
    {
        moduleStatusPoints.append(dataPoint)
    }
    
    func addModuleStatusPoints(dataValue:Double)
    {
        moduleStatusValues.append(dataValue)
    }
    
    func getQuizDataCount() -> Int
    {
        return quizDataPoints.count
    }
    
}
