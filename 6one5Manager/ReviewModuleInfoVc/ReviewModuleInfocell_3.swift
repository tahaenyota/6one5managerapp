//
//  ReviewModuleInfocell_3.swift
//  6one5Manager
//
//  Created by enyotalearning on 22/12/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit

class ReviewModuleInfocell_3: UITableViewCell {
    @IBOutlet weak var descriptorLabel3: UILabel!
    @IBOutlet weak var moduleDescription3: UILabel!
    @IBOutlet weak var descriptoTitleLabel3: UILabel!
    @IBOutlet weak var cell3NextButton: UIButton!
    @IBOutlet weak var cell3PreviousButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
