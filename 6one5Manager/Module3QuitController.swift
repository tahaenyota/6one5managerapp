//
//  Module3QuitController.swift
//  6one5Manager
//
//  Created by Sumit More on 12/21/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation

class Module3QuitController: UIViewController {
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("In QuitModule")
        
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        //        self.performSegueWithIdentifier("goto_modules", sender: self)
        self.parent?.dismiss(animated: true, completion: {
            self.presentingViewController?.dismiss(animated: true, completion: nil)
        })
    }
}

