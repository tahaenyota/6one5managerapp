//
//  ReviewNotification.swift
//  6one5Manager
//
//  Created by enyotalearning on 03/10/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit
import Localize_Swift

class ReviewNotification: UIViewController , UITableViewDelegate,UITableViewDataSource
{
    var progressHUD = ProgressHUD()
    
    @IBOutlet weak var tableView: UITableView!
    var selectedIndex:Int!
    var notificationData = NotificationData.sharedInstance
    let learnerDetails =  LearnerDetails.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //[[self navigationController] tabBarItem].badgeValue = @"YourBadgeValue";
        // self.navigationController?.tabBarItem.badgeValue = "90"
        let dateFormatter = DateFormatter()
        //if some one want 12AM,5PM then use "hh mm a"
        //if some one want 00AM,17PM then use "HH mm a"
        dateFormatter.dateFormat = "hh mm a"
        let stringDate = dateFormatter.string(from: NSDate() as Date)
        print(stringDate)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        loadLernerData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func getNotifs()
    {
        
        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            ]
        let login =  Login.sharedInstance
        let manager_id = login.getManagerId()
        
        let apiURL: String = WebAPI.USERNOTIFICATION + manager_id // Constants.managerGetNotifsURL + manager_id
        
        let request = NSMutableURLRequest(url: NSURL(string: apiURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        print ("Notification Count :\(apiURL)")
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("Error: \(error)")
                let alert = UIAlertController(title: "Notifications", message: "Error in connection. Please check your internet connection and try again.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                self.progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
            } else {
                let httpResponse = response as? HTTPURLResponse
                print("HTTP Response: \(httpResponse)")
                
                do {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        print("Notification result is :",convertedJsonIntoDict)
                        
                        // Get value by key
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        print(statusVal!)
                        
                        if(statusVal != nil)
                        {
                            
                            DispatchQueue.main.sync {
                                let notifs = convertedJsonIntoDict["count"] as? Int
                                
                                //self.notifCount = notifs
                                if notifs == nil{
                                    self.tabBarController?.tabBar.items![3].badgeValue = String("0")
                                }else{
                                    self.tabBarController?.tabBar.items![3].badgeValue = String(describing: notifs!)
                                }
                                
                                self.progressHUD.hide()
                                UIApplication.shared.endIgnoringInteractionEvents()
                            }
                            
                            
                            
                        } else {
                            var error_msg:NSString
                            if convertedJsonIntoDict["message"] as? NSString != nil {
                                error_msg = convertedJsonIntoDict["message"] as! NSString
                            } else {
                                error_msg = "Unknown Error"
                            }
                            print("error_msg",error_msg)
                            self.progressHUD.hide()
                            UIApplication.shared.endIgnoringInteractionEvents()
                        }
                    }
                } catch let error as NSError {
                    print(error)
                    self.progressHUD.hide()
                    UIApplication.shared.endIgnoringInteractionEvents()
                }
            }
            
        })
        
        dataTask.resume()
    }
    func loadLernerData()
    {
        let progressHUD = ProgressHUD()
        self.view.addSubview(progressHUD)
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        if (self.learnerDetails.getLearnerCount() >= 0 )
        {
            print("reviewNotification learner Data Reset ")
            
            learnerDetails.learnerProfileArray = [String]()
            learnerDetails.learnerId = [String]()
            learnerDetails.selectedLearner = -1
            learnerDetails.performanceData = [IndividualPerformanceData]()
            
        }
        
        LearnersDetailsService.sharedInstance.loadData(completionHandler: { (userData:LearnerDetails) -> () in
            if (self.learnerDetails.getLearnerCount() >= 0 )
            {
                DispatchQueue.main.async(execute:
                    {
                        print("Here is the Learner data loaded")
                        //self.imageView.image = image;
                }) //learner details service completed
                
            }// end if learnerDetails.getLearnerCount
            else
            {
                print("error While loading learner data")
            } // end else learnerDetails.getLearnerCount
            
        })
        
        
        if (self.notificationData.getreiviewDetailsCount() >= 0 )
        {
            //print("reviewNotificationData Data Reset ")
            //write clear logic here
            notificationData.notificationDetailsArray = [NotificationDetails]()
            //learnerDetails.learnerProfileArray = [String]()
        }
        
        NotificationService.sharedInstance.loadData(completionHandler: { (userData:NotificationData) -> () in
            if (self.notificationData.getreiviewDetailsCount() >= 0 )
            {
                let login = Login.sharedInstance
                
                
                if login.status != nil
                {
                    progressHUD.hide()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    let alertController = UIAlertController(title: "User status", message: login.message , preferredStyle: .alert)
                    let OKAction = UIAlertAction(title: "ok".localized(), style: .default) {
                        (action:UIAlertAction!) in
                        login.logoutFromDeviceCache()
                        self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
                    }
                    alertController.addAction(OKAction)
                    self.present(alertController, animated: true, completion:nil)
                }
                    
                    
                else
                {
                    
                    DispatchQueue.main.async()
                        {
                            progressHUD.hide()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            self.getNotifs()
                            self.tableView.reloadData()
                            
                            
                            
                    } //learner details service completed
                }
            }// end if learnerDetails.getLearnerCount
            else
            {
                print("Problem while loading data ")
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
                
            } // end else learnerDetails.getLearnerCount
            
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        //print(self.items.count)
        return notificationData.getreiviewDetailsCount() ;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell") as! ReviewNotificationCell
        
        cell.descriptionLabel.text = notificationData.getContent(index: indexPath.row)
        cell.name.text = notificationData.getName(index: indexPath.row)
        cell.name.textColor = UIColor.black
        cell.descriptionLabel.textColor = UIColor.darkGray
        cell.dateLabel.textColor = UIColor.darkGray
        
        let statusStr = notificationData.getNotificationStatus(index: indexPath.row)
        if statusStr == "1"{
            
            if (Constants.device == "iPhone") {
                cell.name.font = UIFont.systemFont(ofSize: 16.0)
            }else{
                cell.name.font = UIFont.systemFont(ofSize: 22.0)
            }
            
            
            
        }else{
            if (Constants.device == "iPhone") {
                cell.name.font = UIFont.boldSystemFont(ofSize: 16)
            }else{
                cell.name.font = UIFont.boldSystemFont(ofSize: 22)
            }
        }
        
        let strTime = notificationData.getDate(index: indexPath.row)
        print("Notif Date: \(strTime)")
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateVal: Date = formatter.date(from: strTime)!
        let date1 = Date()
        
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "yyyy-MM-dd"
        let currentDate = formatter1.string(from: date1)
        let fetchedDate = formatter1.string(from: dateVal)
        
        if currentDate == fetchedDate{
            let dateFormatter = DateFormatter()
            //if some one want 12AM,5PM then use "hh mm a"
            //if some one want 00AM,17PM then use "HH mm a"
            dateFormatter.dateFormat = "hh:mm a"
            let stringDate = dateFormatter.string(from:dateVal)
            print (stringDate)
            cell.dateLabel.text = stringDate
        } else {
            let strSplit = strTime.characters.split(separator: " ")
            cell.dateLabel.text = String(strSplit.first!)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle
    {
        if notificationData.getNotificationType(index: indexPath.row) == "3"
        {
            return UITableViewCellEditingStyle.none
        }
        else
        {
            return UITableViewCellEditingStyle.delete
        }
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        if editingStyle == .delete
        {
            print("Delete request found \(indexPath.row) ")
            
            
            let notificationId = notificationData.getId(index: indexPath.row)
            removeNotification(notificationId: notificationId)
            
            tableView.reloadData()
            //let status = removeNotification()
            //TODO: Here Manager asked to delete this notification
            // This is normal notification
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let indexPath = tableView.indexPathForSelectedRow //optional, to get from any UIButton for example
        
        //let currentCell = tableView.cellForRow(at: indexPath!)! as UITableViewCell
        
        selectedIndex = (indexPath?.row)!
        
        print("tableView cell selected now : \(selectedIndex)")
        
        
        let learnerDetails =  LearnerDetails.sharedInstance
        
        let lId:String = notificationData.getRepresentativeId(index: indexPath!.row)
        notificationData.setCureentUserId(id: lId)
        learnerDetails.setSelectedLearnerId(id: lId)
        learnerDetails.navigationPath = "2"
        learnerDetails.notificationIndex = indexPath!.row
        
        if notificationData.getNotificationType(index: indexPath!.row) == "3"
        {
            let notifObject = NotificationData.sharedInstance
            
            let notifDetailArray = notifObject.getNotificationDetails(index: (indexPath?.row)!)
            
            
            //            let popOverVC = UIStoryboard (name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ManagerAddReviewPopupScroll") as! ManagerFeedbackVC
            //           popOverVC.screenComingFrom = "notification"
            //            popOverVC.notificationModuleID = Int(notifDetailArray.module_id)!
            //            popOverVC.notificationCourseID = Int(notifDetailArray.course_id)!
            //            self.addChildViewController(popOverVC)
            //           // popOverVC.view.frae = self.view.frame
            //            self.view.addSubview(popOverVC.view)
            //            popOverVC.didMove(toParentViewController: self)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController:ManagerFeedbackVC = storyboard.instantiateViewController(withIdentifier: "ManagerFeedbackVC") as! ManagerFeedbackVC
            viewController.screenComingFrom = "notification"
            viewController.notificationModuleID = Int(notifDetailArray.module_id)!
            viewController.notificationCourseID = Int(notifDetailArray.course_id)!
            self.navigationController?.pushViewController(viewController, animated: true)
            
        }else{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController:NotificationDetailViewController = storyboard.instantiateViewController(withIdentifier: "NotificationDetailViewController") as! NotificationDetailViewController
            viewController.tappedNotifIndex = (indexPath?.row)!
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        
        
        print("Review Prepare func init")
        
        let indexPath  = self.tableView.indexPathForSelectedRow!
        
        let learnerDetails =  LearnerDetails.sharedInstance
        
        let lId:String = notificationData.getRepresentativeId(index: indexPath.row)
        notificationData.setCureentUserId(id: lId)
        //print("Review lear id:  \(lId)")
        //learnerDetails.printLearnerid()
        //let index:Int = learnerDetails.getIndexforLearnerid(lId: lId)
        //print("Lerner index : \(index)")
        //learnerDetails.setSelectedLearner(index: index)
        
        learnerDetails.setSelectedLearnerId(id: lId)
        
    }
    
    
    func removeNotification(notificationId:String)
    {
        let progressHUD = ProgressHUD()
        progressHUD.setCustomText(text: "loadingdata".localized())
        self.view.addSubview(progressHUD)
        progressHUD.activityIndictor.color = UIColor.white
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        RemoveUserNotification.sharedInstance.loadData(notificationId:notificationId, completionHandler: { (status:Bool) -> () in
            
            if (self.notificationData.getreiviewDetailsCount() >= 0 )
            {
                let login = Login.sharedInstance
                
                
                //UIApplication.shared.endIgnoringInteractionEvents()
                if login.status != nil
                {
                    progressHUD.hide()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    let alertController = UIAlertController(title: "User status", message: login.message , preferredStyle: .alert)
                    let OKAction = UIAlertAction(title: "ok".localized(), style: .default) {
                        (action:UIAlertAction!) in
                        login.logoutFromDeviceCache()
                        self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
                    }
                    alertController.addAction(OKAction)
                    self.present(alertController, animated: true, completion:nil)
                    
                }
                else
                {
                    
                    DispatchQueue.main.sync
                        {
                            self.loadLernerData()
                            progressHUD.hide()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            //self.reviewNotificationData.desc()
                            //self.tableView.reloadData()
                            
                            
                    } //learner details service completed
                }
            }// end if learnerDetails.getLearnerCount
            else
            {
                print("Problem while loading data ")
                let alert = UIAlertController(title: "notificationerror".localized(), message: "errordetails".localized(), preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "ok".localized(), style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
                
            } // end else learnerDetails.getLearnerCount
            
        })
    }
    
}
