//
//  ModelReviewRequestService.swift
//  6one5Manager
//
//  Created by enyotalearning on 25/11/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation

class ModelReviewRequestService
{
    //using Singleton pattern for single object handling...
    class var sharedInstance: ModelReviewRequestService
    {
        //2
        struct Singleton
        {
            //3
            static let instance = ModelReviewRequestService()
        }
        //4
        return Singleton.instance
    }
    
    func loadData(reprenentativeId:String,completionHandler:@escaping (ModelReviewRequest)->())
    {
        let headers = [
            "cache-control": "no-cache",
            ]
        let login = Login.sharedInstance //"https://key2train.in/admin/api/v1/module_review_request/"
        let apiURL: String = WebAPI.MODULEREVIEWREQUEST + login.getManagerId()// + reprenentativeId //WebAPI.MANAGERGETNOTIFS + login.getManagerId()
        
        let request = NSMutableURLRequest(url: NSURL(string: apiURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("Network error: \(error)")
                
                let modelReviewRequest = ModelReviewRequest.sharedInstance
                completionHandler(modelReviewRequest)
            } else
            {
                //let httpResponse = response as? HTTPURLResponse
                //print("HTTP Response for the learner list @@@@ : \(data)")
                
                do
                {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                    {
                        let modelReviewRequest = ModelReviewRequest.sharedInstance
                        
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        if statusVal == nil
                        {
                            print("status val is null")
                            let statusVal = convertedJsonIntoDict["status"] as? String
                            print("status valule is \(statusVal)")
                            let login = Login.sharedInstance
                            login.status = String(describing: convertedJsonIntoDict["status"] as? Bool)
                            login.message = convertedJsonIntoDict["message"] as? String
                            
                            
                        }
                        else
                        {
                            
                            if(statusVal)!
                            {
                                
                                //var count = convertedJsonIntoDict["count"] as? Int
                                //count = count! - 1
                                
                                let json = JSON(convertedJsonIntoDict)
                                var modelReviewRequestData:ModelReviewRequestData
                                for item in json["data"].arrayValue
                                {
                                    modelReviewRequestData = ModelReviewRequestData()
                                    
                                    modelReviewRequestData.id = item["id"].stringValue
                                    modelReviewRequestData.name = item["name"].stringValue
                                    modelReviewRequestData.l_fname = item["l_fname"].stringValue
                                    modelReviewRequestData.l_lname = item["l_lname"].stringValue
                                    modelReviewRequestData.module_id = item["module_id"].stringValue
                                    modelReviewRequestData.course_id = item["course_id"].stringValue
                                    
                                    modelReviewRequest.addModelReviewRequest(modelReviewRequestData: modelReviewRequestData)
                                }
                                
                                
                            }//end of if
                        }
                        print("Modele Review request count \(modelReviewRequest.getRequestCount())")
                        completionHandler(modelReviewRequest)
                    }
                    
                } catch let error as NSError
                {
                    print(error)
                }
                
                
            }
            
            
        })
        
        dataTask.resume()
        
    }
    
    
    
}
