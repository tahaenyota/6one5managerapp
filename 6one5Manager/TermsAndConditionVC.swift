//
//  TermsAndConditionVC.swift
//  6one5Manager
//
//  Created by enyotalearning on 28/12/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit

class TermsAndConditionVC: UIViewController {
    
    
    @IBOutlet weak var webview: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = false
        self.title = "Terms and Condition"
        // Do any additional setup after loading the view.
        
        let localfilePath = Bundle.main.url(forResource: "copyright_disclaimer2", withExtension: "html");
        let myRequest = NSURLRequest(url: localfilePath!);
        self.webview.loadRequest(myRequest as URLRequest);
        
    }
    
    
    @IBAction func Disagree(_ sender: AnyObject)
    {
        self.dismiss(animated: true, completion: nil)
        
        /* DispatchQueue.main.async()
         {
         //self.imageView.image = image;
         let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
         let initViewController: UIViewController = storyboard.instantiateViewController(withIdentifier: "LoginVC") as UIViewController
         self.present(initViewController, animated: true, completion: nil)
         
         
         }*/
        
        
    }
    
    @IBAction func agreeTapped(_ sender: AnyObject)
    {
        submitReview()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func submitReview()
    {
        
        let progressHUD = ProgressHUD()
        
        self.view.addSubview(progressHUD)
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        
        //course_id = 2
        
        let headers = [
            "cache-control": "no-cache"
        ]
        
        let login = Login.sharedInstance
        
        let dataStr: String!
        
        dataStr = "data={ \"accept_terms\":\"\("1")\"}"
        
        
        
        let postData = NSMutableData(data: dataStr.data(using: String.Encoding.utf8)!)
        let apiString = WebAPI.ACCEPTTERMS + login.getManagerId()//Constants.managerSubmitRatingURL + manager_id
        let request = NSMutableURLRequest(url: NSURL(string: apiString)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            //print("Magic Data is  \(data!)")
            if (error != nil) {
                print("Error: \(error)")
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
                let alertController = UIAlertController(title: "Terms and Condition", message: "Acceptence Failed. Network error. Please try again." , preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "ok".localized(), style: .default) {
                    (action:UIAlertAction!) in
                    //self.navigationController?.isNavigationBarHidden = false
                    //self.view.removeFromSuperview()
                    //self.navigationController?.popViewController(animated: true)
                    self.dismiss(animated: true, completion: nil)
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion:nil)
                progressHUD.hide()
                
            } else {
                let httpResponse = response as? HTTPURLResponse
                print("Response: \(httpResponse)")
                do {
                    
                    print("Magic Data is  \(data!)")
                    
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        print("Manager ffff",convertedJsonIntoDict)
                        
                        // Get value by key
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        print("Status: \(statusVal!)")
                        
                        if(statusVal! == true)
                        {
                            let alertController = UIAlertController(title: "Terms and Condition", message: "Terms & Conditions accepted." , preferredStyle: .alert)
                            progressHUD.hide()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            let OKAction = UIAlertAction(title: "ok".localized(), style: .default) { (action:UIAlertAction!) in
                                //self.navigationController?.isNavigationBarHidden = false
                                //self.view.removeFromSuperview()
                                let login = Login.sharedInstance
                                login.termAccepted = "1"
                                login.saveLoginToDevice()
                                //TODO : Post service to send accepted flag to server
                                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let initViewController: UIViewController = storyboard.instantiateViewController(withIdentifier: "PersonalDetailsController") as UIViewController
                                self.present(initViewController, animated: true, completion: nil)
                                //self.dismiss(animated: true, completion: nil)
                                /*DispatchQueue.main.async()
                                 {
                                 //self.imageView.image = image;
                                 let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                 let initViewController: UIViewController = storyboard.instantiateViewController(withIdentifier: "homeviewController") as UIViewController
                                 self.present(initViewController, animated: true, completion: nil)
                                 
                                 
                                 }*/
                                
                                
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                            progressHUD.hide()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            
                        } else {
                            progressHUD.hide()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            var error_msg:NSString
                            if convertedJsonIntoDict["message"] as? NSString != nil {
                                error_msg = convertedJsonIntoDict["message"] as! NSString
                            } else {
                                error_msg = "Unknown Error"
                            }
                            print("error_msg",error_msg)
                            let alertController = UIAlertController(title: "Terms and Condition", message: "Error in contacting the server. Please try again.." , preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "ok".localized(), style: .default) { (action:UIAlertAction!) in
                                //self.navigationController?.isNavigationBarHidden = false
                                //self.view.removeFromSuperview()
                                //self.navigationController?.popViewController(animated: true)
                                self.dismiss(animated: true, completion: nil)
                            }
                            
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                            progressHUD.hide()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            
                        }
                    }
                } catch let error as NSError {
                    print(error)
                    progressHUD.hide()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    let alertController = UIAlertController(title: "Terms and Condition", message: "Error in contacting the server. Please try again.." , preferredStyle: .alert)
                    let OKAction = UIAlertAction(title: "ok".localized(), style: .default) { (action:UIAlertAction!) in
                        //self.navigationController?.isNavigationBarHidden = false
                        //self.view.removeFromSuperview()
                        //self.navigationController?.popViewController(animated: true)
                        self.dismiss(animated: true, completion: nil)
                    }
                    
                    alertController.addAction(OKAction)
                    self.present(alertController, animated: true, completion:nil)
                    
                    
                    
                }
                
                
                
                print("Data: \(data)")
            }
        })
        
        dataTask.resume()
    }
    
    
}




