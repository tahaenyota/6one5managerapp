//
//  FAQHeaderView.swift
//  LearningApp
//
//  Created by Sumit More on 12/29/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import UIKit

class AccordionHeaderView: FZAccordionTableViewHeaderView {
    static let kDefaultAccordionHeaderViewHeight: CGFloat = 44.0;
    static let kAccordionHeaderViewReuseIdentifier = "AccordionHeaderViewReuseIdentifier";
}
