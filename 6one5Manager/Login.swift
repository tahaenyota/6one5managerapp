//
//  Login.swift
//  6one5Manager
//
//  Created by enyotalearning on 29/09/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit

public class Login: NSObject
{
    
    var userName : String!
    var password : String!
    var id : String!
    var courseId:String!
    var isLoggedIn:String!
    var errorMessage:String!
    var fName:String!
    var lName:String!
    var language:String!
    var notificationcount = 0
    var status:String!
    var message:String!
    var termAccepted:String!
    
    
    //Singleton pattern
    //This is to make sure will use only one object for creating the login
    class var sharedInstance: Login
    {
        //2
        struct Singleton {
            //3
            static let instance = Login()
        }
        //4
        return Singleton.instance
    }
    
    //This is init method but I have not used the default method just because of the future purpose and we can achive the singleton pattern,
    func setup(userName: String, password: String, id: String, courseId:String, isLoggedIn:String, fname:String, lName:String, language:String, notification_count:Int, termAccepted:String)
    {
        print("islogged in value for save \(isLoggedIn)");
        //super.init()
        self.userName = userName
        self.password = password
        self.id = id
        self.courseId = courseId
        self.fName = fname
        self.lName = lName
        self.isLoggedIn = isLoggedIn
        self.language = language
        self.notificationcount = Int(notification_count)
        self.termAccepted = termAccepted
        
        print ("user details  \(description)")
    }
    
    //Showing object description for the login object
    override public var description: String
    {
        return "userName: \(userName) \t" +
            "password: \(password) \t" +
            "id: \(id) \t" +
            "course id: \(courseId) \t" +
            "isLoggedIn: \(isLoggedIn) \t" +
        "prefared language \(language) \t"
    }
    
    //will return the result as we want to check the user is loged in or not
    func getIsLogged()->String
    {
        return self.isLoggedIn
    }
    
    func setIsLogin(islogin:String){
        self.isLoggedIn = islogin
    }
    
    //we want to save records to the device memory for the login persistancy with device.
    func saveLoginToDevice()
    {
        let defaults = UserDefaults.standard // This is just to set this is device cache.
        print("login details saved in cache \(self.isLoggedIn)")
        defaults.set("Coding Explorer", forKey: "userNameKey")
        defaults.set(self.userName, forKey: "username")
        defaults.set(self.password, forKey: "password")
        defaults.set(self.id, forKey: "id")
        defaults.set(self.courseId, forKey: "courseId")
        defaults.set(self.isLoggedIn, forKey: "isLoggedIn")
        defaults.set(self.fName,forKey:"fName")
        defaults.set(self.lName,forKey:"lName")
        defaults.set(self.language,forKey:"praferdLanguage")
        defaults.set(self.notificationcount,forKey:"notification_count")
        defaults.set(self.termAccepted, forKey:"terms_accepted")
        
    }
    
    func setErrorMessage(msg:String)
    {
        errorMessage = msg
    }
    
    func getManagerId() -> String
    {
        return id
    }
    
    func getPraferdLanguage() ->String
    {
        return language
    }
    
    func getNotificationcount() ->Int
    {
        return notificationcount
    }
    func logoutFromDeviceCache()
    {
        setup(userName:  "",
              password: "",
              id: "",
              courseId: "",
              isLoggedIn: "0",
              fname: "",
              lName: "",
              language: "en",
              notification_count:0,
              termAccepted: "0"
        )
        saveLoginToDevice()
    }
    
}
