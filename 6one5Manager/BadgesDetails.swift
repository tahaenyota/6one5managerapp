//
//  BadgesDetails.swift
//  6one5Manager
//
//  Created by enyotalearning on 23/11/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation
import UIKit

class BadgesDetails
{
    var id:String!
    var badgeId:String!
    var userId:String!
    var badge:UIImage!
    
    
    func setBadgeImage()
    {
        switch badgeId
        {
        case "1":
            badge = UIImage(named: "b1.png")!
            break
            
        case "2":
            badge = UIImage(named: "b2.png")!
            break
            
        case "3":
            badge = UIImage(named: "b3.png")!
            break
            
        case "4":
            badge = UIImage(named: "b4.png")!
            break
            
        case "5":
            badge = UIImage(named: "b5.png")!
            break
            
        default :
            print("there is no badge found for this id")
        }
        
    }
}
