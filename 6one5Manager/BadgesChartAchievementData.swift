//
//  BadgesChartAchievementData.swift
//  6one5Manager
//
//  Created by enyotalearning on 03/12/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation

class BadgesChartAchievementData
{
    var badgesAchievmentArray = [BadgesAchievmentData]()
    var idValues = [String]()
    var countValues = [Double]()
    var message:String!
    var status:String!
    
    class var sharedInstance: BadgesChartAchievementData
    {
        //2
        struct Singleton {
            //3
            static let instance = BadgesChartAchievementData()
        }
        //4
        return Singleton.instance
    }
    
    func addBadgesAchievmentArray(badgeAchievmentData:BadgesAchievmentData)
    {
        badgesAchievmentArray.append(badgeAchievmentData)
        idValues.append(badgeAchievmentData.id)
        countValues.append(Double(badgeAchievmentData.cnt)!)
    }
    
    func getBadgesAchievmentArray(index:Int) -> BadgesAchievmentData
    {
        let rv:BadgesAchievmentData = badgesAchievmentArray[index];
        
        return rv
    }
    
    
    func freeBadgesOrientationData()
    {
        badgesAchievmentArray = [BadgesAchievmentData]()
        idValues = [String]()
        countValues = [Double]()
    }
    
    
}


class BadgesAchievmentData
{
    var id:String!
    var cnt:String!
}
