//
//  BadgesService.swift
//  6one5Manager
//
//  Created by enyotalearning on 23/11/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation

class BadgesService
{
    class var sharedInstance: BadgesService
    {
        //2
        struct Singleton
        {
            //3
            static let instance = BadgesService()
        }
        //4
        return Singleton.instance
    }
    
    func loadData(completionHandler:@escaping (Badges)->())
    {
        let headers = [
            "cache-control": "no-cache",
            ]
        
        let login = Login.sharedInstance
        
        let apiURL: String =  WebAPI.USERBADGESNOTIFICATION + login.getManagerId()
        //WebAPI.MANAGERCERTIFICATE + "4/2" // login.getManagerId() + "/2" // "https://api.enyotalms.com/lms/test/admin/api/v1/learners/41"
        
        let request = NSMutableURLRequest(url: NSURL(string: apiURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil)
            {
                print("Error: \(error)")
                let badges = Badges.sharedInstance
                completionHandler(badges)
            } //end of error if
            else
            {
                //let httpResponse = response as? HTTPURLResponse
                print("HTTP Response for the learner list @@@@ : \(data)")
                
                do
                {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                    {
                        
                        print("Print converted dictionary",convertedJsonIntoDict)
                        
                        
                        // Get value by key
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        if statusVal == nil
                        {
                            print("status val is null")
                            let statusVal = convertedJsonIntoDict["status"] as? String
                            print("status valule is \(statusVal)")
                            let login = Login.sharedInstance
                            login.status = String(describing: convertedJsonIntoDict["status"] as? Bool)
                            login.message = convertedJsonIntoDict["message"] as? String
                            
                            
                        }
                        else
                        {
                            
                            if(statusVal)!
                            {
                                let badges = Badges.sharedInstance
                                //var path = convertedJsonIntoDict["data"] as? String
                                //certificates.path = path
                                
                                let json = JSON(convertedJsonIntoDict)
                                var badgeDetails:BadgesDetails
                                for item in json["data"].arrayValue
                                {
                                    badgeDetails = BadgesDetails()
                                    badgeDetails.id = item["id"].stringValue
                                    badgeDetails.badgeId = item["badge_id"].stringValue
                                    badgeDetails.userId = item["user_id"].stringValue
                                    //certDetails.id = item["id"].stringValue
                                    //certDetails.path = item["path"].stringValue
                                    
                                    /*let url = URL(string: certDetails.path)
                                     let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                                     certDetails.image = UIImage(data: data!)*/
                                    
                                    badges.addCertificateData(badgesDetils: badgeDetails)
                                    print(" badges id " + item["id"].stringValue)
                                }
                                
                                completionHandler(badges)
                            }//end of if
                        }
                        
                        
                    }
                    
                } catch let error as NSError
                {
                    print(error)
                }
                
                
            }// End else
            
            
        })
        
        dataTask.resume()
    }
    
}
