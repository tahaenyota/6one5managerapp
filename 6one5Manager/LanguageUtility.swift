//
//  LanguageUtility.swift
//  6one5Manager
//
//  Created by enyotalearning on 09/11/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation
import Localize_Swift

class LanguageUtility
{
    class var sharedInstance: LanguageUtility
    {
        //2
        struct Singleton {
            //3
            static let instance = LanguageUtility()
        }
        //4
        return Singleton.instance
    }
    
    
    func updateLanguage()
    {
        //Localization
        let language = Language.sharedInstance
        //["English","Chinese","Arabic"]
        if language.currentLanguage == "Chinese"
        {
            Localize.setCurrentLanguage("zh-Hans");
            print("########### Chinese language selected")
            UserDefaults.standard.set(["zh-Hans"], forKey: "AppleLanguages")
            UserDefaults.standard.synchronize()
            
        }
        else if language.currentLanguage == "Arabic"
        {
            Localize.setCurrentLanguage("ar");
            print("########### Arabic language selected")
            UserDefaults.standard.set(["ar"], forKey: "AppleLanguages")
            UserDefaults.standard.synchronize()
            
        }
        else
        {
            Localize.setCurrentLanguage("eg");
            UserDefaults.standard.set(["en"], forKey: "AppleLanguages")
            UserDefaults.standard.synchronize()
            
            print("########### English language selected")
        }
        
        //        Localize.setCurrentLanguage("zh-Hans");
        //        print("########### Chinese language selected")
        //        UserDefaults.standard.set(["zh-Hans"], forKey: "AppleLanguages")
        //        UserDefaults.standard.synchronize()
    }
}
