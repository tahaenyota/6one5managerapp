//
//  Constants.swift
//  6one5Manager
//
//  Created by Sumit More on 12/19/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation
import UIKit

class Constants {
    
    static let profile_image_size: CGFloat = 200.0
    static let profile_image_radius: CGFloat = 100.0
     static let screenSize = UIScreen.main.bounds
    static let device = UIDevice.current.model
    
    static let Font_iPHONE_Tag = "<font face ='Helvetica Neue' size='5'>"
    static let Font_iPAD_Tag = "<font face ='Helvetica Neue' size='6'>"
    static let Font_CLOSE_Tag = "</font>"
    
    static let fontclosing:String = "</font>";
    static var DiscripterFontTag:String = "<font size='6'>";
   
    
    static let salesRepFeedbackURL: String = "https://key2train.in/admin/api/v1/feedback/"
    //    static let salesRepFeedbackURL: String = "https://api.enyotalms.com/lms/test/admin/api/v1/feedback/"
    
    static let salesRepQuizSubmitForReviewURL: String = "https://key2train.in/admin/api/v1/module_data/"
    //    static let salesRepQuizSubmitForReviewURL: String = "https://api.enyotalms.com/lms/test/admin/api/v1/module_data/"
    
    //    static let loginURL: String = "https://api.enyotalms.com/lms/test/admin/api/v1/login"
    static let loginURL: String = "https://key2train.in/admin/api/v1/login"
    
    static let salesRepModulesInfoURL: String = "https://key2train.in/admin/api/v1/fetch_learner_manager_review/"
    //    static let salesRepModulesInfoURL: String = "https://api.enyotalms.com/lms/test/admin/api/v1/fetch_learner_manager_review/"
    
    static let salesManagerLatestModuleInfo: String = "https://key2train.in/admin/api/v1//module_data_status/"
    //a) URL => https://key2train.in/admin/api/v1//module_data_status/1576/2/5
    static let managerGetNotifsURL: String = "https://key2train.in/admin/api/v1/module_review_request/"
    //    static let managerGetNotifsURL: String = "https://api.enyotalms.com/lms/test/admin/api/v1/module_review_request/"
    
    static let managerSubmitRatingURL: String = "https://key2train.in/admin/api/v1/manager_descriptor_ratings/"
    //    static let managerSubmitRatingURL: String = "https://api.enyotalms.com/lms/test/admin/api/v1/manager_descriptor_ratings/"
    
    static let salesRep_manager_graph_color: UIColor = UIColor(red: 125/255, green: 208/255, blue: 26/255, alpha: 1)
    static let salesRep_quiz_graph_color: UIColor = UIColor(red: 242/255, green: 101/255, blue: 34/255, alpha: 1)
    static let salesRep_blank_graph_color: UIColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
    static let salesRep_hole_color: UIColor = UIColor(red: 235/255, green: 235/255, blue: 241/255, alpha: 1)
    
    static let salesRepPracticeWithManagerURL: String = "https://key2train.in/admin/api/v1/review_request/"
    //    static let salesRepPracticeWithManagerURL: String = "https://key2train.in/admin/api/v1/review_request/"
    
    static let salesRepPracticeWithCustomerURL: String = "https://key2train.in/admin/api/v1/customer_role_play_review_request/"
    
    static let salesRepCertificateURL: String = "https://key2train.in/admin/api/v1/send_certificate/"
    //    static let salesRepCertificateURL: String = "https://api.enyotalms.com/lms/test/admin/api/v1/send_certificate/"
    
    static let salesRepUpdateProfileURL: String = "https://key2train.in/admin/api/v1/update_user_profile"
    // static let salesRepUpdateProfileURL: String = "https://key2train.in/admin/api/v1/test"
    static let salesRepPersonalDetailsURL: String = "https://key2train.in/admin/api/v1/update_personal_email_mobile/"
    static let getNotificationStatusDetailURL: String = "a) https://key2train.in/admin/api/v1/update_user_notification_to_read/"
    
    // Module 0
    
    
    static let M0_mgr_m0_e1l1Constraints_6splus = [230,520]
    static let M0_mgr_m0_e2l1Constraints_6splus = [230,595]
    
    static let M0_mgr_m0_e4l1Constraints_6splus = [230,525]
    static let M0_mgr_m0_e5l1Constraints_6splus = [230,585]
    static let M0_mgr_m0_e6l1Constraints_6splus = [230,643]
    
    static let M0_mgr_m0_e8l1Constraints_6splus = [201,95]
    static let M0_mgr_m0_e8l2Constraints_6splus = [230,285]
    static let M0_mgr_m0_e8l3Constraints_6splus = [230,411]
    static let M0_mgr_m0_e8l4Constraints_6splus = [31,591]
    
    static let M0_mgr_m0_e17l1Constraints_6splus = [230,268]
    static let M0_mgr_m0_e17l2Constraints_6splus = [230,333]
    static let M0_mgr_m0_e17l3Constraints_6splus = [230,387]
    static let M0_mgr_m0_e17l4Constraints_6splus = [230,445]
    static let M0_mgr_m0_e17l5Constraints_6splus = [230,500]
    
    static let M0_mgr_m0_e22l1Constraints_6splus = [230,340]
    static let M0_mgr_m0_e23l1Constraints_6splus = [230,535]
    
    
    
    // Module 1
    
    
    static let M1_mgr_m1_e2l1Constraints_6splus = [125,311]
    static let M1_mgr_m1_e2l2Constraints_6splus = [125,180]
    static let M1_mgr_m1_e2l3Constraints_6splus = [235,325]
    static let M1_mgr_m1_e2l4Constraints_6splus = [140,455,131]
    
    static let M1_mgr_m1_e4b1Constraints_6splus = [78,75,125,100]
    static let M1_mgr_m1_e4b2Constraints_6splus = [225,225,125]
    static let M1_mgr_m1_e4b3Constraints_6splus = [88,360,125]
    static let M1_mgr_m1_e4b4Constraints_6splus = [230,475,125]
    
    
    // Module 2
    
    static let M2_mgr_m2_e2l1Constraints_6splus = [125,320,35]
    static let M2_mgr_m2_e3l1Constraints_6splus = [140,155,160,120]
    static let M2_mgr_m2_e4l1Constraints_6splus = [172,325,225]
    static let M2_mgr_m2_e5l1Constraints_6splus = [140,460,160]
    
    
    static let M2_mgr_m2_e7l1Constraints_6splus = 495
    static let M2_mgr_m2_e8l1Constraints_6splus = 540
    static let M2_mgr_m2_e9l1Constraints_6splus = 540
    static let M2_mgr_m2_e10l1Constraints_6splus = 495
    
    static let M2_mgr_m2_e11l1Constraints_6splus = 275
    static let M2_mgr_m2_e12l1Constraints_6splus = 420
    static let M2_mgr_m2_e13l1Constraints_6splus = 525
    //    static let M2_mgr_m2_e10l1Constraints_6splus = 500
    
    
    // Module 3
    
    static let M3_mgr_m2_e7l1Constraints_6splus = 500
    static let M3_mgr_m2_e8l1Constraints_6splus = 550
    static let M3_mgr_m2_e9l1Constraints_6splus = 557
    static let M3_mgr_m2_e10l1Constraints_6splus = 500
    
    
    
    
    static let M3_mgr_m3_e15l1Constraints_6splus = 152
    static let M3_mgr_m3_e15l2Constraints_6splus = 211
    static let M3_mgr_m3_e15l3Constraints_6splus = 275
    static let M3_mgr_m3_e15l4Constraints_6splus = 325
    static let M3_mgr_m3_e15l5Constraints_6splus = 400
    static let M3_mgr_m3_e15l6Constraints_6splus = 450
    static let M3_mgr_m3_e15l7Constraints_6splus = 515
    static let M3_mgr_m3_e15l8Constraints_6splus = 575
    
    static let M3_mgr_m3_e16l1Constraints_6splus = [120,203]
    static let M3_mgr_m3_e16l2Constraints_6splus = [120,306]
    
    
    
    // Module 4
    
    
    static let M4_mgr_m4_e2l1Constraints_6splus = 325
    static let M4_mgr_m4_e2l2Constraints_6splus = [66,422]
    //                                             W   T   L  H
    static let M4_mgr_m4_e3l1Constraints_6splus = [90,414,253,86]
    
    
    static let M4_mgr_m4_e4l2Constraints_6splus = [195,61]
    static let M4_mgr_m4_e4l3Constraints_6splus = 195
    static let M4_mgr_m4_e4l4Constraints_6splus = 521
    static let M4_mgr_m4_e4l5Constraints_6splus = 521
    
    static let M4_mgr_m4_e4l1Constraints_6splus = [130,335]
    static let M4_mgr_m4_e6l1Constraints_6splus = [130,350]
    static let M4_mgr_m4_e7l1Constraints_6splus = [150,250]
    static let M4_mgr_m4_e8l1Constraints_6splus = [150,440]
    static let M4_mgr_m4_e9l1Constraints_6splus = [150,425]
    static let M4_mgr_m4_e11l1Constraints_6splus = [250,180,101]
    static let M4_mgr_m4_e12l1Constraints_6splus = [250,275]
    static let M4_mgr_m4_e13l1Constraints_6splus = [250,353,101]
    static let M4_mgr_m4_e14l1Constraints_6splus = [250,460]
    static let M4_mgr_m4_e15l1Constraints_6splus = [250,570,121]
    static let M4_mgr_m4_e16l1Constraints_6splus = [100,220]
    
    static let M4_mgr_m4_e16l2Constraints_6splus = [90,330]
    static let M4_mgr_m4_e16l3Constraints_6splus = [90,350]
    static let M4_mgr_m4_e16l4Constraints_6splus = [90,421]
    static let M4_mgr_m4_e16l5Constraints_6splus = [90,415]

}
