//
//  RepresentativeListVC.swift
//  6one5Manager
//
//  Created by enyotalearning on 02/12/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit

class RepresentativeListVC: UIViewController ,UITableViewDataSource, UITableViewDelegate
{
    
    let learnerDetails =  LearnerDetails.sharedInstance
    
    @IBOutlet weak var tableView: UITableView!
    
    var badgeId:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        print("Badge is \(badgeId!)")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        
        //self.title = "performance".localized()
        
        let progressHUD = ProgressHUD()
        self.view.addSubview(progressHUD)
        
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        /* if (self.individualPerformanceData.getQuizDataCount() == 0 )
         {
         
         clearIndividualData()
         
         }// end of checking
         
         */
        if (self.learnerDetails.getLearnerCount() > 0 )
        {
            //print("lerner details Data Reset ")
            learnerDetails.learnerProfileArray = [String]()
            learnerDetails.learnerProfileArray = [String]()
            learnerDetails.learnerId = [String]()
            learnerDetails.selectedLearner = -1
            learnerDetails.performanceData = [IndividualPerformanceData]()
        }
        
        LearnersDetailsService.sharedInstance.loadData(completionHandler: { (userData:LearnerDetails) -> () in
            
            
            let login = Login.sharedInstance
            
            if login.status != nil
            {
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
                let alertController = UIAlertController(title: "User status", message: login.message , preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "ok".localized(), style: .default) {
                    (action:UIAlertAction!) in
                    login.logoutFromDeviceCache()
                    self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion:nil)
                
            }
                
                
            else
            {
                if (self.learnerDetails.getLearnerCount() > 0 )
                {
                    //
                    DispatchQueue.main.async()
                        {
                            //self.imageView.image = image;
                            progressHUD.hide()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            self.tableView.reloadData()
                    } //learner details service completed
                    
                }// end if learnerDetails.getLearnerCount
                else
                {
                    DispatchQueue.main.async()
                        {
                            progressHUD.hide()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            self.tableView.reloadData()
                    }
                } // end else learnerDetails.getLearnerCount
            }
        })
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        /*if(searchActive) {
         return filtered.count
         }*/
        return learnerDetails.learnerProfileArray.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell;
        /*if(searchActive){
         cell.textLabel?.text = filtered[(indexPath as NSIndexPath).row]
         } else {
         cell.textLabel?.text = learnerDetails.learnerProfileArray[(indexPath as NSIndexPath).row];
         }*/
        
        cell.textLabel?.text = learnerDetails.learnerProfileArray[(indexPath as NSIndexPath).row];
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let indexPath = tableView.indexPathForSelectedRow //optional, to get from any UIButton for example
        
        //let currentCell = tableView.cellForRow(at: indexPath!)! as UITableViewCell
        
        let selectedIndex:Int = (indexPath?.row)!
        learnerDetails.setSelectedLearner(index: selectedIndex)
        let lernerid:String = learnerDetails.getLearnerId(index: selectedIndex)
        //let reviewNotificationData = ReviewNotificationData.sharedInstance
        //reviewNotificationData.setCureentUserId(id: lernerid)
        learnerDetails.setSelectedLearnerId(id: lernerid)
        
        
        //TODO: Call the post service from here to assign a badge to Representative.
        submitBadgeToRepresentative(learnerId: lernerid)
        
    }
    
    
    func submitBadgeToRepresentative(learnerId:String)
    {
        
        let progressHUD = ProgressHUD()
        self.view.addSubview(progressHUD)
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        //let badgesId = "3"
        let userId = learnerId
        let courseId = "2"
        
        let headers = [
            "cache-control": "no-cache"
        ]
        
        let dataStr: String!
        
        dataStr = "data=[{ \"badge_id\":\"\(badgeId as NSString)\",\"user_id\":\"\(String(userId) as NSString)\",\"course_id\":\"\(String(courseId) as NSString)\" }]"
        
        
        
        print("Data string is here \(dataStr)")
        
        let postData = NSMutableData(data: dataStr.data(using: String.Encoding.utf8)!)
        //print("Post string is \(dataStr.data(using: String.Encoding.utf8))")
        /*let dataStr: String = "data={ \"uname\" : \"\(self.UserNameTxtField.text! as NSString)\" , \"upass\" : \"\(self.PasswordTxtField.text! as NSString)\" }"
         print("POST Params: \(dataStr)")
         var postData = NSMutableData(data: dataStr.data(using: String.Encoding.utf8)!)
         
         */
        
        let apiString = "https://key2train.in/admin/api/v1/assign_user_badges"//WebAPI.MANAGERSUBMITRATING + manager_id//Constants.managerSubmitRatingURL + manager_id
        let request = NSMutableURLRequest(url: NSURL(string: apiString)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            //print("Magic Data is  \(data!)")
            if (error != nil) {
                print("Error: \(error)")
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
                let alertController = UIAlertController(title: "representativelist".localized(), message: "connectionerror".localized() , preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "ok".localized(), style: .default) { (action:UIAlertAction!) in
                    _ = self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion:nil)
                
                
                
            } else {
                let httpResponse = response as? HTTPURLResponse
                print("Response: \(httpResponse)")
                do {
                    
                    //print("Magic Data is  \(data!)")
                    
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        print("Print converted dictionary",convertedJsonIntoDict)
                        
                        // Get value by key
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        print("Status: \(statusVal!)")
                        
                        if(statusVal! == true)
                        {
                            progressHUD.hide()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            let alertController = UIAlertController(title: "badgesubmission".localized(), message: "badgesubmissionsucess".localized() , preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "ok".localized(), style: .default) { (action:UIAlertAction!) in
                                //self.navigationController?.isNavigationBarHidden = false
                                //self.view.removeFromSuperview()
                                _ = self.navigationController?.popViewController(animated: true)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                            
                            
                            
                        } else {
                            var error_msg:NSString
                            if convertedJsonIntoDict["message"] as? NSString != nil {
                                error_msg = convertedJsonIntoDict["message"] as! NSString
                            } else {
                                error_msg = "Unknown Error"
                            }
                            progressHUD.hide()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            print("error_msg",error_msg)
                            let alertController = UIAlertController(title: "badgesubmission".localized(), message: "unresponsiveserver".localized(), preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "ok".localized(), style: .default) { (action:UIAlertAction!) in
                                
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                            
                        }
                    }
                } catch let error as NSError {
                    print(error)
                    progressHUD.hide()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    let alertController = UIAlertController(title: "badgesubmission".localized(), message: "errordetails".localized() , preferredStyle: .alert)
                    let OKAction = UIAlertAction(title: "ok".localized(), style: .default) { (action:UIAlertAction!) in
                        self.navigationController?.popViewController(animated: true)
                    }
                    alertController.addAction(OKAction)
                    self.present(alertController, animated: true, completion:nil)
                    
                    
                }
                print("Data: \(data)")
            }
        })
        
        dataTask.resume()
    }
}
