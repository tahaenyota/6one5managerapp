//
//  NotificationDetailViewController.swift
//  6one5Manager
//
//  Created by enyotalearning on 17/01/17.
//  Copyright © 2017 enyotalearning. All rights reserved.
//

import UIKit

class NotificationDetailViewController: UIViewController {
    @IBOutlet weak var notifName: UILabel!
    @IBOutlet weak var notifDate: UILabel!
    @IBOutlet weak var notifcontent: UILabel!
    var tappedNotifIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        let notifObject = NotificationData.sharedInstance
        
        let notifDetailArray = notifObject.getNotificationDetails(index: tappedNotifIndex)
        self.notifName.text = notifDetailArray.name
        self.notifDate.text = notifDetailArray.assigned_date
        self.notifcontent.text = notifDetailArray.content
        //self.notificationId = notifDetailArray.id
        self.getNotificationStatus(notificationId:notifDetailArray.id )
        //self.navigationController?.isNavigationBarHidden = true
        
        // Do any additional setup after loading the view.
    }
    
    func getNotificationStatus(notificationId:String){
        
        let progressHUD = ProgressHUD()
        self.view.addSubview(progressHUD)
        
        
        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            ]
        let apiURL: String = "https://key2train.in/admin/api/v1/update_user_notification_to_read/" + notificationId
        
        let request = NSMutableURLRequest(url: NSURL(string: apiURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            DispatchQueue.main.async() {
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
            }
            if (error != nil) {
                print("Network Error: \(error)")
                let alert = UIAlertController(title: "modelreviewsubmission".localized(), message: "errordetails".localized(), preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "ok".localized(), style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            } else {
                let httpResponse = response as? HTTPURLResponse
                print("HTTP Response: \(httpResponse)")
                
                do {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        print("Print converted dictionary 111 ",convertedJsonIntoDict)
                        
                        // Get value by key
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        print("status value is \(statusVal!)")
                        
                        if(statusVal! == true)
                        {
                            OperationQueue.main.addOperation{
                                // let tempDict = convertedJsonIntoDict ["details"] as? NSDictionary
                                
                                
                                progressHUD.hide()
                            }
                        }                     }
                } catch let error as NSError {
                    print(error)
                    progressHUD.hide()
                    
                    
                }
                
                
            }
        })
        
        dataTask.resume()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func notificationOKButton(_ sender: AnyObject) {
        
        _ = navigationController?.popViewController(animated: true)
    }
}
