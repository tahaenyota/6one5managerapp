//
//  IndividualPerformanceService.swift
//  6one5Manager
//
//  Created by enyotalearning on 01/10/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation
public class IndividualPerformanceService : NSObject
{
    
    //using Singleton pattern for single object handling...
    class var sharedInstance: IndividualPerformanceService
    {
        //2
        struct Singleton
        {
            //3
            static let instance = IndividualPerformanceService()
        }
        //4
        return Singleton.instance
    }
    
    /*func loadData(completionHandler:@escaping (IndividualPerformanceData)->())
     {
     let headers = [
     "cache-control": "no-cache",
     ]
     
     //let learnerDetails = LearnerDetails.sharedInstance
     // let learnerId = learnerDetails.getLearnerId(index: learnerDetails.selectedLearner)
     let login = Login.sharedInstance
     let apiURL: String = WebAPI.INDIVIDUALPERFORMANCE + "61"// login.getManagerId() //"https://key2train.in/admin/api/v1/performance/61"
     print(" @@@ Api Url is : \(apiURL)")
     var request = NSMutableURLRequest(url: NSURL(string: apiURL)! as URL,
     cachePolicy: .useProtocolCachePolicy,
     timeoutInterval: 10.0)
     request.httpMethod = "GET"
     request.allHTTPHeaderFields = headers
     
     let session = URLSession.shared
     let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
     if (error != nil) {
     print("Error: \(error)")
     var individualPerformance = IndividualPerformanceData.sharedInstance
     completionHandler(individualPerformance)
     } else
     {
     //let httpResponse = response as? HTTPURLResponse
     //print("HTTP Response for the learner list @@@@ : \(data)")
     
     do
     {
     if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
     {
     var individualPerformance = IndividualPerformanceData.sharedInstance
     
     let quizData = convertedJsonIntoDict["quiz"] as! NSDictionary
     
     for (key, value) in quizData {
     //print("quizData: Value: \(value) for key: \(key)")
     individualPerformance.addQuizDataPoints(dataPoint: key as! String)
     individualPerformance.addQuizDataValues(dataValue: value as! Double)
     }
     
     let reviewData = convertedJsonIntoDict["review"] as! NSDictionary
     
     for (key, value) in reviewData {
     individualPerformance.addReviewDataPoints(dataPoint: key as! String)
     individualPerformance.addReviewDataValues(dataValue: value as! Double)
     }
     
     let moduleStatus = convertedJsonIntoDict["module_status"] as! NSDictionary
     
     for (key, value) in moduleStatus {
     individualPerformance.addModuleStatusPoints(dataPoint: key as! String)
     individualPerformance.addModuleStatusPoints(dataValue: value as! Double)
     }
     completionHandler(individualPerformance)
     
     }
     
     } catch let error as NSError
     {
     print(error)
     }
     
     
     }
     
     
     })
     
     dataTask.resume()
     
     }*/
    
}
