//
//  ManagerFeedbackData.swift
//  6one5Manager
//
//  Created by enyotalearning on 04/10/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation
public class ManagerFeedbackData: NSObject
{
    class var sharedInstance: ManagerFeedbackData
    {
        //2
        struct Singleton {
            //3
            static let instance = ManagerFeedbackData()
        }
        //4
        return Singleton.instance
    }
}
