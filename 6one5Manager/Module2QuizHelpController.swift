//
//  Module2QuizHelpController.swift
//  6one5Manager
//
//  Created by Sumit More on 12/20/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation
import UIKit


class Module2QuizHelpController: UIViewController {
    
    @IBOutlet weak var HelpCloseBtn: UIButton!
    @IBOutlet weak var helpTextfield: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UserDefaults.standard.bool(forKey: "isPlusversion") == true {
            
            self.HelpCloseBtn.removeConstraints(self.HelpCloseBtn.constraints)
            let top_HelpCloseBtn = NSLayoutConstraint(item: self.HelpCloseBtn,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: 615)
            let height_HelpCloseBtn = NSLayoutConstraint(item: self.HelpCloseBtn,
                                                         attribute: NSLayoutAttribute.height,
                                                         relatedBy: NSLayoutRelation.equal,
                                                         toItem: nil,
                                                         attribute: NSLayoutAttribute.notAnAttribute,
                                                         multiplier: 1,
                                                         constant: 51)
            let left_HelpCloseBtn = NSLayoutConstraint(item: self.HelpCloseBtn,
                                                       attribute: NSLayoutAttribute.left,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.view,
                                                       attribute: NSLayoutAttribute.left,
                                                       multiplier: 1,
                                                       constant: 51)
            self.view.addConstraint(top_HelpCloseBtn)
            self.view.addConstraint(height_HelpCloseBtn)
            self.view.addConstraint(left_HelpCloseBtn)
            //            self.HelpCloseBtn.backgroundColor = UIColor.red
            
        }

        self.helpTextfield.text = "Mgr_Quiz_M2_Help".localized()
        self.helpTextfield.isScrollEnabled = false
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 36.0/255.0, green: 184.0/255.0, blue: 239.0/255.0, alpha: 1.0)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.helpTextfield.isScrollEnabled = true
    }
    
    @IBAction func CloseHelpPopupBtnTouch(_ sender: AnyObject) {
        //self.view.removeFromSuperview()
        _ = navigationController?.popViewController(animated: true)
    }
    
}
