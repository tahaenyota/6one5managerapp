//
//  Module1IntroController.swift
//  6one5Manager
//
//  Created by Sumit More on 12/20/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import AVFoundation

class Module1IntroController: UIViewController, AVAudioPlayerDelegate {
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var multimediaIntro: UIButton!
    @IBOutlet weak var introductionImage: UIImageView!
    
    @IBOutlet weak var mgr_m1_e1l1: UILabel!
    @IBOutlet weak var mgr_m1_e2l1: UILabel!
    @IBOutlet weak var mgr_m1_e2l2: UILabel!
    @IBOutlet weak var mgr_m1_e2l3: UILabel!
    @IBOutlet weak var mgr_m1_e2l4: UILabel!
    @IBOutlet weak var mgr_m1_e3l1: UILabel!
    @IBOutlet weak var mgr_m1_e3l2: UILabel!
    @IBOutlet weak var mgr_m1_e3l3: UILabel!
    @IBOutlet weak var mgr_m1_e3l4: UILabel!
    @IBOutlet weak var mgr_m1_e3l5: UILabel!
    @IBOutlet weak var mgr_m1_e3l6: UILabel!
    @IBOutlet weak var mgr_m1_e4l1: UILabel!
    
    @IBOutlet weak var mgr_m1_e4b1: UIButton!
    @IBOutlet weak var mgr_m1_e4b2: UIButton!
    @IBOutlet weak var mgr_m1_e4b3: UIButton!
    @IBOutlet weak var mgr_m1_e4b4: UIButton!
    
    
    @IBAction func introFwdClick(_ sender: AnyObject) {
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        audioPlayerIntroAud?.pause()
    }
    
    @IBAction func bulb1Click(_ sender: AnyObject) {
        self.bulbNo = 1
        let popOverVC = UIStoryboard (name: "Module1SB", bundle: nil).instantiateViewController(withIdentifier: "m1_wQID") as! Module1IntroPopupsController
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        popOverVC.modalTransitionStyle = .crossDissolve
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    @IBAction func bulb2Click(_ sender: AnyObject) {
        self.bulbNo = 2
        let popOverVC = UIStoryboard (name: "Module1SB", bundle: nil).instantiateViewController(withIdentifier: "m1_wQID") as! Module1IntroPopupsController
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        popOverVC.modalTransitionStyle = .crossDissolve
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    @IBAction func bulb3Click(_ sender: AnyObject) {
        self.bulbNo = 3
        let popOverVC = UIStoryboard (name: "Module1SB", bundle: nil).instantiateViewController(withIdentifier: "m1_wQID") as! Module1IntroPopupsController
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        popOverVC.modalTransitionStyle = .crossDissolve
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    @IBAction func bulb4Click(_ sender: AnyObject) {
        self.bulbNo = 4
        let popOverVC = UIStoryboard (name: "Module1SB", bundle: nil).instantiateViewController(withIdentifier: "m1_wQID") as! Module1IntroPopupsController
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        popOverVC.modalTransitionStyle = .crossDissolve
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    var playFlagIntroAud = 1
    var audioPlayerIntroAud: AVAudioPlayer?
    
    var GTTimer : Timer = Timer()
    var GTTimer2 : Timer = Timer()
    var isPaused = false
    var intervalVal: Double = 5
    var intervalVal2: Double = 0.15
    var bulbNo: Int = 0
    var questionNo: Int = 1
    
    @IBOutlet weak var introBtmLbl: UILabel!
    
    override func viewDidLoad() {
        
        self.introBtmLbl.text = "Mgr_M1_IntroductionBtmLbl".localized()
        
        //        let image = UIImage(named: "sof.png")
        //        self.navigationItem.titleView = UIImageView(image: image)
        
        let device = UIDevice.current.model
        print("Device type: " + device)
        
        if(device == "iPhone") {
            print("Changing size to 16")
            let attrs = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont.systemFont(ofSize: 16.0)
            ]
            self.navigationController!.navigationBar.titleTextAttributes = attrs
            
            let button: UIButton = UIButton(type: UIButtonType.custom)
            button.setImage(UIImage(named: "sof.png"), for: UIControlState.normal)
            button.backgroundColor = UIColor.white
            button.frame = CGRect(x: 0, y: 0, width: 56, height: 45)
            let barButton = UIBarButtonItem(customView: button)
            
            
            let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            negativeSpacer.width = -17;
            
            self.navigationItem.setRightBarButtonItems([negativeSpacer, barButton ], animated: false)
            
        } else {
            print("Changing size to 32")
            let attrs = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont.systemFont(ofSize: 32.0)
            ]
            self.navigationController!.navigationBar.titleTextAttributes = attrs
            
            let button: UIButton = UIButton(type: UIButtonType.custom)
            button.setImage(UIImage(named: "sof.png"), for: UIControlState.normal)
            button.backgroundColor = UIColor.white
            //set frame
            button.frame = CGRect(x: 0, y: 0, width: 56, height: 45)
            let barButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            
            
            let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            negativeSpacer.width = -21;
            
            self.navigationItem.setRightBarButtonItems([negativeSpacer, barButton ], animated: false)
        }
        
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 36.0/255.0, green: 184.0/255.0, blue: 239.0/255.0, alpha: 1.0)
        
        self.navigationItem.title = "Mgr_M1_ModuleName".localized()
        
        self.introductionImage.image = UIImage(named: "mgr_m1_intro1")
        
        self.mgr_m1_e1l1.isHidden = false
        self.mgr_m1_e2l1.isHidden = true
        self.mgr_m1_e2l2.isHidden = true
        self.mgr_m1_e2l3.isHidden = true
        self.mgr_m1_e2l4.isHidden = true
        self.mgr_m1_e3l1.isHidden = true
        self.mgr_m1_e3l2.isHidden = true
        self.mgr_m1_e3l3.isHidden = true
        self.mgr_m1_e3l4.isHidden = true
        self.mgr_m1_e3l5.isHidden = true
        self.mgr_m1_e3l6.isHidden = true
        self.mgr_m1_e4l1.isHidden = true
        
        self.mgr_m1_e4b1.isHidden = true
        self.mgr_m1_e4b2.isHidden = true
        self.mgr_m1_e4b3.isHidden = true
        self.mgr_m1_e4b4.isHidden = true
        
        self.mgr_m1_e1l1.text = "Mgr_Intro_M1_E1L1".localized()
        self.mgr_m1_e2l1.text = "Mgr_Intro_M1_E2L1".localized()
        self.mgr_m1_e2l2.text = "Mgr_Intro_M1_E3L1".localized()
        self.mgr_m1_e2l3.text = "Mgr_Intro_M1_E4L1".localized()
        self.mgr_m1_e2l4.text = "Mgr_Intro_M1_E5L1".localized()
        self.mgr_m1_e3l1.text = "Mgr_Intro_M1_E6L1".localized()
        self.mgr_m1_e3l2.text = "Mgr_Intro_M1_E6L2".localized()
        self.mgr_m1_e3l3.text = "Mgr_Intro_M1_E6L3".localized()
        self.mgr_m1_e3l4.text = "Mgr_Intro_M1_E6L4".localized()
        self.mgr_m1_e3l5.text = "Mgr_Intro_M1_E6L5".localized()
        self.mgr_m1_e3l6.text = "Mgr_Intro_M1_E7L1".localized()
        self.mgr_m1_e4l1.text = "Mgr_Intro_M1_E9L1".localized()
        
        self.mgr_m1_e4b1.setTitle("Mgr_Intro_M1_E8L1".localized(), for: .normal)
        self.mgr_m1_e4b2.setTitle("Mgr_Intro_M1_E8L2".localized(), for: .normal)
        self.mgr_m1_e4b3.setTitle("Mgr_Intro_M1_E8L3".localized(), for: .normal)
        self.mgr_m1_e4b4.setTitle("Mgr_Intro_M1_E8L4".localized(), for: .normal)
        
        self.mgr_m1_e4b1.isEnabled = false
        self.mgr_m1_e4b2.isEnabled = false
        self.mgr_m1_e4b3.isEnabled = false
        self.mgr_m1_e4b4.isEnabled = false
        
        if UserDefaults.standard.bool(forKey: "isPlusversion") == true {
            // screen 2
            self.mgr_m1_e2l1.removeConstraints(self.mgr_m1_e2l1.constraints)
            
            let width_mgr_m1_e2l1 = NSLayoutConstraint(item: self.mgr_m1_e2l1,
                                                       attribute: NSLayoutAttribute.width,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: nil,
                                                       attribute: NSLayoutAttribute.notAnAttribute,
                                                       multiplier: 1,
                                                       constant: CGFloat(Constants.M1_mgr_m1_e2l1Constraints_6splus[0]))
            
            let top_mgr_m1_e2l1 = NSLayoutConstraint(item: self.mgr_m1_e2l1,
                                                     attribute: NSLayoutAttribute.top,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.top,
                                                     multiplier: 1,
                                                     constant: CGFloat(Constants.M1_mgr_m1_e2l1Constraints_6splus[1]))
            
            
            
            let top_mgr_m1_e2l2 = NSLayoutConstraint(item: self.mgr_m1_e2l2,
                                                     attribute: NSLayoutAttribute.top,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.top,
                                                     multiplier: 1,
                                                     constant: CGFloat(Constants.M1_mgr_m1_e2l2Constraints_6splus[1]))
            
            
            self.mgr_m1_e2l3.removeConstraints(self.mgr_m1_e2l3.constraints)
            
            let top_mgr_m1_e2l3 = NSLayoutConstraint(item: self.mgr_m1_e2l3,
                                                     attribute: NSLayoutAttribute.top,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.top,
                                                     multiplier: 1,
                                                     constant: CGFloat(Constants.M1_mgr_m1_e2l3Constraints_6splus[1]))
            
            let lead_mgr_m1_e2l3 = NSLayoutConstraint(item: self.mgr_m1_e2l3,
                                                      attribute: NSLayoutAttribute.left,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.left,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M1_mgr_m1_e2l3Constraints_6splus[0]))
            
            
            let parentViewConstraints4: [NSLayoutConstraint] = self.view.constraints
            
            for parentConstraint in parentViewConstraints4 {
                if let viewLbl = parentConstraint.firstItem as? UILabel {
                    if viewLbl.text! == "Mgr_Intro_M1_E4L1".localized() {
                        self.view.removeConstraint(parentConstraint)
                    }
                }
            }
            
            self.view.addConstraint(lead_mgr_m1_e2l3)
            self.view.addConstraint(top_mgr_m1_e2l3)
            
            
            self.mgr_m1_e2l4.removeConstraints(self.mgr_m1_e2l4.constraints)
            
            let width_mgr_m1_e2l4 = NSLayoutConstraint(item: self.mgr_m1_e2l4,
                                                       attribute: NSLayoutAttribute.width,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: nil,
                                                       attribute: NSLayoutAttribute.notAnAttribute,
                                                       multiplier: 1,
                                                       constant: CGFloat(Constants.M1_mgr_m1_e2l4Constraints_6splus[0]))
            
            let top_mgr_m1_e2l4 = NSLayoutConstraint(item: self.mgr_m1_e2l4,
                                                     attribute: NSLayoutAttribute.top,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.top,
                                                     multiplier: 1,
                                                     constant: CGFloat(Constants.M1_mgr_m1_e2l4Constraints_6splus[1]))
            
            let lead_mgr_m1_e2l4 = NSLayoutConstraint(item: self.mgr_m1_e2l4,
                                                      attribute: NSLayoutAttribute.left,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.left,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M1_mgr_m1_e2l4Constraints_6splus[2]))
            
            
            
            self.view.addConstraint(top_mgr_m1_e2l1)
            self.view.addConstraint(width_mgr_m1_e2l1)
            
            self.view.addConstraint(top_mgr_m1_e2l2)
            
            
            self.view.addConstraint(top_mgr_m1_e2l4)
            self.view.addConstraint(width_mgr_m1_e2l4)
            self.view.addConstraint(lead_mgr_m1_e2l4)
            
            
            self.mgr_m1_e4b1.removeConstraints(self.mgr_m1_e4b1.constraints)
            let lead_mgr_m1_e4b1 = NSLayoutConstraint(item: self.mgr_m1_e4b1,
                                                      attribute: NSLayoutAttribute.left,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.left,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M1_mgr_m1_e4b1Constraints_6splus[0]))
            let top_mgr_m1_e4b1 = NSLayoutConstraint(item: self.mgr_m1_e4b1,
                                                     attribute: NSLayoutAttribute.top,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.top,
                                                     multiplier: 1,
                                                     constant: CGFloat(Constants.M1_mgr_m1_e4b1Constraints_6splus[1]))
            let height_mgr_m1_e4b1 = NSLayoutConstraint(item: self.mgr_m1_e4b1,
                                                        attribute: NSLayoutAttribute.height,
                                                        relatedBy: NSLayoutRelation.equal,
                                                        toItem: nil,
                                                        attribute: NSLayoutAttribute.notAnAttribute,
                                                        multiplier: 1,
                                                        constant: 120)
            let width_mgr_m1_e4b1 = NSLayoutConstraint(item: self.mgr_m1_e4b1,
                                                       attribute: NSLayoutAttribute.width,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: nil,
                                                       attribute: NSLayoutAttribute.notAnAttribute,
                                                       multiplier: 1,
                                                       constant: 120)
            
            
            
            self.view.addConstraint(top_mgr_m1_e4b1)
            self.view.addConstraint(height_mgr_m1_e4b1)
            self.view.addConstraint(lead_mgr_m1_e4b1)
            self.view.addConstraint(width_mgr_m1_e4b1)
            
            self.mgr_m1_e4b2.removeConstraints(self.mgr_m1_e4b2.constraints)
            let lead_mgr_m1_e4b2 = NSLayoutConstraint(item: self.mgr_m1_e4b2,
                                                      attribute: NSLayoutAttribute.leading,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.leading,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M1_mgr_m1_e4b2Constraints_6splus[0]))
            let top_mgr_m1_e4b2 = NSLayoutConstraint(item: self.mgr_m1_e4b2,
                                                     attribute: NSLayoutAttribute.top,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.top,
                                                     multiplier: 1,
                                                     constant: CGFloat(Constants.M1_mgr_m1_e4b2Constraints_6splus[1]))
            
            let height_mgr_m1_e4b2 = NSLayoutConstraint(item: self.mgr_m1_e4b2,
                                                        attribute: NSLayoutAttribute.height,
                                                        relatedBy: NSLayoutRelation.equal,
                                                        toItem: nil,
                                                        attribute: NSLayoutAttribute.notAnAttribute,
                                                        multiplier: 1,
                                                        constant: 100)
            
            
            let width_mgr_m1_e4b2 = NSLayoutConstraint(item: self.mgr_m1_e4b2,
                                                       attribute: NSLayoutAttribute.width,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: nil,
                                                       attribute: NSLayoutAttribute.notAnAttribute,
                                                       multiplier: 1,
                                                       constant: 100)
            
            
            
            self.view.addConstraint(top_mgr_m1_e4b2)
            self.view.addConstraint(height_mgr_m1_e4b2)
            self.view.addConstraint(lead_mgr_m1_e4b2)
            self.view.addConstraint(width_mgr_m1_e4b2)
            
            
            self.mgr_m1_e4b3.removeConstraints(self.mgr_m1_e4b3.constraints)
            
            let lead_mgr_m1_e4b3 = NSLayoutConstraint(item: self.mgr_m1_e4b3,
                                                      attribute: NSLayoutAttribute.left,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.left,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M1_mgr_m1_e4b3Constraints_6splus[0]))
            
            let top_mgr_m1_e4b3 = NSLayoutConstraint(item: self.mgr_m1_e4b3,
                                                     attribute: NSLayoutAttribute.top,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.top,
                                                     multiplier: 1,
                                                     constant: CGFloat(Constants.M1_mgr_m1_e4b3Constraints_6splus[1]))
            
            let height_mgr_m1_e4b3 = NSLayoutConstraint(item: self.mgr_m1_e4b3,
                                                        attribute: NSLayoutAttribute.height,
                                                        relatedBy: NSLayoutRelation.equal,
                                                        toItem: nil,
                                                        attribute: NSLayoutAttribute.notAnAttribute,
                                                        multiplier: 1,
                                                        constant: 100)
            
            
            let width_mgr_m1_e4b3 = NSLayoutConstraint(item: self.mgr_m1_e4b3,
                                                       attribute: NSLayoutAttribute.width,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: nil,
                                                       attribute: NSLayoutAttribute.notAnAttribute,
                                                       multiplier: 1,
                                                       constant: 100)
            
            
            
            self.view.addConstraint(top_mgr_m1_e4b3)
            self.view.addConstraint(height_mgr_m1_e4b3)
            self.view.addConstraint(lead_mgr_m1_e4b3)
            self.view.addConstraint(width_mgr_m1_e4b3)
            
            
            self.mgr_m1_e4b4.removeConstraints(self.mgr_m1_e4b4.constraints)
            
            let lead_mgr_m1_e4b4 = NSLayoutConstraint(item: self.mgr_m1_e4b4,
                                                      attribute: NSLayoutAttribute.left,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.left,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M1_mgr_m1_e4b4Constraints_6splus[0]))
            
            let top_mgr_m1_e4b4 = NSLayoutConstraint(item: self.mgr_m1_e4b4,
                                                     attribute: NSLayoutAttribute.top,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.top,
                                                     multiplier: 1,
                                                     constant: CGFloat(Constants.M1_mgr_m1_e4b4Constraints_6splus[1]))
            
            let height_mgr_m1_e4b4 = NSLayoutConstraint(item: self.mgr_m1_e4b4,
                                                        attribute: NSLayoutAttribute.height,
                                                        relatedBy: NSLayoutRelation.equal,
                                                        toItem: nil,
                                                        attribute: NSLayoutAttribute.notAnAttribute,
                                                        multiplier: 1,
                                                        constant: 100)
            
            
            let width_mgr_m1_e4b4 = NSLayoutConstraint(item: self.mgr_m1_e4b4,
                                                       attribute: NSLayoutAttribute.width,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: nil,
                                                       attribute: NSLayoutAttribute.notAnAttribute,
                                                       multiplier: 1,
                                                       constant: 100)
            
            
            
            self.view.addConstraint(top_mgr_m1_e4b4)
            self.view.addConstraint(height_mgr_m1_e4b4)
            self.view.addConstraint(lead_mgr_m1_e4b4)
            self.view.addConstraint(width_mgr_m1_e4b4)
        }

        intervalVal = 8
        self.bulbNo = 1
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module1IntroController.screen1), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module1IntroController.monitorTimer), userInfo: nil, repeats: true)
        
        if self.revealViewController() != nil {
            menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: UIControlEvents.touchUpInside)
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            
        }
        
        do{
            let urlVideo = Bundle.main.url(forResource: "MGR_M01_S01", withExtension: "mp3")
            audioPlayerIntroAud = try AVAudioPlayer(contentsOf: urlVideo!)
            audioPlayerIntroAud!.delegate = self
            audioPlayerIntroAud!.prepareToPlay()
            audioPlayerIntroAud!.play()
        } catch {
            print("Error getting the audio file")
        }
        
    }
    
    func monitorTimer() {
        
        self.intervalVal = self.intervalVal - 0.15
        print("Interval in monitor: " + String(self.intervalVal))
    }
    
    func screen1(){
        
        print("In screen1")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m1_intro2__1")
        self.mgr_m1_e1l1.isHidden = true
        self.mgr_m1_e2l1.isHidden = false
        
        self.intervalVal = 8
        self.bulbNo = 2
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module1IntroController.screen2), userInfo: nil, repeats: false)
        
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module1IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen2(){
        
        print("In screen2")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m1_intro2__2")
        
        self.mgr_m1_e2l2.isHidden = false
        
        //        UIView.transition(with: self.intro_m1e3l1,
        //                          duration: 2.0,
        //                          options: [.transitionCrossDissolve],
        //                          animations: {
        //                            self.intro_m1e3l1.text = "Mgr_Intro_M1_E3L1".localized
        //            }, completion: nil)
        
        self.intervalVal = 5
        self.bulbNo = 3
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module1IntroController.screen3), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module1IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen3(){
        
        print("In screen3")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m1_intro2__3")
        
        self.mgr_m1_e2l3.isHidden = false
        
        //        UIView.transition(with: self.intro_m1e3l1,
        //                          duration: 2.0,
        //                          options: [.transitionCrossDissolve],
        //                          animations: {
        //                            self.intro_m1e3l1.text = "Mgr_Intro_M1_E3L1".localized
        //            }, completion: nil)
        
        self.intervalVal = 6
        self.bulbNo = 4
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module1IntroController.screen4), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module1IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen4(){
        
        print("In screen4")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m1_intro2")
        
        self.mgr_m1_e2l4.isHidden = false
        
        //        UIView.transition(with: self.intro_m1e3l1,
        //                          duration: 2.0,
        //                          options: [.transitionCrossDissolve],
        //                          animations: {
        //                            self.intro_m1e3l1.text = "Mgr_Intro_M1_E3L1".localized
        //            }, completion: nil)
        
        self.intervalVal = 7
        self.bulbNo = 5
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module1IntroController.screen5), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module1IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen5(){
        
        print("In screen5")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m1_intro3")
        
        self.mgr_m1_e2l1.isHidden = true
        self.mgr_m1_e2l2.isHidden = true
        self.mgr_m1_e2l3.isHidden = true
        self.mgr_m1_e2l4.isHidden = true
        
        self.mgr_m1_e3l1.isHidden = false
        self.mgr_m1_e3l2.isHidden = false
        self.mgr_m1_e3l3.isHidden = false
        self.mgr_m1_e3l4.isHidden = false
        self.mgr_m1_e3l5.isHidden = false
        
        //        UIView.transition(with: self.intro_m1e3l1,
        //                          duration: 2.0,
        //                          options: [.transitionCrossDissolve],
        //                          animations: {
        //                            self.intro_m1e3l1.text = "Mgr_Intro_M1_E3L1".localized
        //            }, completion: nil)
        
        self.intervalVal = 10
        self.bulbNo = 6
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module1IntroController.screen6), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module1IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen6(){
        
        print("In screen6")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.mgr_m1_e3l6.isHidden = false
        
        //        UIView.transition(with: self.intro_m1e3l1,
        //                          duration: 2.0,
        //                          options: [.transitionCrossDissolve],
        //                          animations: {
        //                            self.intro_m1e3l1.text = "Mgr_Intro_M1_E3L1".localized
        //            }, completion: nil)
        
        self.intervalVal = 6
        self.bulbNo = 7
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module1IntroController.screen7), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module1IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen7(){
        
        print("In screen7")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m1_intro4")
        
        self.mgr_m1_e3l1.isHidden = true
        self.mgr_m1_e3l2.isHidden = true
        self.mgr_m1_e3l3.isHidden = true
        self.mgr_m1_e3l4.isHidden = true
        self.mgr_m1_e3l5.isHidden = true
        self.mgr_m1_e3l6.isHidden = true
        
        self.mgr_m1_e4b1.isHidden = false
        self.mgr_m1_e4b2.isHidden = false
        self.mgr_m1_e4b3.isHidden = false
        self.mgr_m1_e4b4.isHidden = false
        
        self.mgr_m1_e4b1.isEnabled = false
        self.mgr_m1_e4b2.isEnabled = false
        self.mgr_m1_e4b3.isEnabled = false
        self.mgr_m1_e4b4.isEnabled = false
        
        //        UIView.transition(with: self.intro_m1e3l1,
        //                          duration: 2.0,
        //                          options: [.transitionCrossDissolve],
        //                          animations: {
        //                            self.intro_m1e3l1.text = "Mgr_Intro_M1_E3L1".localized
        //            }, completion: nil)
        
        self.intervalVal = 12
        self.bulbNo = 8
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module1IntroController.screen8), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module1IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen8(){
        
        print("In screen8")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.mgr_m1_e4l1.isHidden = false
        
        //        UIView.transition(with: self.intro_m1e3l1,
        //                          duration: 2.0,
        //                          options: [.transitionCrossDissolve],
        //                          animations: {
        //                            self.intro_m1e3l1.text = "Mgr_Intro_M1_E3L1".localized
        //            }, completion: nil)
        
        self.intervalVal = 4
        self.bulbNo = 9
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module1IntroController.screen9), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module1IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen9(){
        
        print("In screen9")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.mgr_m1_e4b1.isEnabled = true
        self.mgr_m1_e4b2.isEnabled = true
        self.mgr_m1_e4b3.isEnabled = true
        self.mgr_m1_e4b4.isEnabled = true
        
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("In Finish Playing")
        playFlagIntroAud = 3
        if let image = UIImage(named: "ic_replay_white_36pt.png") {
            multimediaIntro.setImage(image, for: .normal)
        }
    }
    
    @IBAction func multimediaIntroClicked(_ sender: AnyObject) {
        //        let layer = introductionImage.layer
        
        if (playFlagIntroAud == 3) {
            
            self.introductionImage.image = UIImage(named: "mgr_m1_intro1")
            
            self.mgr_m1_e1l1.isHidden = false
            self.mgr_m1_e2l1.isHidden = true
            self.mgr_m1_e2l2.isHidden = true
            self.mgr_m1_e2l3.isHidden = true
            self.mgr_m1_e2l4.isHidden = true
            self.mgr_m1_e3l1.isHidden = true
            self.mgr_m1_e3l2.isHidden = true
            self.mgr_m1_e3l3.isHidden = true
            self.mgr_m1_e3l4.isHidden = true
            self.mgr_m1_e3l5.isHidden = true
            self.mgr_m1_e3l6.isHidden = true
            self.mgr_m1_e4l1.isHidden = true
            
            self.mgr_m1_e4b1.isHidden = true
            self.mgr_m1_e4b2.isHidden = true
            self.mgr_m1_e4b3.isHidden = true
            self.mgr_m1_e4b4.isHidden = true
            
            self.mgr_m1_e1l1.text = "Mgr_Intro_M1_E1L1".localized()
            self.mgr_m1_e2l1.text = "Mgr_Intro_M1_E2L1".localized()
            self.mgr_m1_e2l2.text = "Mgr_Intro_M1_E3L1".localized()
            self.mgr_m1_e2l3.text = "Mgr_Intro_M1_E4L1".localized()
            self.mgr_m1_e2l4.text = "Mgr_Intro_M1_E5L1".localized()
            self.mgr_m1_e3l1.text = "Mgr_Intro_M1_E6L1".localized()
            self.mgr_m1_e3l2.text = "Mgr_Intro_M1_E6L2".localized()
            self.mgr_m1_e3l3.text = "Mgr_Intro_M1_E6L3".localized()
            self.mgr_m1_e3l4.text = "Mgr_Intro_M1_E6L4".localized()
            self.mgr_m1_e3l5.text = "Mgr_Intro_M1_E6L5".localized()
            self.mgr_m1_e3l6.text = "Mgr_Intro_M1_E7L1".localized()
            
            self.mgr_m1_e4b1.setTitle("Mgr_Intro_M1_E8L1".localized(), for: .normal)
            self.mgr_m1_e4b2.setTitle("Mgr_Intro_M1_E8L2".localized(), for: .normal)
            self.mgr_m1_e4b3.setTitle("Mgr_Intro_M1_E8L3".localized(), for: .normal)
            self.mgr_m1_e4b4.setTitle("Mgr_Intro_M1_E8L4".localized(), for: .normal)
            
            self.mgr_m1_e4b1.isEnabled = false
            self.mgr_m1_e4b2.isEnabled = false
            self.mgr_m1_e4b3.isEnabled = false
            self.mgr_m1_e4b4.isEnabled = false
            
            intervalVal = 8
            self.bulbNo = 1
            
            let timeInterval: TimeInterval = TimeInterval(intervalVal)
            self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module1IntroController.screen1), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module1IntroController.monitorTimer), userInfo: nil, repeats: true)
            
            playFlagIntroAud = 1
            if let image = UIImage(named: "ic_pause_white_36pt.png") {
                multimediaIntro.setImage(image, for: .normal)
                audioPlayerIntroAud?.play()
                //                resumeLayer(layer: layer)
            }
        } else if (playFlagIntroAud == 2) {
            
            let timeInterval: TimeInterval = TimeInterval(self.intervalVal)
            print("Interval: " + String(self.intervalVal))
            
            switch self.bulbNo {
            case 1: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module1IntroController.screen1), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module1IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 2: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module1IntroController.screen2), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module1IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 3: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module1IntroController.screen3), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module1IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 4: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module1IntroController.screen4), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module1IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 5: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module1IntroController.screen5), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module1IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 6: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module1IntroController.screen6), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module1IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 7: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module1IntroController.screen7), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module1IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 8: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module1IntroController.screen8), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module1IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 9: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module1IntroController.screen9), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module1IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            default: break
            }
            
            playFlagIntroAud = 1
            if let image = UIImage(named: "ic_pause_white_36pt.png") {
                multimediaIntro.setImage(image, for: .normal)
                audioPlayerIntroAud?.play()
            }
        } else {
            
            self.GTTimer.invalidate()
            self.GTTimer2.invalidate()
            
            playFlagIntroAud = 2
            if let image = UIImage(named: "ic_play_arrow_white_36pt.png") {
                multimediaIntro.setImage(image, for: .normal)
                audioPlayerIntroAud?.pause()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        audioPlayerIntroAud?.stop()
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
    }
    
}
