//
//  Module0IntroController.swift
//  6one5Manager
//
//  Created by Sumit More on 12/20/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import AVFoundation

class Module0IntroController: UIViewController, AVAudioPlayerDelegate {
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var multimediaIntro: UIButton!
    @IBOutlet weak var introductionImage: UIImageView!
    @IBOutlet weak var introBtmLbl: UILabel!
    
    @IBOutlet weak var mgr_m0_e1l1: UILabel!
    @IBOutlet weak var mgr_m0_e2l1: UILabel!
    @IBOutlet weak var mgr_m0_e4l1: UILabel!
    @IBOutlet weak var mgr_m0_e5l1: UILabel!
    @IBOutlet weak var mgr_m0_e6l1: UILabel!
    @IBOutlet weak var mgr_m0_e8l1: UILabel!
    @IBOutlet weak var mgr_m0_e8l2: UILabel!
    @IBOutlet weak var mgr_m0_e8l3: UILabel!
    @IBOutlet weak var mgr_m0_e8l4: UILabel!
    @IBOutlet weak var mgr_m0_e17l1: UILabel!
    @IBOutlet weak var mgr_m0_e17l2: UILabel!
    @IBOutlet weak var mgr_m0_e17l3: UILabel!
    @IBOutlet weak var mgr_m0_e17l4: UILabel!
    @IBOutlet weak var mgr_m0_e17l5: UILabel!
    @IBOutlet weak var mgr_m0_e22l1: UILabel!
    @IBOutlet weak var mgr_m0_e23l1: UILabel!
    @IBOutlet weak var mgr_m0_e24l1: UILabel!
    
    
    var playFlagIntroAud = 1
    var audioPlayerIntroAud: AVAudioPlayer?
    
    var GTTimer : Timer = Timer()
    var GTTimer2 : Timer = Timer()
    var isPaused = false
    var intervalVal: Double = 5
    var intervalVal2: Double = 0.15
    var bulbNo: Int = 0
    var questionNo: Int = 1
    
    var timeIntervals = [7,5,4,3,2,5,6,2,2,3,4,7,10,6,8,7,9,10,8,8,6,3,3]
    
    override func viewDidLoad() {
        
        self.introBtmLbl.text = "Mgr_M0_IntroductionBtmLbl".localized()
        
        let device = UIDevice.current.model
        print("Device type: " + device)
        
        if(device == "iPhone") {
            print("Changing size to 16")
            let attrs = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont.systemFont(ofSize: 14.0)
            ]
            self.navigationController!.navigationBar.titleTextAttributes = attrs
            
            let button: UIButton = UIButton(type: UIButtonType.custom)
            button.setImage(UIImage(named: "sof.png"), for: UIControlState.normal)
            button.backgroundColor = UIColor.white
            button.frame = CGRect(x: 0, y: 0, width: 56, height: 45)
            let barButton = UIBarButtonItem(customView: button)
            
            
            let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            negativeSpacer.width = -17;
            
            self.navigationItem.setRightBarButtonItems([negativeSpacer, barButton ], animated: false)
            
        } else {
            print("Changing size to 32")
            let attrs = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont.systemFont(ofSize: 32.0)
            ]
            self.navigationController!.navigationBar.titleTextAttributes = attrs
            
            let button: UIButton = UIButton(type: UIButtonType.custom)
            button.setImage(UIImage(named: "sof.png"), for: UIControlState.normal)
            button.backgroundColor = UIColor.white
            //set frame
            button.frame = CGRect(x: 0, y: 0, width: 56, height: 45)
            let barButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            
            
            let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            negativeSpacer.width = -21;
            
            self.navigationItem.setRightBarButtonItems([negativeSpacer, barButton ], animated: false)
        }
        
        self.navigationItem.title = "Mgr_M0_ModuleName".localized()
        
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 36.0/255.0, green: 184.0/255.0, blue: 239.0/255.0, alpha: 1.0)
        
        self.introductionImage.image = UIImage(named: "mgr_m0_intro14")
        
        self.mgr_m0_e1l1.isHidden = false
        self.mgr_m0_e2l1.isHidden = true
        self.mgr_m0_e4l1.isHidden = true
        self.mgr_m0_e5l1.isHidden = true
        self.mgr_m0_e6l1.isHidden = true
        self.mgr_m0_e8l1.isHidden = true
        self.mgr_m0_e8l2.isHidden = true
        self.mgr_m0_e8l3.isHidden = true
        self.mgr_m0_e8l4.isHidden = true
        self.mgr_m0_e17l1.isHidden = true
        self.mgr_m0_e17l2.isHidden = true
        self.mgr_m0_e17l3.isHidden = true
        self.mgr_m0_e17l4.isHidden = true
        self.mgr_m0_e17l5.isHidden = true
        self.mgr_m0_e22l1.isHidden = true
        self.mgr_m0_e23l1.isHidden = true
        self.mgr_m0_e24l1.isHidden = true
        
        self.mgr_m0_e1l1.text = "Mgr_M0_E1L1".localized()
        self.mgr_m0_e2l1.text = "Mgr_M0_E2L1".localized()
        self.mgr_m0_e4l1.text = "Mgr_M0_E4L1".localized()
        self.mgr_m0_e5l1.text = "Mgr_M0_E5L1".localized()
        self.mgr_m0_e6l1.text = "Mgr_M0_E6L1".localized()
        self.mgr_m0_e8l1.text = "Mgr_M0_E8L1".localized()
        self.mgr_m0_e8l2.text = "Mgr_M0_E8L2".localized()
        self.mgr_m0_e8l3.text = "Mgr_M0_E8L3".localized()
        self.mgr_m0_e8l4.text = "Mgr_M0_E8L4".localized()
        self.mgr_m0_e17l1.text = "Mgr_M0_E17L1".localized()
        self.mgr_m0_e17l2.text = "Mgr_M0_E17L2".localized()
        self.mgr_m0_e17l3.text = "Mgr_M0_E17L3".localized()
        self.mgr_m0_e17l4.text = "Mgr_M0_E17L4".localized()
        self.mgr_m0_e17l5.text = "Mgr_M0_E17L5".localized()
        self.mgr_m0_e22l1.text = "Mgr_M0_E22L1".localized()
        self.mgr_m0_e23l1.text = "Mgr_M0_E23L1".localized()
        self.mgr_m0_e24l1.text = "Mgr_M0_E24L1".localized()
        
        if UserDefaults.standard.bool(forKey: "isPlusversion") == true {
            
            // screen 1
            
            self.mgr_m0_e1l1.removeConstraints(self.mgr_m0_e1l1.constraints)
            
            let top_mgr_m0_e1l1 = NSLayoutConstraint(item: self.mgr_m0_e1l1,
                                                     attribute: NSLayoutAttribute.top,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.top,
                                                     multiplier: 1,
                                                     constant: CGFloat(Constants.M0_mgr_m0_e1l1Constraints_6splus[1]))
            
            
            
            let top_mgr_m0_e2l1 = NSLayoutConstraint(item: self.mgr_m0_e2l1,
                                                     attribute: NSLayoutAttribute.top,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.top,
                                                     multiplier: 1,
                                                     constant: CGFloat(Constants.M0_mgr_m0_e2l1Constraints_6splus[1]))
            
            
            
            //  self.view.addConstraint(lead_mgr_m0_e1l1)
            self.view.addConstraint(top_mgr_m0_e1l1)
            self.view.addConstraint(top_mgr_m0_e2l1)
            
            // screen 2
            self.mgr_m0_e4l1.removeConstraints(self.mgr_m0_e4l1.constraints)
            let top_mgr_m0_e4l1 = NSLayoutConstraint(item: self.mgr_m0_e4l1,
                                                     attribute: NSLayoutAttribute.top,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.top,
                                                     multiplier: 1,
                                                     constant: CGFloat(Constants.M0_mgr_m0_e4l1Constraints_6splus[1]))
            
            
            
            let top_mgr_m0_e5l1 = NSLayoutConstraint(item: self.mgr_m0_e5l1,
                                                     attribute: NSLayoutAttribute.top,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.top,
                                                     multiplier: 1,
                                                     constant: CGFloat(Constants.M0_mgr_m0_e5l1Constraints_6splus[1]))
            
            
            let top_mgr_m0_e6l1 = NSLayoutConstraint(item: self.mgr_m0_e6l1,
                                                     attribute: NSLayoutAttribute.top,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.top,
                                                     multiplier: 1,
                                                     constant: CGFloat(Constants.M0_mgr_m0_e6l1Constraints_6splus[1]))
            
            self.view.addConstraint(top_mgr_m0_e4l1)
            self.view.addConstraint(top_mgr_m0_e5l1)
            self.view.addConstraint(top_mgr_m0_e6l1)
            
            
            // Screen 3
            
            self.mgr_m0_e8l1.removeConstraints(self.mgr_m0_e8l1.constraints)
            let lead_mgr_m0_e8l1 = NSLayoutConstraint(item: self.mgr_m0_e8l1,
                                                     attribute: NSLayoutAttribute.left,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.left,
                                                     multiplier: 1,
                                                     constant: CGFloat(Constants.M0_mgr_m0_e8l1Constraints_6splus[0]))
            
            let top_mgr_m0_e8l1 = NSLayoutConstraint(item: self.mgr_m0_e8l1,
                                                     attribute: NSLayoutAttribute.top,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.top,
                                                     multiplier: 1,
                                                     constant: CGFloat(Constants.M0_mgr_m0_e8l1Constraints_6splus[1]))
            
            
            
            let top_mgr_m0_e8l2 = NSLayoutConstraint(item: self.mgr_m0_e8l2,
                                                     attribute: NSLayoutAttribute.top,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.top,
                                                     multiplier: 1,
                                                     constant: CGFloat(Constants.M0_mgr_m0_e8l2Constraints_6splus[1]))
            
            
            let top_mgr_m0_e8l3 = NSLayoutConstraint(item: self.mgr_m0_e8l3,
                                                     attribute: NSLayoutAttribute.top,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.top,
                                                     multiplier: 1,
                                                     constant: CGFloat(Constants.M0_mgr_m0_e8l3Constraints_6splus[1]))
            
            let lead_mgr_m0_e8l4 = NSLayoutConstraint(item: self.mgr_m0_e8l4,
                                                     attribute: NSLayoutAttribute.left,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.left,
                                                     multiplier: 1,
                                                     constant: CGFloat(Constants.M0_mgr_m0_e8l4Constraints_6splus[0]))
            let top_mgr_m0_e8l4 = NSLayoutConstraint(item: self.mgr_m0_e8l4,
                                                     attribute: NSLayoutAttribute.top,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.top,
                                                     multiplier: 1,
                                                     constant: CGFloat(Constants.M0_mgr_m0_e8l4Constraints_6splus[1]))
            
            
            
            self.view.addConstraint(lead_mgr_m0_e8l1)
            self.view.addConstraint(top_mgr_m0_e8l1)
            self.view.addConstraint(top_mgr_m0_e8l2)
            self.view.addConstraint(top_mgr_m0_e8l3)
            self.view.addConstraint(lead_mgr_m0_e8l4)
            self.view.addConstraint(top_mgr_m0_e8l4)
            
            
            
            // Screen 8
            
            self.mgr_m0_e17l1.removeConstraints(self.mgr_m0_e17l1.constraints)
            let top_mgr_m0_e17l1 = NSLayoutConstraint(item: self.mgr_m0_e17l1,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M0_mgr_m0_e17l1Constraints_6splus[1]))
            
            
            
            let top_mgr_m0_e17l2 = NSLayoutConstraint(item: self.mgr_m0_e17l2,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M0_mgr_m0_e17l2Constraints_6splus[1]))
            
            
            let top_mgr_m0_e17l3 = NSLayoutConstraint(item: self.mgr_m0_e17l3,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M0_mgr_m0_e17l3Constraints_6splus[1]))
            
            let top_mgr_m0_e17l4 = NSLayoutConstraint(item: self.mgr_m0_e17l4,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M0_mgr_m0_e17l4Constraints_6splus[1]))
            
            let top_mgr_m0_e17l5 = NSLayoutConstraint(item: self.mgr_m0_e17l5,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M0_mgr_m0_e17l5Constraints_6splus[1]))
            
            
            
            self.view.addConstraint(top_mgr_m0_e17l1)
            self.view.addConstraint(top_mgr_m0_e17l2)
            self.view.addConstraint(top_mgr_m0_e17l3)
            self.view.addConstraint(top_mgr_m0_e17l4)
            self.view.addConstraint(top_mgr_m0_e17l5)
            
            
            // screen 9
            
            let top_mgr_m0_e22l1 = NSLayoutConstraint(item: self.mgr_m0_e22l1,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M0_mgr_m0_e22l1Constraints_6splus[1]))
            
            let top_mgr_m0_e23l1 = NSLayoutConstraint(item: self.mgr_m0_e23l1,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M0_mgr_m0_e23l1Constraints_6splus[1]))
            
            
            
            self.view.addConstraint(top_mgr_m0_e22l1)
            self.view.addConstraint(top_mgr_m0_e23l1)
            
            
            
        }

        self.bulbNo = 0
        //self.intervalVal = self.timeIntervals[self.bulbNo]
        self.intervalVal = 8
        self.bulbNo = 1
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen1), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
        
        if self.revealViewController() != nil {
            menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: UIControlEvents.touchUpInside)
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            
        }
        
        do{
            let urlVideo = Bundle.main.url(forResource: "MGR_M00_S01", withExtension: "mp3")
            audioPlayerIntroAud = try AVAudioPlayer(contentsOf: urlVideo!)
            audioPlayerIntroAud!.delegate = self
            audioPlayerIntroAud!.prepareToPlay()
            audioPlayerIntroAud?.volume = 1
            do {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            }
            catch {
                // report for an error
            }
            audioPlayerIntroAud!.play()
        } catch {
            print("Error getting the audio file")
        }
        
    }
    
    func monitorTimer() {
        self.intervalVal = self.intervalVal - 0.15
        print("Interval in monitor: " + String(self.intervalVal))
    }
    
    func screen1(){
        
        print("In screen1")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m0_intro11")
        
        self.mgr_m0_e2l1.isHidden = false
        self.intervalVal = 6
        //self.intervalVal = self.timeIntervals[self.bulbNo]
        self.bulbNo = 2
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen2), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen2(){
        
        print("In screen2")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m0_intro13")
        
        self.mgr_m0_e1l1.isHidden = true
        self.mgr_m0_e2l1.isHidden = true
        self.intervalVal = 5
        //self.intervalVal = self.timeIntervals[self.bulbNo]
        self.bulbNo = 3
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen3), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen3(){
        
        print("In screen3")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m0_intro1_1")
        
        self.mgr_m0_e4l1.isHidden = false
        
        // self.intervalVal = self.timeIntervals[self.bulbNo]
        self.intervalVal = 4
        self.bulbNo = 4
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen4), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen4(){
        
        print("In screen4")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m0_intro1_2")
        
        self.mgr_m0_e5l1.isHidden = false
        
        self.intervalVal = 3
        self.bulbNo = 5
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen5), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen5(){
        
        print("In screen5")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m0_intro1")
        
        self.mgr_m0_e6l1.isHidden = false
        
        self.intervalVal = 3
        self.bulbNo = 6
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen6), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen6(){
        
        print("In screen6")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        //self.introductionImage.image = UIImage(named: "mgr_m0_intro1")
        
        //self.intervalVal = self.timeIntervals[self.bulbNo]
        self.intervalVal = 3
        self.bulbNo = 7
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen7), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen7(){
        
        print("In screen7")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m0_intro2")
        
        self.mgr_m0_e4l1.isHidden = true
        self.mgr_m0_e5l1.isHidden = true
        self.mgr_m0_e6l1.isHidden = true
        
        self.mgr_m0_e8l1.isHidden = false
        self.mgr_m0_e8l2.isHidden = false
        self.mgr_m0_e8l3.isHidden = false
        self.mgr_m0_e8l4.isHidden = false
        
        // self.intervalVal = self.timeIntervals[self.bulbNo]
        self.intervalVal = 7
        
        self.bulbNo = 8
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen8), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen8(){
        
        print("In screen8")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.mgr_m0_e8l1.isHidden = true
        self.mgr_m0_e8l2.isHidden = true
        self.mgr_m0_e8l3.isHidden = true
        self.mgr_m0_e8l4.isHidden = true
        
        self.introductionImage.image = UIImage(named: "mgr_m0_intro3")
        
        //  self.intervalVal = self.timeIntervals[self.bulbNo]
        self.intervalVal = 3
        self.bulbNo = 9
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen9), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen9(){
        
        print("In screen9")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m0_intro3_1")
        
        // self.intervalVal = self.timeIntervals[self.bulbNo]
        self.intervalVal = 3
        self.bulbNo = 10
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen10), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
    }
    
    func screen10(){
        
        print("In screen10")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m0_intro3_2")
        
        // self.intervalVal = self.timeIntervals[self.bulbNo]
        self.intervalVal = 4
        self.bulbNo = 11
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen11), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
    }
    
    func screen11(){
        
        print("In screen11")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m0_intro3_3")
        
        //self.intervalVal = self.timeIntervals[self.bulbNo]
        self.intervalVal = 5
        self.bulbNo = 12
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen12), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
    }
    
    func screen12(){
        
        print("In screen12")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m0_intro3_4")
        
        // self.intervalVal = self.timeIntervals[self.bulbNo]
        self.intervalVal = 8
        self.bulbNo = 13
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen13), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
    }
    
    func screen13(){
        
        print("In screen13")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m0_intro4")
        
        // self.intervalVal = self.timeIntervals[self.bulbNo]
        self.intervalVal = 11
        self.bulbNo = 14
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen14), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
    }
    
    func screen14(){
        
        print("In screen14")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m0_intro5")
        
        // self.intervalVal = self.timeIntervals[self.bulbNo]
        self.intervalVal = 7
        self.bulbNo = 15
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen15), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
    }
    
    func screen15(){
        
        print("In screen15")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m0_intro6")
        
        // self.intervalVal = self.timeIntervals[self.bulbNo]
        self.intervalVal = 9
        self.bulbNo = 16
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen16), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
    }
    
    func screen16(){
        
        print("In screen16")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        self.introductionImage.image = UIImage(named: "mgr_m0_intro9")
        
        // self.introductionImage.image = UIImage(named: "mgr_m0_intro7")
        
        self.mgr_m0_e17l1.isHidden = false
        self.mgr_m0_e17l2.isHidden = false
        self.mgr_m0_e17l3.isHidden = false
        self.mgr_m0_e17l4.isHidden = false
        self.mgr_m0_e17l5.isHidden = false
        
        // self.intervalVal = self.timeIntervals[self.bulbNo]
        self.intervalVal = 8
        self.bulbNo = 17
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen17), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
    }
    
    func screen17(){
        
        print("In screen17")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        //self.intervalVal = self.timeIntervals[self.bulbNo]
        self.intervalVal = 10
        self.bulbNo = 18
        
        self.introductionImage.image = UIImage(named: "mgr_m0_intro7")
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen18), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
    }
    
    func screen18(){
        
        print("In screen18")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m0_intro8")
        
        self.mgr_m0_e17l1.isHidden = true
        self.mgr_m0_e17l2.isHidden = true
        self.mgr_m0_e17l3.isHidden = true
        self.mgr_m0_e17l4.isHidden = true
        self.mgr_m0_e17l5.isHidden = true
        
        //self.intervalVal = self.timeIntervals[self.bulbNo]
        self.intervalVal = 11
        self.bulbNo = 19
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen19), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
    }
    
    func screen19(){
        
        print("In screen19")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m0_intro9")
        
        self.mgr_m0_e17l1.isHidden = false
        self.mgr_m0_e17l2.isHidden = false
        self.mgr_m0_e17l3.isHidden = false
        self.mgr_m0_e17l4.isHidden = false
        self.mgr_m0_e17l5.isHidden = false
        
        // self.intervalVal = self.timeIntervals[self.bulbNo]
        self.intervalVal = 9
        self.bulbNo = 20
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen20), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
    }
    
    func screen20(){
        
        print("In screen20")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m0_intro10")
        
        // self.intervalVal = self.timeIntervals[self.bulbNo]
        self.intervalVal = 9
        self.bulbNo = 21
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen21), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
    }
    
    func screen21(){
        
        print("In screen21")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m0_intro120")
        
        self.mgr_m0_e17l1.isHidden = true
        self.mgr_m0_e17l2.isHidden = true
        self.mgr_m0_e17l3.isHidden = true
        self.mgr_m0_e17l4.isHidden = true
        self.mgr_m0_e17l5.isHidden = true
        
        self.mgr_m0_e22l1.isHidden = false
        
        // self.intervalVal = self.timeIntervals[self.bulbNo]
        self.intervalVal = 7
        self.bulbNo = 22
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen22), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
    }
    
    func screen22(){
        
        print("In screen22")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m0_intro12")
        
        self.mgr_m0_e23l1.isHidden = false
        
        //self.intervalVal = self.timeIntervals[self.bulbNo]
        self.intervalVal = 4
        self.bulbNo = 23
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen23), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
    }
    
    func screen23(){
        
        print("In screen23")
        
        self.introductionImage.image = UIImage(named: "mgr_m0_intro24")
        
        self.mgr_m0_e22l1.isHidden = true
        self.mgr_m0_e23l1.isHidden = true
        
        self.mgr_m0_e24l1.isHidden = false
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("In Finish Playing")
        playFlagIntroAud = 3
        if let image = UIImage(named: "ic_replay_white_36pt.png") {
            multimediaIntro.setImage(image, for: .normal)
        }
    }
    
    @IBAction func multimediaIntroClicked(_ sender: AnyObject) {
        //        let layer = introductionImage.layer
        
        if (playFlagIntroAud == 3) {
            
            self.introductionImage.image = UIImage(named: "mgr_m0_intro14")
            
            self.mgr_m0_e1l1.isHidden = false
            self.mgr_m0_e2l1.isHidden = true
            self.mgr_m0_e4l1.isHidden = true
            self.mgr_m0_e5l1.isHidden = true
            self.mgr_m0_e6l1.isHidden = true
            self.mgr_m0_e8l1.isHidden = true
            self.mgr_m0_e8l2.isHidden = true
            self.mgr_m0_e8l3.isHidden = true
            self.mgr_m0_e8l4.isHidden = true
            self.mgr_m0_e17l1.isHidden = true
            self.mgr_m0_e17l2.isHidden = true
            self.mgr_m0_e17l3.isHidden = true
            self.mgr_m0_e17l4.isHidden = true
            self.mgr_m0_e17l5.isHidden = true
            self.mgr_m0_e22l1.isHidden = true
            self.mgr_m0_e23l1.isHidden = true
            self.mgr_m0_e24l1.isHidden = true
            
            intervalVal = 8
            self.bulbNo = 1
            
            let timeInterval: TimeInterval = TimeInterval(intervalVal)
            self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen1), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
            
            playFlagIntroAud = 1
            if let image = UIImage(named: "ic_pause_white_36pt.png") {
                multimediaIntro.setImage(image, for: .normal)
                audioPlayerIntroAud?.play()
                //                resumeLayer(layer: layer)
            }
        } else if (playFlagIntroAud == 2) {
            
            let timeInterval: TimeInterval = TimeInterval(self.intervalVal)
            print("Interval: " + String(self.intervalVal))
            
            switch self.bulbNo {
            case 1: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen1), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 2: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen2), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 3: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen3), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 4: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen4), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 5: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen5), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 6: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen6), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 7: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen7), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 8: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen8), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 9: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen9), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 10: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen10), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 11: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen11), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 12: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen12), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 13: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen13), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 14: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen14), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 15: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen15), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 16: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen16), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 17: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen17), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 18: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen18), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 19: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen19), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 20: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen20), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 21: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen21), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 22: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen22), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 23: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen23), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            default: break
            }
            
            playFlagIntroAud = 1
            if let image = UIImage(named: "ic_pause_white_36pt.png") {
                multimediaIntro.setImage(image, for: .normal)
                audioPlayerIntroAud?.play()
            }
        } else {
            
            self.GTTimer.invalidate()
            self.GTTimer2.invalidate()
            
            playFlagIntroAud = 2
            if let image = UIImage(named: "ic_play_arrow_white_36pt.png") {
                multimediaIntro.setImage(image, for: .normal)
                audioPlayerIntroAud?.pause()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        audioPlayerIntroAud?.stop()
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
    }
    
}
