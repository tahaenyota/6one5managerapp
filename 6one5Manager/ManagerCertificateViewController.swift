//
//  ManagerCertificateViewController.swift
//  6one5Manager
//
//  Created by enyotalearning on 30/11/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//
import UIKit

class ManagerCertificateViewController: UIViewController {
    

    @IBOutlet var certificateMainView: UIView!
    let certificates = Certificate.sharedInstance
    
    @IBOutlet weak var certImageView: UIImageView!
    
    @IBOutlet weak var downloadBtn: UIButton!
    @IBOutlet weak var shareBtn: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
//        certificateMainView.layer.shadowColor = UIColor.gray.cgColor
//        certificateMainView.layer.shadowOpacity = 1
//        certificateMainView.layer.shadowOffset = CGSize.zero
//        certificateMainView.layer.shadowRadius = 3
        
        downloadBtn.isEnabled = false
        downloadBtn.alpha = 0.7
        
        shareBtn.isEnabled = false
        shareBtn.alpha = 0.7
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadCertificateData()
        
        
    }
    
    @IBAction func shareBtnTapped(_ sender: AnyObject)
    {
        //TODO: Share button funcanality
        
        let cert = certificates.certificates[0]
        //cert.image = UIImage(named: "cert.png")!
        // set up activity view controller
        let imageToShare = [ cert.image ]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    @IBAction func downloadBtnTapped(_ sender: AnyObject)
    {
        //TODO: Download button funcanality
        var certImage = certificates.certificates[0].image
        certImage = self.certImageView.image
        UIImageWriteToSavedPhotosAlbum(certImage!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        
    }
    func loadCertificateData()
    {
        
        
        if (self.certificates.certificates.count >= 0 )
        {
            certificates.freeCertificateData()
        }
        let progressHUD = ProgressHUD()
        progressHUD.setCustomText(text: "loadingdata".localized())
        self.view.addSubview(progressHUD)
        progressHUD.activityIndictor.color = UIColor.white
        UIApplication.shared.beginIgnoringInteractionEvents()
        let headers = [
            "cache-control": "no-cache",
            ]
        
        //let prefs:UserDefaults = UserDefaults.standard
        //var user_id = "4"
        //user_id = prefs.value(forKey: "user_id") as! String!
        
        let login = Login.sharedInstance
        
        let apiURL = Constants.salesRepCertificateURL + login.getManagerId()
        
        let request = NSMutableURLRequest(url: NSURL(string: apiURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: {
            (data, response, error) -> Void in
            if (error != nil) {
                print("Error: \(error)")
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "certificate_title".localized(), message: "certificate_save_error".localized(), preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "certificate_ok".localized(), style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
            } else {
                print("HTTP Response: \(data)")
                do {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        print("2222 Print converted dictionary",convertedJsonIntoDict)
                        
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        if statusVal == nil {
                            print("status val is null")
                            let statusVal = convertedJsonIntoDict["status"] as? String
                            print("status valule is \(statusVal)")
                            let login = Login.sharedInstance
                            login.status = String(describing: convertedJsonIntoDict["status"] as? Bool)
                            login.message = convertedJsonIntoDict["message"] as? String
                            progressHUD.hide()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            
                        } else {
                            if(statusVal)! {
                                let certificates = Certificate.sharedInstance
                                let certsArrJ: NSArray = convertedJsonIntoDict["data"] as! NSArray
                                
                                var certDetails:CertificateDetails
                                for i in 0..<certsArrJ.count
                                {
                                    let certObj: JSON = JSON(certsArrJ[i])
                                    certDetails = CertificateDetails()
                                    let certId = certObj["id"].stringValue
                                    let certPath = certObj["path"].stringValue
                                    certDetails.id = certId
                                    certDetails.path = certPath
                                    if i == 1 // Avoid mobile data
                                    {
                                        let url = URL(string: certDetails.path)
                                        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                                        certDetails.image = UIImage(data: data!)
                                        
                                        
                                    }
                                    certificates.addCertificateData(certificateDetails: certDetails)
                                    let imagePath : String = self.certificates.certificates[0].path!
                                    let url = URL(string : imagePath)
                                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                                    if(data != nil) {
                                        self.certImageView.image = UIImage(data: data!)
                                        self.downloadBtn.isEnabled = true
                                        self.downloadBtn.alpha = 1
                                        self.shareBtn.isEnabled = true
                                        self.shareBtn.alpha = 1
                                        //                                        self.downloadBtn.isUserInteractionEnabled = true
                                        //                                        self.downloadBtn.alpha = 1
                                        //
                                        //                                        self.shareBtn.isUserInteractionEnabled = true
                                        //                                        self.shareBtn.alpha = 1
                                    } else {
                                        DispatchQueue.main.async {
                                            let alert = UIAlertController(title: "certificate_title".localized(), message: "certificate_no_certificates".localized(), preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "certificate_ok".localized(), style: UIAlertActionStyle.default, handler: nil))
                                            self.present(alert, animated: true, completion: nil)
                                        }
                                    }
                                }
                                progressHUD.hide()
                                UIApplication.shared.endIgnoringInteractionEvents()
                            } else {
                                print("In else!")
                                
                                DispatchQueue.main.async {
                                    let alert = UIAlertController(title: "certificate_title".localized(), message: "certificate_no_certificates".localized(), preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "certificate_ok".localized(), style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                    progressHUD.hide()
                                    UIApplication.shared.endIgnoringInteractionEvents()
                                }
                            }
                        }
                    }
                } catch let error as NSError
                {
                    print(error)
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "certificate_title".localized(), message: "certificate_save_error".localized(), preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "certificate_ok".localized(), style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    progressHUD.hide()
                    UIApplication.shared.beginIgnoringInteractionEvents()
                }
            }
        })
        dataTask.resume()
    }
    
    func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "saveerror".localized(), message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "ok".localized(), style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "saved".localized(), message: "imagesaved".localized(), preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "ok".localized(), style: .default))
            present(ac, animated: true)
        }
    }
    
    
}
