//
//  ManagerFeedbackVC.swift
//  6one5Manager
//
//  Created by enyotalearning on 30/09/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit
import Foundation
import Localize_Swift

class ManagerFeedbackVC: UIViewController {
    //, DKDropMenuDelegate
    @IBOutlet weak var descriptor1: CosmosView!
    @IBOutlet weak var descriptor2: CosmosView!
    @IBOutlet weak var descriptor3: CosmosView!
    //@IBOutlet weak var dropdownView: DKDropMenu!
    @IBOutlet weak var dropdownTxtfield: UITextField!
    
    @IBOutlet weak var dropdownView: UIView!
    
    @IBOutlet weak var descriptor4: CosmosView!
    
    
    
    @IBOutlet weak var AddReviewTitle: UILabel!
    @IBOutlet weak var moduleTitle: UILabel!
    @IBOutlet weak var descText1: UILabel!
    @IBOutlet weak var descText2: UILabel!
    
    @IBOutlet weak var descText3: UILabel!
    @IBOutlet weak var descText4: UILabel!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    var menuView: BTNavigationDropdownMenu!
    var selectedModuleId :Int = 0
    var module_id = 2
    var course_id = 2
    var user_id: String = "4"
    var manager_id: String = "165"
    var reviewerName: String = ""
    var salesrep_id = "1"
    var salesmodule_id = "1"
    var modulePickerArray = [String]()
    var moduleIdArray = [String]()
    var courseIdArray = [String]()
    var screenComingFrom = ""
    var notificationModuleID = 0
    var notificationCourseID = 0
    var modelReviewRequest = ModelReviewRequest.sharedInstance
    //"5discripter3".localized()
    var descripters = [                                                                                                 ["1discripter1".localized(),"1discripter2".localized(),"1discripter3".localized() ],
                                                                                                                        ["2discripter1".localized(),"2discripter2".localized(), "2discripter3".localized()],
                                                                                                                        ["3discripter1".localized(),"3discripter2".localized(), "3discripter3".localized(), "Cross & up sells"],
                                                                                                                        ["4discripter1".localized(),"4discripter2".localized(), "4discripter3".localized()],
                                                                                                                        ["5discripter1".localized(),"5discripter2".localized()],
                                                                                                                        ["6discripter1".localized(),"6discripter2".localized(), "6discripter3".localized()]]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let learnerDetails =  LearnerDetails.sharedInstance
        self.user_id = learnerDetails.selectedLearnerId//
        print("Here is user id \(user_id)")
        if screenComingFrom == "notification" {
            addDataToView()
        }else{
            self.getModelReviewNotificationNotifs()
        }
        // self.navigationItem.titleView = menuView
    }
    func setDropDownOnView() {
        
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 0.0/255.0, green:180/255.0, blue:220/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        if screenComingFrom == "notification" {
            menuView = BTNavigationDropdownMenu(navigationController: self.navigationController, containerView: self.navigationController!.view, title: modulePickerArray[0], items: modulePickerArray as [AnyObject])
        }else{
            menuView = BTNavigationDropdownMenu(navigationController: self.navigationController, containerView: self.navigationController!.view, title: modulePickerArray[0], items: modulePickerArray as [AnyObject])
        }
        menuView.menuTitleColor = UIColor.black
        menuView.cellBackgroundColor = self.navigationController?.navigationBar.barTintColor
        menuView.cellSelectionColor = UIColor(red: 0.0/255.0, green:160.0/255.0, blue:195.0/255.0, alpha: 1.0)
        menuView.shouldKeepSelectedCellColor = true
        menuView.cellTextLabelColor = UIColor.black
        menuView.cellTextLabelFont = UIFont(name: "Avenir-Heavy", size: 17)
        menuView.cellTextLabelAlignment = .left // .Center // .Right // .Left
        menuView.arrowPadding = 15
        menuView.animationDuration = 0.5
        menuView.maskBackgroundColor = UIColor.black
        menuView.maskBackgroundOpacity = 0.3
        menuView.didSelectItemAtIndexHandler = {(indexPath: Int) -> () in
            print("Did select item at index: \(indexPath)")
            self.selectedModuleId = indexPath
            print("self.selectedModuleId: \(self.selectedModuleId)")
            
            switch self.moduleIdArray[indexPath] {
            case "1":
                self.module_id = 1
                self.setDescripterText(index:1)
                
                break
            case "2":
                self.module_id = 2
                self.setDescripterText(index:2)
                
                break
            case "3":
                self.module_id = 3
                self.setDescripterText(index:3)
                
                break
            case "4":
                self.module_id = 4
                self.setDescripterText(index:4)
                
                break
            case "5":
                self.module_id = 5
                self.setDescripterText(index:5)
                
                break
            case "6":
                self.module_id = 6
                self.setDescripterText(index:6)
                
                break
            default:
                break
            }
            
        }
        self.dropdownView.addSubview(menuView)
    }
    @IBAction func OnLClickTappedInfoButton(_ sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        // let viewController:ReviewInfoViewController = storyboard.instantiateViewController(withIdentifier: "ReviewInfoViewController") as! ReviewInfoViewController
        
        
        let viewController:ReviewModuleInfoViewController = storyboard.instantiateViewController(withIdentifier: "ReviewModuleInfoViewController") as! ReviewModuleInfoViewController
        viewController.moduleid = module_id
        self.navigationController?.present(viewController, animated: true, completion: nil)
        
    }
    override func viewWillAppear(_ animated: Bool){
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        self.title = "Add Review "
        
        let btn1 = UIButton(type: .infoDark)
        // btn1.setImage(UIImage(named: "plusbtnImage"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(infoTapped), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        self.navigationItem.setRightBarButtonItems([item1], animated: true)
    }
    
    func infoTapped (){
        
        if (Constants.device == "iPhone")
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController:ReviewInfoViewController = storyboard.instantiateViewController(withIdentifier: "ReviewInfoViewController") as! ReviewInfoViewController
        self.navigationController?.present(viewController, animated: true, completion: nil)
        }
        else
        {
            let storyboard = UIStoryboard(name: "Storyboard_ipad", bundle: nil)
            let viewController:ReviewInfoViewController = storyboard.instantiateViewController(withIdentifier: "ReviewInfoViewController") as! ReviewInfoViewController
            self.navigationController?.present(viewController, animated: true, completion: nil)
        }
    }
    
    func addDataToView()
    {
        DispatchQueue.main.async() {
            let login = Login.sharedInstance
            self.manager_id = login.getManagerId()
            let learnerDetails =  LearnerDetails.sharedInstance
            self.user_id = learnerDetails.selectedLearnerId//learnerDetails.getLearnerId(index: learnerDetails.selectedLearner)
            
            print( " user id is \(self.user_id)")
            
            
            //module_id = reviewNotificationData
            if learnerDetails.navigationPath == "2"
            {
                //Only selected Module review
                let notificationData = NotificationData.sharedInstance
                let notificationObject = notificationData.getNotificationDetails(index: learnerDetails.notificationIndex)
                self.modulePickerArray = [notificationObject.moduleName]
                self.moduleIdArray = [notificationObject.module_id]
                self.courseIdArray = [notificationObject.course_id]
                self.module_id = Int(notificationObject.module_id)!
                self.setDescripterText(index:self.module_id)
            }
            self .setDropDownOnView()
        }
    }
    
    func setDescripterText(index:Int)
    {
        descText1.text = descripters[index - 1][0]
        descText2.text = descripters[index - 1][1]
        switch module_id {
        case 3:
            descText3.text = descripters[index - 1][2]
            descriptor3.alpha = 1
            descText3.alpha = 1
            descText4.text = descripters[index - 1][3]
            descText4.alpha = 1
            descriptor4.alpha = 1
            break
        case 5:
            
            descriptor3.alpha = 0
            descText3.alpha = 0
            descText4.alpha = 0
            descriptor4.alpha = 0
            break
        default:
            
            descText3.text = descripters[index - 1][2]
            descriptor3.alpha = 1
            descText3.alpha = 1
            descText4.alpha = 0
            descriptor4.alpha = 0
            break
            
        }
        
        //        if module_id == 5
        //        {
        //            descriptor3.alpha = 0
        //            descText3.alpha = 0
        //            descText4.alpha = 0
        //            descriptor4.alpha = 0
        //        }
        //        else
        //        {
        //            descText3.text = descripters[index - 1][2]
        //            descriptor3.alpha = 1
        //            descText3.alpha = 1
        //            descText4.alpha = 0
        //            descriptor4.alpha = 0
        //        }
    }
    
    func loadModelReviewRequestData(representativeId:String)
    {
        
        let progressHUD = ProgressHUD()
        self.view.addSubview(progressHUD)
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        if (modelReviewRequest.getRequestCount() >= 0 )
        {
            //print("reviewNotificationData Data Reset ")
            //write clear logic here
            modelReviewRequest.modelReviewRequestArray = [ModelReviewRequestData]()
            //learnerDetails.learnerProfileArray = [String]()
        }
        
        ModelReviewRequestService.sharedInstance.loadData(reprenentativeId: representativeId, completionHandler: { (userData:ModelReviewRequest) -> () in
            DispatchQueue.main.async() {
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
            }
            if (self.modelReviewRequest.getRequestCount() >= 0 )
            {
                
                let login = Login.sharedInstance
                progressHUD.hide()
                
                UIApplication.shared.endIgnoringInteractionEvents()
                if login.status != nil
                {
                    let alertController = UIAlertController(title: "User status", message: login.message , preferredStyle: .alert)
                    let OKAction = UIAlertAction(title: "ok".localized(), style: .default) {
                        (action:UIAlertAction!) in
                        login.logoutFromDeviceCache()
                        //self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
                        self.navigationController?.popViewController(animated: true)
                        
                    }
                    alertController.addAction(OKAction)
                    self.present(alertController, animated: true, completion:nil)
                    
                }
                    
                    
                else
                {
                    
                    DispatchQueue.main.async()
                        {
                            print("Model Review request main execute")
                            progressHUD.hide()
                            //self.reviewNotificationData.desc()
                            
                            self.addDataToView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            
                    }
                }
                
                
                //learner details service completed
                
            }// end if learnerDetails.getLearnerCount
            else
            {
                print("Problem while loading data ")
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
                
            } // end else learnerDetails.getLearnerCount
            
        })
        
    }
    
    func itemSelected(withIndex: Int, name: String) {
        print("\(name) selected --- index \(withIndex+1)");
        module_id = withIndex+1
    }
    
    @IBAction func submitCancelClicked(_ sender: AnyObject) {
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func reviewSubmitClicked(_ sender: AnyObject) {
        
        //print("Des1: \(self.descriptor1.rating)")
        //print("Des2: \(self.descriptor2.rating)")
        //print("Des3: \(self.descriptor3.rating)")
        submitReview()
        
    }
    func submitReview()
    {
        
        let progressHUD = ProgressHUD()
        self.view.addSubview(progressHUD)
        
        let desc1 = self.descriptor1.rating
        let desc2 = self.descriptor2.rating
        let desc3 = self.descriptor3.rating
        let desc4 = self.descriptor4.rating
        //course_id = 2
        
        let headers = [
            "cache-control": "no-cache"
        ]
        
        let dataStr: String!
        
        switch module_id {
        case 5:
            dataStr = "data=[{ \"user_id\":\"\(user_id as NSString)\",\"module_id\":\"\(String(module_id) as NSString)\",\"course_id\":\"\(String(course_id) as NSString)\",\"descriptor\": { \"1\":\"\(String(desc1) as NSString)\",\"2\":\"\(String(desc2) as NSString)\",\"3\":\"\(String("") as NSString)\" } }]"
            break
        case 3:
            dataStr = "data=[{ \"user_id\":\"\(user_id as NSString)\",\"module_id\":\"\(String(module_id) as NSString)\",\"course_id\":\"\(String(course_id) as NSString)\",\"descriptor\": { \"1\":\"\(String(desc1) as NSString)\",\"2\":\"\(String(desc2) as NSString)\",\"3\":\"\(String(desc3) as NSString)\",\"4\":\"\(String(desc4) as NSString)\"} }]"
            break
        default:
            dataStr = "data=[{ \"user_id\":\"\(user_id as NSString)\",\"module_id\":\"\(String(module_id) as NSString)\",\"course_id\":\"\(String(course_id) as NSString)\",\"descriptor\": { \"1\":\"\(String(desc1) as NSString)\",\"2\":\"\(String(desc2) as NSString)\",\"3\":\"\(String(desc3) as NSString)\" } }]"
        }
        
        //        if module_id != 5 && module_id !== 3
        //        {
        //
        //
        //        }else if module_id == 3{
        //
        //
        //        }
        //        else
        //        {
        //
        //        }
        
        //print("Data string is \(dataStr)")
        
        let postData = NSMutableData(data: dataStr.data(using: String.Encoding.utf8)!)
        let login = Login.sharedInstance
        self.manager_id = login.getManagerId()
        print("manager_-----Id:---",self.manager_id)
        let apiString = WebAPI.MANAGERSUBMITRATING + manager_id//Constants.managerSubmitRatingURL + manager_id
        let request = NSMutableURLRequest(url: NSURL(string: apiString)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            DispatchQueue.main.async() {
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
            }
            //print("Magic Data is  \(data!)")
            if (error != nil) {
                print("Error: \(error)")
                let alertController = UIAlertController(title: "reviewsubmission".localized(), message: "connectionerror".localized() , preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "ok".localized(), style: .default) { (action:UIAlertAction!) in
                    
                    self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion:nil)
                progressHUD.hide()
                
            } else {
                let httpResponse = response as? HTTPURLResponse
                print("Response: \(httpResponse)")
                do {
                    
                    //print("Magic Data is  \(data!)")
                    
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        print("Print converted dictionary",convertedJsonIntoDict)
                        
                        // Get value by key
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        print("Status: \(statusVal!)")
                        
                        if(statusVal! == true)
                        {
                            let alertController = UIAlertController(title: "reviewsubmission".localized(), message: "reviewsubmitsucess".localized() , preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "ok".localized(), style: .default) { (action:UIAlertAction!) in
                                self.navigationController?.popViewController(animated: true)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                            progressHUD.hide()
                            
                        } else {
                            var error_msg:NSString
                            if convertedJsonIntoDict["message"] as? NSString != nil {
                                error_msg = convertedJsonIntoDict["message"] as! NSString
                            } else {
                                error_msg = "Unknown Error"
                            }
                            print("error_msg",error_msg)
                            let alertController = UIAlertController(title: "reviewsubmission".localized(), message: "unresponsiveserver".localized() , preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "ok".localized(), style: .default) { (action:UIAlertAction!) in
                                self.navigationController?.popViewController(animated: true)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                            progressHUD.hide()
                        }
                    }
                } catch let error as NSError {
                    print(error)
                    let alertController = UIAlertController(title: "reviewsubmission".localized(), message: "unresponsiveserver".localized() , preferredStyle: .alert)
                    let OKAction = UIAlertAction(title: "ok".localized(), style: .default) { (action:UIAlertAction!) in
                        self.navigationController?.popViewController(animated: true)
                    }
                    alertController.addAction(OKAction)
                    self.present(alertController, animated: true, completion:nil)
                    
                    progressHUD.hide()
                    
                }
                print("Data: \(data)")
            }
        })
        
        dataTask.resume()
    }
    
    
    func getModelReviewNotificationNotifs()
    {
        let progressHUD = ProgressHUD()
        self.view.addSubview(progressHUD)
        
        
        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            ]
        
        let apiURL: String = "https://key2train.in/admin/api/v1/module_review_request/" + user_id
        
        let request = NSMutableURLRequest(url: NSURL(string: apiURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            DispatchQueue.main.async() {
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
            }
            if (error != nil) {
                print("Network Error: \(error)")
                let alert = UIAlertController(title: "modelreviewsubmission".localized(), message: "errordetails".localized(), preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "ok".localized(), style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            } else {
                let httpResponse = response as? HTTPURLResponse
                print("HTTP Response: \(httpResponse)")
                
                do {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        print("Print converted dictionary 111 ",convertedJsonIntoDict)
                        
                        // Get value by key
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        print("status value is \(statusVal!)")
                        
                        if(statusVal! == true)
                        {
                            OperationQueue.main.addOperation{
                                
                                let modelReviewRequest = ModelReviewRequest.sharedInstance
                                let json = JSON(convertedJsonIntoDict)
                                var modelReviewRequestData:ModelReviewRequestData
                                for item in json["data"].arrayValue
                                {
                                    modelReviewRequestData = ModelReviewRequestData()
                                    
                                    modelReviewRequestData.id = item["id"].stringValue
                                    modelReviewRequestData.name = item["name"].stringValue
                                    modelReviewRequestData.l_fname = item["l_fname"].stringValue
                                    modelReviewRequestData.l_lname = item["l_lname"].stringValue
                                    modelReviewRequestData.module_id = item["module_id"].stringValue
                                    modelReviewRequestData.course_id = item["course_id"].stringValue
                                    //"module_name"
                                    self.modulePickerArray.append(item["name"].stringValue)
                                    self.moduleIdArray.append(item["module_id"].stringValue)
                                    self.courseIdArray.append(item["course_id"].stringValue)
                                    
                                    modelReviewRequest.addModelReviewRequest(modelReviewRequestData: modelReviewRequestData)
                                }
                                self.setDropDownOnView()
                                print("Here is module id 88 \(self.moduleIdArray[0])")
                                self.module_id = Int(self.moduleIdArray[0])!
                                self.setDescripterText(index:self.module_id)
                                //self.modulePicker.reloadAllComponents()
                                progressHUD.hide()
                            }
                        } else
                        {
                            /*var error_msg:NSString
                             if convertedJsonIntoDict["message"] as? NSString != nil
                             {
                             error_msg = convertedJsonIntoDict["message"] as! NSString
                             } else {
                             error_msg = "Unknown Error"
                             }
                             print("error_msg",error_msg)*/
                            
                            progressHUD.hide();
                            
                            
                            let alertController = UIAlertController(title: "modelreviewsubmission", message: "No review request" , preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "ok".localized(), style: .default) { (action:UIAlertAction!) in
                                //self.navigationController?.popViewController(animated: true)
                                self.navigationController?.isNavigationBarHidden = false
                                self.navigationController?.popViewController(animated: true)
                                //self.view.removeFromSuperview()
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                            
                        }
                    }
                } catch let error as NSError {
                    print(error)
                    progressHUD.hide()
                    
                    
                }
                
                
            }
        })
        
        dataTask.resume()
    }
    
}
