//
//  Module4IntroController.swift
//  6one5Manager
//
//  Created by Sumit More on 12/22/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import AVFoundation

class Module4IntroController: UIViewController, AVAudioPlayerDelegate {
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var multimediaIntro: UIButton!
    @IBOutlet weak var introductionImage: UIImageView!
    
    @IBOutlet weak var mgr_m4_e1l1: UILabel!
    @IBOutlet weak var mgr_m4_e2l1: UILabel!
    @IBOutlet weak var mgr_m4_e2l2: UILabel!
    @IBOutlet weak var mgr_m4_e3l1: UILabel!
    @IBOutlet weak var mgr_m4_e4l1: UILabel!
    @IBOutlet weak var mgr_m4_e4l2: UILabel!
    @IBOutlet weak var mgr_m4_e4l3: UILabel!
    @IBOutlet weak var mgr_m4_e4l4: UILabel!
    @IBOutlet weak var mgr_m4_e4l5: UILabel!
    @IBOutlet weak var mgr_m4_e5l1: UILabel!
    @IBOutlet weak var mgr_m4_e6l1: UILabel!
    @IBOutlet weak var mgr_m4_e7l1: UILabel!
    @IBOutlet weak var mgr_m4_e8l1: UILabel!
    @IBOutlet weak var mgr_m4_e9l1: UILabel!
    @IBOutlet weak var mgr_m4_e10l1: UILabel!
    @IBOutlet weak var mgr_m4_e11l1: UILabel!
    @IBOutlet weak var mgr_m4_e12l1: UILabel!
    @IBOutlet weak var mgr_m4_e13l1: UILabel!
    @IBOutlet weak var mgr_m4_e14l1: UILabel!
    @IBOutlet weak var mgr_m4_e15l1: UILabel!
    @IBOutlet weak var mgr_m4_e16l1: UILabel!
    @IBOutlet weak var mgr_m4_e16l2: UILabel!
    @IBOutlet weak var mgr_m4_e16l3: UILabel!
    @IBOutlet weak var mgr_m4_e16l4: UILabel!
    @IBOutlet weak var mgr_m4_e16l5: UILabel!
    @IBOutlet weak var mgr_m4_e17l1: UILabel!
    
    @IBOutlet weak var mgr_m4_e16b1: UIButton!
    @IBOutlet weak var mgr_m4_e16b2: UIButton!
    @IBOutlet weak var mgr_m4_e16b3: UIButton!
    @IBOutlet weak var mgr_m4_e16b4: UIButton!
    @IBOutlet weak var mgr_m4_e16b5: UIButton!
    
    
    @IBAction func introFwdClick(_ sender: AnyObject) {
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        audioPlayerIntroAud?.pause()
    }
    
    @IBAction func bulb1Click(_ sender: AnyObject) {
        self.bulbNo = 1
        let popOverVC = UIStoryboard (name: "Module4SB", bundle: nil).instantiateViewController(withIdentifier: "m4_wQID") as! Module4IntroPopupsController
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        popOverVC.modalTransitionStyle = .crossDissolve
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    @IBAction func bulb2Click(_ sender: AnyObject) {
        self.bulbNo = 2
        let popOverVC = UIStoryboard (name: "Module4SB", bundle: nil).instantiateViewController(withIdentifier: "m4_wQID") as! Module4IntroPopupsController
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        popOverVC.modalTransitionStyle = .crossDissolve
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    @IBAction func bulb3Click(_ sender: AnyObject) {
        self.bulbNo = 3
        let popOverVC = UIStoryboard (name: "Module4SB", bundle: nil).instantiateViewController(withIdentifier: "m4_wQID") as! Module4IntroPopupsController
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        popOverVC.modalTransitionStyle = .crossDissolve
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    @IBAction func bulb4Click(_ sender: AnyObject) {
        self.bulbNo = 4
        let popOverVC = UIStoryboard (name: "Module4SB", bundle: nil).instantiateViewController(withIdentifier: "m4_wQID") as! Module4IntroPopupsController
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        popOverVC.modalTransitionStyle = .crossDissolve
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    @IBAction func bulb5Click(_ sender: AnyObject) {
        self.bulbNo = 5
        let popOverVC = UIStoryboard (name: "Module4SB", bundle: nil).instantiateViewController(withIdentifier: "m4_wQID") as! Module4IntroPopupsController
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        popOverVC.modalTransitionStyle = .crossDissolve
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    var playFlagIntroAud = 1
    var audioPlayerIntroAud: AVAudioPlayer?
    
    var GTTimer : Timer = Timer()
    var GTTimer2 : Timer = Timer()
    var isPaused = false
    var intervalVal: Double = 5
    var intervalVal2: Double = 0.15
    var bulbNo: Int = 0
    var questionNo: Int = 1
    
    //var timeIntervals = [4,9,5,11,5,6,16,18,15,11,6,5,8,8,10,6,3]
    
    @IBOutlet weak var introBtmLbl: UILabel!
    
    override func viewDidLoad() {
        
        self.bulbNo = 0
        
        self.introBtmLbl.text = "Mgr_M4_IntroductionBtmLbl".localized()
        
        //        let image = UIImage(named: "sof.png")
        //        self.navigationItem.titleView = UIImageView(image: image)
        
        let device = UIDevice.current.model
        print("Device type: " + device)
        
        if(device == "iPhone") {
            print("Changing size to 16")
            let attrs = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont.systemFont(ofSize: 16.0)
            ]
            self.navigationController!.navigationBar.titleTextAttributes = attrs
            
            let button: UIButton = UIButton(type: UIButtonType.custom)
            button.setImage(UIImage(named: "sof.png"), for: UIControlState.normal)
            button.backgroundColor = UIColor.white
            button.frame = CGRect(x: 0, y: 0, width: 56, height: 45)
            let barButton = UIBarButtonItem(customView: button)
            
            
            let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            negativeSpacer.width = -17;
            
            self.navigationItem.setRightBarButtonItems([negativeSpacer, barButton ], animated: false)
            
        } else {
            print("Changing size to 32")
            let attrs = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont.systemFont(ofSize: 32.0)
            ]
            self.navigationController!.navigationBar.titleTextAttributes = attrs
            
            let button: UIButton = UIButton(type: UIButtonType.custom)
            button.setImage(UIImage(named: "sof.png"), for: UIControlState.normal)
            button.backgroundColor = UIColor.white
            //set frame
            button.frame = CGRect(x: 0, y: 0, width: 56, height: 45)
            let barButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            
            
            let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            negativeSpacer.width = -21;
            
            self.navigationItem.setRightBarButtonItems([negativeSpacer, barButton ], animated: false)
        }
        
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 36.0/255.0, green: 184.0/255.0, blue: 239.0/255.0, alpha: 1.0)
        
        self.navigationItem.title = "Mgr_M4_ModuleName".localized()
        
        self.introductionImage.image = UIImage(named: "mgr_m4_intro1")
        
        self.mgr_m4_e1l1.text = "Mgr_Intro_M4_E1L1".localized()
        self.mgr_m4_e2l1.text = "Mgr_Intro_M4_E2L1".localized()
        self.mgr_m4_e2l2.text = "Mgr_Intro_M4_E2L2".localized()
        self.mgr_m4_e3l1.text = "Mgr_Intro_M4_E3L1".localized()
        self.mgr_m4_e4l1.text = "Mgr_Intro_M4_E4L1".localized()
        self.mgr_m4_e4l2.text = "Mgr_Intro_M4_E4L2".localized()
        self.mgr_m4_e4l3.text = "Mgr_Intro_M4_E4L3".localized()
        self.mgr_m4_e4l4.text = "Mgr_Intro_M4_E4L4".localized()
        self.mgr_m4_e4l5.text = "Mgr_Intro_M4_E4L5".localized()
        self.mgr_m4_e5l1.text = "Mgr_Intro_M4_E5L1".localized()
        self.mgr_m4_e6l1.text = "Mgr_Intro_M4_E6L1".localized()
        self.mgr_m4_e7l1.text = "Mgr_Intro_M4_E7L1".localized()
        self.mgr_m4_e8l1.text = "Mgr_Intro_M4_E8L1".localized()
        self.mgr_m4_e9l1.text = "Mgr_Intro_M4_E9L1".localized()
        self.mgr_m4_e10l1.text = "Mgr_Intro_M4_E10L1".localized()
        self.mgr_m4_e11l1.text = "Mgr_Intro_M4_E11L1".localized()
        self.mgr_m4_e12l1.text = "Mgr_Intro_M4_E12L1".localized()
        self.mgr_m4_e13l1.text = "Mgr_Intro_M4_E13L1".localized()
        self.mgr_m4_e14l1.text = "Mgr_Intro_M4_E14L1".localized()
        self.mgr_m4_e15l1.text = "Mgr_Intro_M4_E15L1".localized()
        self.mgr_m4_e16l1.text = "Mgr_Intro_M4_E16L1".localized()
        self.mgr_m4_e16l2.text = "Mgr_Intro_M4_E16L2".localized()
        self.mgr_m4_e16l3.text = "Mgr_Intro_M4_E16L3".localized()
        self.mgr_m4_e16l4.text = "Mgr_Intro_M4_E16L4".localized()
        self.mgr_m4_e16l5.text = "Mgr_Intro_M4_E16L5".localized()
        self.mgr_m4_e17l1.text = "Mgr_Intro_M4_E17L1".localized()
        
        self.mgr_m4_e16b1.setTitle("".localized(), for: .normal)
        self.mgr_m4_e16b2.setTitle("".localized(), for: .normal)
        self.mgr_m4_e16b3.setTitle("".localized(), for: .normal)
        self.mgr_m4_e16b4.setTitle("".localized(), for: .normal)
        self.mgr_m4_e16b5.setTitle("".localized(), for: .normal)
        
        self.mgr_m4_e1l1.isHidden = false
        self.mgr_m4_e2l1.isHidden = true
        self.mgr_m4_e2l2.isHidden = true
        self.mgr_m4_e3l1.isHidden = true
        self.mgr_m4_e4l1.isHidden = true
        self.mgr_m4_e4l2.isHidden = true
        self.mgr_m4_e4l3.isHidden = true
        self.mgr_m4_e4l4.isHidden = true
        self.mgr_m4_e4l5.isHidden = true
        self.mgr_m4_e5l1.isHidden = true
        self.mgr_m4_e6l1.isHidden = true
        self.mgr_m4_e7l1.isHidden = true
        self.mgr_m4_e8l1.isHidden = true
        self.mgr_m4_e9l1.isHidden = true
        self.mgr_m4_e10l1.isHidden = true
        self.mgr_m4_e11l1.isHidden = true
        self.mgr_m4_e12l1.isHidden = true
        self.mgr_m4_e13l1.isHidden = true
        self.mgr_m4_e14l1.isHidden = true
        self.mgr_m4_e15l1.isHidden = true
        self.mgr_m4_e16l1.isHidden = true
        self.mgr_m4_e16l2.isHidden = true
        self.mgr_m4_e16l3.isHidden = true
        self.mgr_m4_e16l4.isHidden = true
        self.mgr_m4_e16l5.isHidden = true
        self.mgr_m4_e17l1.isHidden = true
        
        self.mgr_m4_e16b1.isEnabled = false
        self.mgr_m4_e16b2.isEnabled = false
        self.mgr_m4_e16b3.isEnabled = false
        self.mgr_m4_e16b4.isEnabled = false
        self.mgr_m4_e16b5.isEnabled = false

        if UserDefaults.standard.bool(forKey: "isPlusversion") == true {
            // screen 2
            
            self.mgr_m4_e3l1.removeConstraints(self.mgr_m4_e3l1.constraints)
            self.mgr_m4_e3l1.translatesAutoresizingMaskIntoConstraints = false
            
            let width_mgr_m4_e3l1 = NSLayoutConstraint(item: self.mgr_m4_e3l1,
                                                       attribute: NSLayoutAttribute.width,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: nil,
                                                       attribute: NSLayoutAttribute.notAnAttribute,
                                                       multiplier: 1,
                                                       constant: CGFloat(Constants.M4_mgr_m4_e3l1Constraints_6splus[0]))
            
            let top_mgr_m4_e3l1 = NSLayoutConstraint(item: self.mgr_m4_e3l1,
                                                     attribute: NSLayoutAttribute.top,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.top,
                                                     multiplier: 1,
                                                     constant: CGFloat(Constants.M4_mgr_m4_e3l1Constraints_6splus[1]))
            
            
            let lead_mgr_m4_e3l1 = NSLayoutConstraint(item: self.mgr_m4_e3l1,
                                                      attribute: NSLayoutAttribute.leading,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.leading,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M4_mgr_m4_e3l1Constraints_6splus[2]))
            
            let height_mgr_m4_e3l1 = NSLayoutConstraint(item: self.mgr_m4_e3l1,
                                                        attribute: NSLayoutAttribute.height,
                                                        relatedBy: NSLayoutRelation.equal,
                                                        toItem: nil,
                                                        attribute: NSLayoutAttribute.notAnAttribute,
                                                        multiplier: 1,
                                                        constant: CGFloat(Constants.M4_mgr_m4_e3l1Constraints_6splus[3]))
            
            
            let parentViewConstraints: [NSLayoutConstraint] = self.view.constraints
            
            for parentConstraint in parentViewConstraints {
                if let viewLbl = parentConstraint.firstItem as? UILabel {
                    if viewLbl.text! == "Mgr_Intro_M4_E3L1".localized() {
                        self.view.removeConstraint(parentConstraint)
                    }
                }
            }
            
            
            
            self.view.addConstraint(lead_mgr_m4_e3l1)   // not working
            self.view.addConstraint(width_mgr_m4_e3l1)
            self.view.addConstraint(top_mgr_m4_e3l1)
            self.view.addConstraint(height_mgr_m4_e3l1)
            
            
            self.mgr_m4_e2l1.removeConstraints(self.mgr_m4_e2l1.constraints)
            
            
            let top_mgr_m4_e2l1 = NSLayoutConstraint(item: self.mgr_m4_e2l1,
                                                     attribute: NSLayoutAttribute.top,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.top,
                                                     multiplier: 1,
                                                     constant:  CGFloat(Constants.M4_mgr_m4_e2l1Constraints_6splus))
            
            
            self.view.addConstraint(top_mgr_m4_e2l1)
            
            self.mgr_m4_e2l2.removeConstraints(self.mgr_m4_e2l2.constraints)
            self.mgr_m4_e2l2.translatesAutoresizingMaskIntoConstraints = false
            
            let width_mgr_m4_e2l2 = NSLayoutConstraint(item: self.mgr_m4_e2l2,
                                                       attribute: NSLayoutAttribute.width,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: nil,
                                                       attribute: NSLayoutAttribute.notAnAttribute,
                                                       multiplier: 1,
                                                       constant: 100)
            
            let lead_mgr_m4_e2l2 = NSLayoutConstraint(item: self.mgr_m4_e2l2,
                                                      attribute: NSLayoutAttribute.leading,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.leading,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M4_mgr_m4_e2l2Constraints_6splus[0]))
            
            let top_mgr_m4_e2l2 = NSLayoutConstraint(item: self.mgr_m4_e2l2,
                                                     attribute: NSLayoutAttribute.top,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.top,
                                                     multiplier: 1,
                                                     constant:  CGFloat(Constants.M4_mgr_m4_e2l2Constraints_6splus[1]))
            
            let parentViewConstraints2: [NSLayoutConstraint] = self.view.constraints
            
            for parentConstraint in parentViewConstraints2 {
                if let viewLbl = parentConstraint.firstItem as? UILabel {
                    if viewLbl.text! == "Mgr_Intro_M4_E2L2".localized() {
                        self.view.removeConstraint(parentConstraint)
                    }
                }
            }
            
            
            self.view.addConstraint(width_mgr_m4_e2l2)
            self.view.addConstraint(lead_mgr_m4_e2l2)
            self.view.addConstraint(top_mgr_m4_e2l2)
            
            
            
            self.mgr_m4_e4l2.removeConstraints(self.mgr_m4_e4l2.constraints)
            
            
            let top_mgr_m4_e4l2 = NSLayoutConstraint(item: self.mgr_m4_e4l2,
                                                     attribute: NSLayoutAttribute.top,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.top,
                                                     multiplier: 1,
                                                     constant:  CGFloat(Constants.M4_mgr_m4_e4l2Constraints_6splus[0]))
            let left_mgr_m4_e4l2 = NSLayoutConstraint(item: self.mgr_m4_e4l2,
                                                     attribute: NSLayoutAttribute.left,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.left,
                                                     multiplier: 1,
                                                     constant:  CGFloat(Constants.M4_mgr_m4_e4l2Constraints_6splus[1]))
            
            //
            self.view.addConstraint(top_mgr_m4_e4l2)
            self.view.addConstraint(left_mgr_m4_e4l2)
            
            
            self.mgr_m4_e4l3.removeConstraints(self.mgr_m4_e4l3.constraints)
            
            let top_mgr_m4_e4l3 = NSLayoutConstraint(item: self.mgr_m4_e4l3,
                                                     attribute: NSLayoutAttribute.top,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.top,
                                                     multiplier: 1,
                                                     constant: CGFloat(Constants.M4_mgr_m4_e4l3Constraints_6splus))
            
            
            self.view.addConstraint(top_mgr_m4_e4l3)
            
            
            self.mgr_m4_e4l4.removeConstraints(self.mgr_m4_e4l4.constraints)
            
            let top_mgr_m4_e4l4 = NSLayoutConstraint(item: self.mgr_m4_e4l4,
                                                     attribute: NSLayoutAttribute.top,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.top,
                                                     multiplier: 1,
                                                     constant:  CGFloat(Constants.M4_mgr_m4_e4l4Constraints_6splus))
            
            
            self.view.addConstraint(top_mgr_m4_e4l4)
            
            
            self.mgr_m4_e4l5.removeConstraints(self.mgr_m4_e4l5.constraints)
            
            let top_mgr_m4_e4l5 = NSLayoutConstraint(item: self.mgr_m4_e4l5,
                                                     attribute: NSLayoutAttribute.top,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.top,
                                                     multiplier: 1,
                                                     constant: CGFloat(Constants.M4_mgr_m4_e4l5Constraints_6splus))
            
            
            self.view.addConstraint(top_mgr_m4_e4l5)
            
            //
            
            self.mgr_m4_e4l1.removeConstraints(self.mgr_m4_e4l1.constraints)
            
            let width_mgr_m4_e4l1 = NSLayoutConstraint(item: self.mgr_m4_e4l1,
                                                       attribute: NSLayoutAttribute.width,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: nil,
                                                       attribute: NSLayoutAttribute.notAnAttribute,
                                                       multiplier: 1,
                                                       constant:  CGFloat(Constants.M4_mgr_m4_e4l1Constraints_6splus[0]))
            
            let top_mgr_m4_e4l1 = NSLayoutConstraint(item: self.mgr_m4_e4l1,
                                                     attribute: NSLayoutAttribute.top,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.top,
                                                     multiplier: 1,
                                                     constant:  CGFloat(Constants.M4_mgr_m4_e4l1Constraints_6splus[1]))
            
            self.view.addConstraint(width_mgr_m4_e4l1)
            self.view.addConstraint(top_mgr_m4_e4l1)
            
            
            //
            
            self.mgr_m4_e5l1.removeConstraints(self.mgr_m4_e5l1.constraints)
            
            
            let width_mgr_m4_e5l1 = NSLayoutConstraint(item: self.mgr_m4_e5l1,
                                                       attribute: NSLayoutAttribute.width,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: nil,
                                                       attribute: NSLayoutAttribute.notAnAttribute,
                                                       multiplier: 1,
                                                       constant: CGFloat(Constants.M4_mgr_m4_e6l1Constraints_6splus[0]))
            
            
            let top_mgr_m4_e5l1 = NSLayoutConstraint(item: self.mgr_m4_e5l1,
                                                     attribute: NSLayoutAttribute.top,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.top,
                                                     multiplier: 1,
                                                     constant:  CGFloat(Constants.M4_mgr_m4_e6l1Constraints_6splus[1]))
            
            self.view.addConstraint(width_mgr_m4_e5l1)
            self.view.addConstraint(top_mgr_m4_e5l1)
            
            
            self.mgr_m4_e7l1.removeConstraints(self.mgr_m4_e7l1.constraints)
            
            
            let width_mgr_m4_e7l1 = NSLayoutConstraint(item: self.mgr_m4_e7l1,
                                                       attribute: NSLayoutAttribute.width,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: nil,
                                                       attribute: NSLayoutAttribute.notAnAttribute,
                                                       multiplier: 1,
                                                       constant: CGFloat(Constants.M4_mgr_m4_e7l1Constraints_6splus[0]))
            
            
            let top_mgr_m4_e7l1 = NSLayoutConstraint(item: self.mgr_m4_e7l1,
                                                     attribute: NSLayoutAttribute.top,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.top,
                                                     multiplier: 1,
                                                     constant:  CGFloat(Constants.M4_mgr_m4_e7l1Constraints_6splus[1]))
            
            self.view.addConstraint(width_mgr_m4_e7l1)
            self.view.addConstraint(top_mgr_m4_e7l1)
            
            
            
            self.mgr_m4_e8l1.removeConstraints(self.mgr_m4_e8l1.constraints)
            
            
            let width_mgr_m4_e8l1 = NSLayoutConstraint(item: self.mgr_m4_e8l1,
                                                       attribute: NSLayoutAttribute.width,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: nil,
                                                       attribute: NSLayoutAttribute.notAnAttribute,
                                                       multiplier: 1,
                                                       constant: CGFloat(Constants.M4_mgr_m4_e8l1Constraints_6splus[0]))
            
            
            let top_mgr_m4_e8l1 = NSLayoutConstraint(item: self.mgr_m4_e8l1,
                                                     attribute: NSLayoutAttribute.top,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.top,
                                                     multiplier: 1,
                                                     constant: CGFloat(Constants.M4_mgr_m4_e8l1Constraints_6splus[1]))
            
            self.view.addConstraint(width_mgr_m4_e8l1)
            self.view.addConstraint(top_mgr_m4_e8l1)
            
            
            self.mgr_m4_e9l1.removeConstraints(self.mgr_m4_e9l1.constraints)
            
            
            let width_mgr_m4_e9l1 = NSLayoutConstraint(item: self.mgr_m4_e9l1,
                                                       attribute: NSLayoutAttribute.width,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: nil,
                                                       attribute: NSLayoutAttribute.notAnAttribute,
                                                       multiplier: 1,
                                                       constant: CGFloat(Constants.M4_mgr_m4_e9l1Constraints_6splus[0]))
            
            
            let top_mgr_m4_e9l1 = NSLayoutConstraint(item: self.mgr_m4_e9l1,
                                                     attribute: NSLayoutAttribute.top,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.top,
                                                     multiplier: 1,
                                                     constant: CGFloat(Constants.M4_mgr_m4_e9l1Constraints_6splus[1]))
            
            self.view.addConstraint(width_mgr_m4_e9l1)
            self.view.addConstraint(top_mgr_m4_e9l1)
            
            
            // mgr_m4_e11l1
            
            self.mgr_m4_e11l1.removeConstraints(self.mgr_m4_e11l1.constraints)
            
            
            let width_mgr_m4_e11l1 = NSLayoutConstraint(item: self.mgr_m4_e11l1,
                                                        attribute: NSLayoutAttribute.width,
                                                        relatedBy: NSLayoutRelation.equal,
                                                        toItem: nil,
                                                        attribute: NSLayoutAttribute.notAnAttribute,
                                                        multiplier: 1,
                                                        constant: CGFloat(Constants.M4_mgr_m4_e11l1Constraints_6splus[0]))
            
            
            let top_mgr_m4_e11l1 = NSLayoutConstraint(item: self.mgr_m4_e11l1,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant:  CGFloat(Constants.M4_mgr_m4_e11l1Constraints_6splus[1]))

            let left_mgr_m4_e11l1 = NSLayoutConstraint(item: self.mgr_m4_e11l1,
                                                      attribute: NSLayoutAttribute.left,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.left,
                                                      multiplier: 1,
                                                      constant:  CGFloat(Constants.M4_mgr_m4_e11l1Constraints_6splus[2]))
            
            self.view.addConstraint(width_mgr_m4_e11l1)
            self.view.addConstraint(top_mgr_m4_e11l1)
            self.view.addConstraint(left_mgr_m4_e11l1)
            
            
            self.mgr_m4_e12l1.removeConstraints(self.mgr_m4_e12l1.constraints)
            
            
            let width_mgr_m4_e12l1 = NSLayoutConstraint(item: self.mgr_m4_e12l1,
                                                        attribute: NSLayoutAttribute.width,
                                                        relatedBy: NSLayoutRelation.equal,
                                                        toItem: nil,
                                                        attribute: NSLayoutAttribute.notAnAttribute,
                                                        multiplier: 1,
                                                        constant: CGFloat(Constants.M4_mgr_m4_e12l1Constraints_6splus[0]))
            
            
            let top_mgr_m4_e12l1 = NSLayoutConstraint(item: self.mgr_m4_e12l1,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M4_mgr_m4_e12l1Constraints_6splus[1]))
            
            self.view.addConstraint(width_mgr_m4_e12l1)
            self.view.addConstraint(top_mgr_m4_e12l1)
            
            
            self.mgr_m4_e13l1.removeConstraints(self.mgr_m4_e13l1.constraints)
            
            
            let width_mgr_m4_e13l1 = NSLayoutConstraint(item: self.mgr_m4_e13l1,
                                                        attribute: NSLayoutAttribute.width,
                                                        relatedBy: NSLayoutRelation.equal,
                                                        toItem: nil,
                                                        attribute: NSLayoutAttribute.notAnAttribute,
                                                        multiplier: 1,
                                                        constant: CGFloat(Constants.M4_mgr_m4_e13l1Constraints_6splus[0]))
            
            
            let top_mgr_m4_e13l1 = NSLayoutConstraint(item: self.mgr_m4_e13l1,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M4_mgr_m4_e13l1Constraints_6splus[1]))
            let left_mgr_m4_e13l1 = NSLayoutConstraint(item: self.mgr_m4_e13l1,
                                                      attribute: NSLayoutAttribute.left,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.left,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M4_mgr_m4_e13l1Constraints_6splus[2]))
            
            self.view.addConstraint(width_mgr_m4_e13l1)
            self.view.addConstraint(top_mgr_m4_e13l1)
            self.view.addConstraint(left_mgr_m4_e13l1)
            
            
            
            self.mgr_m4_e14l1.removeConstraints(self.mgr_m4_e14l1.constraints)
            
            
            let width_mgr_m4_e14l1 = NSLayoutConstraint(item: self.mgr_m4_e14l1,
                                                        attribute: NSLayoutAttribute.width,
                                                        relatedBy: NSLayoutRelation.equal,
                                                        toItem: nil,
                                                        attribute: NSLayoutAttribute.notAnAttribute,
                                                        multiplier: 1,
                                                        constant: CGFloat(Constants.M4_mgr_m4_e14l1Constraints_6splus[0]))
            
            
            let top_mgr_m4_e14l1 = NSLayoutConstraint(item: self.mgr_m4_e14l1,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M4_mgr_m4_e14l1Constraints_6splus[1]))
            
            self.view.addConstraint(width_mgr_m4_e14l1)
            self.view.addConstraint(top_mgr_m4_e14l1)
            
            // mgr_m4_e15l1
            
            
            self.mgr_m4_e15l1.removeConstraints(self.mgr_m4_e15l1.constraints)
            
            
            let width_mgr_m4_e15l1 = NSLayoutConstraint(item: self.mgr_m4_e15l1,
                                                        attribute: NSLayoutAttribute.width,
                                                        relatedBy: NSLayoutRelation.equal,
                                                        toItem: nil,
                                                        attribute: NSLayoutAttribute.notAnAttribute,
                                                        multiplier: 1,
                                                        constant: CGFloat(Constants.M4_mgr_m4_e15l1Constraints_6splus[0]))
            
            
            let top_mgr_m4_e15l1 = NSLayoutConstraint(item: self.mgr_m4_e15l1,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M4_mgr_m4_e15l1Constraints_6splus[1]))

            let left_mgr_m4_e15l1 = NSLayoutConstraint(item: self.mgr_m4_e15l1,
                                                      attribute: NSLayoutAttribute.left,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.left,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M4_mgr_m4_e15l1Constraints_6splus[2]))
            
            self.view.addConstraint(width_mgr_m4_e15l1)
            self.view.addConstraint(top_mgr_m4_e15l1)
            self.view.addConstraint(left_mgr_m4_e15l1)
            
            
            
            //// Last Screen
            
            self.mgr_m4_e16l1.removeConstraints(self.mgr_m4_e16l1.constraints)
            
            
            let width_mgr_m4_e16l1 = NSLayoutConstraint(item: self.mgr_m4_e16l1,
                                                        attribute: NSLayoutAttribute.width,
                                                        relatedBy: NSLayoutRelation.equal,
                                                        toItem: nil,
                                                        attribute: NSLayoutAttribute.notAnAttribute,
                                                        multiplier: 1,
                                                        constant: CGFloat(Constants.M4_mgr_m4_e16l1Constraints_6splus[0]))
            
            
            let top_mgr_m4_e16l1 = NSLayoutConstraint(item: self.mgr_m4_e16l1,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant:  CGFloat(Constants.M4_mgr_m4_e16l1Constraints_6splus[1]))
            
            self.view.addConstraint(width_mgr_m4_e16l1)
            self.view.addConstraint(top_mgr_m4_e16l1)
            
            
            
            self.mgr_m4_e16l2.removeConstraints(self.mgr_m4_e16l2.constraints)
            
            
            let width_mgr_m4_e16l2 = NSLayoutConstraint(item: self.mgr_m4_e16l2,
                                                        attribute: NSLayoutAttribute.width,
                                                        relatedBy: NSLayoutRelation.equal,
                                                        toItem: nil,
                                                        attribute: NSLayoutAttribute.notAnAttribute,
                                                        multiplier: 1,
                                                        constant:  CGFloat(Constants.M4_mgr_m4_e16l2Constraints_6splus[0]))
            
            
            let top_mgr_m4_e16l2 = NSLayoutConstraint(item: self.mgr_m4_e16l2,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant:   CGFloat(Constants.M4_mgr_m4_e16l2Constraints_6splus[1]))
            
            self.view.addConstraint(width_mgr_m4_e16l2)
            self.view.addConstraint(top_mgr_m4_e16l2)
            
            
            
            self.mgr_m4_e16l3.removeConstraints(self.mgr_m4_e16l3.constraints)
            
            
            let width_mgr_m4_e16l3 = NSLayoutConstraint(item: self.mgr_m4_e16l3,
                                                        attribute: NSLayoutAttribute.width,
                                                        relatedBy: NSLayoutRelation.equal,
                                                        toItem: nil,
                                                        attribute: NSLayoutAttribute.notAnAttribute,
                                                        multiplier: 1,
                                                        constant:   CGFloat(Constants.M4_mgr_m4_e16l3Constraints_6splus[0]))
            
            
            let top_mgr_m4_e16l3 = NSLayoutConstraint(item: self.mgr_m4_e16l3,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant:   CGFloat(Constants.M4_mgr_m4_e16l3Constraints_6splus[1]))
            
            let lead_mgr_m4_e16l3 = NSLayoutConstraint(item: self.mgr_m4_e16l3,
                                                       attribute: NSLayoutAttribute.leading,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.view,
                                                       attribute: NSLayoutAttribute.leading,
                                                       multiplier: 1,
                                                       constant: 170)
            
            let parentViewConstraints3: [NSLayoutConstraint] = self.view.constraints
            
            for parentConstraint in parentViewConstraints3 {
                if let viewLbl = parentConstraint.firstItem as? UILabel {
                    if viewLbl.text! == "Mgr_Intro_M4_E16L3".localized() {
                        self.view.removeConstraint(parentConstraint)
                    }
                }
            }
            
            
            
            self.view.addConstraint(lead_mgr_m4_e16l3)
            self.view.addConstraint(width_mgr_m4_e16l3)
            self.view.addConstraint(top_mgr_m4_e16l3)
            
            
            self.mgr_m4_e16l4.removeConstraints(self.mgr_m4_e16l4.constraints)
            
            
            let width_mgr_m4_e16l4 = NSLayoutConstraint(item: self.mgr_m4_e16l4,
                                                        attribute: NSLayoutAttribute.width,
                                                        relatedBy: NSLayoutRelation.equal,
                                                        toItem: nil,
                                                        attribute: NSLayoutAttribute.notAnAttribute,
                                                        multiplier: 1,
                                                        constant:   CGFloat(Constants.M4_mgr_m4_e16l4Constraints_6splus[0]))
            
            
            let top_mgr_m4_e16l4 = NSLayoutConstraint(item: self.mgr_m4_e16l4,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant:   CGFloat(Constants.M4_mgr_m4_e16l4Constraints_6splus[1]))
            
            let lead_mgr_m4_e16l4 = NSLayoutConstraint(item: self.mgr_m4_e16l4,
                                                       attribute: NSLayoutAttribute.leading,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.view,
                                                       attribute: NSLayoutAttribute.leading,
                                                       multiplier: 1,
                                                       constant: 174)
            
            
            let parentViewConstraints4: [NSLayoutConstraint] = self.view.constraints
            
            for parentConstraint in parentViewConstraints4 {
                if let viewLbl = parentConstraint.firstItem as? UILabel {
                    if viewLbl.text! == "Mgr_Intro_M4_E16L4".localized() {
                        self.view.removeConstraint(parentConstraint)
                    }
                }
            }
            
            self.view.addConstraint(lead_mgr_m4_e16l4)
            
            self.view.addConstraint(width_mgr_m4_e16l4)
            self.view.addConstraint(top_mgr_m4_e16l4)
            
            
            
            self.mgr_m4_e16l5.removeConstraints(self.mgr_m4_e16l5.constraints)
            
            
            let width_mgr_m4_e16l5 = NSLayoutConstraint(item: self.mgr_m4_e16l5,
                                                        attribute: NSLayoutAttribute.width,
                                                        relatedBy: NSLayoutRelation.equal,
                                                        toItem: nil,
                                                        attribute: NSLayoutAttribute.notAnAttribute,
                                                        multiplier: 1,
                                                        constant: CGFloat(Constants.M4_mgr_m4_e16l5Constraints_6splus[0]))
            
            
            let top_mgr_m4_e16l5 = NSLayoutConstraint(item: self.mgr_m4_e16l5,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M4_mgr_m4_e16l5Constraints_6splus[1]))
            
            self.view.addConstraint(width_mgr_m4_e16l5)
            self.view.addConstraint(top_mgr_m4_e16l5)
            
        }
        
        intervalVal = 5
        self.bulbNo = 1
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen1), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
        
        if self.revealViewController() != nil {
            menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: UIControlEvents.touchUpInside)
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            
        }
        
        do{
            let urlVideo = Bundle.main.url(forResource: "MGR_M04_S01", withExtension: "mp3")
            audioPlayerIntroAud = try AVAudioPlayer(contentsOf: urlVideo!)
            audioPlayerIntroAud!.delegate = self
            audioPlayerIntroAud!.prepareToPlay()
            audioPlayerIntroAud!.play()
        } catch {
            print("Error getting the audio file")
        }
        
    }
    
    func monitorTimer() {
        self.intervalVal = self.intervalVal - 0.15
        print("Interval in monitor: " + String(self.intervalVal))
    }
    
    func screen1(){
        
        print("In screen1")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m4_intro2")
        
        self.mgr_m4_e1l1.isHidden = true
        
        self.mgr_m4_e2l1.isHidden = false
        self.mgr_m4_e2l2.isHidden = false
        
        self.intervalVal = 9
        self.bulbNo = 2
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen2), userInfo: nil, repeats: false)
        
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
    }
    
    
    func screen2(){
        
        print("In screen2")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.mgr_m4_e3l1.isHidden = false
        
        //        UIView.transition(with: self.intro_m1e3l1,
        //                          duration: 2.0,
        //                          options: [.transitionCrossDissolve],
        //                          animations: {
        //                            self.intro_m1e3l1.text = "Mgr_Intro_m4_E3L1".localized
        //            }, completion: nil)
        
        self.intervalVal = 5
        self.bulbNo = 3
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen3), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen3(){
        
        print("In screen3")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m4_intro3")
        
        self.mgr_m4_e2l1.isHidden = true
        self.mgr_m4_e2l2.isHidden = true
        self.mgr_m4_e3l1.isHidden = true
        
        self.mgr_m4_e4l1.isHidden = false
        self.mgr_m4_e4l2.isHidden = false
        self.mgr_m4_e4l3.isHidden = false
        self.mgr_m4_e4l4.isHidden = false
        self.mgr_m4_e4l5.isHidden = false
        
        self.intervalVal = 10
        
        self.bulbNo = 4
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen4), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen4(){
        
        print("In screen4")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.mgr_m4_e5l1.isHidden = false
        
        self.intervalVal = 5
        self.bulbNo = 5
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen5), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen5(){
        
        print("In screen5")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.mgr_m4_e6l1.isHidden = false
        
        self.intervalVal = 7
        self.bulbNo = 6
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen6), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen6(){
        
        print("In screen6")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.mgr_m4_e7l1.isHidden = false
        
        self.intervalVal = 16
        self.bulbNo = 7
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen7), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen7(){
        
        print("In screen7")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.mgr_m4_e8l1.isHidden = false
        
        self.intervalVal = 19
        self.bulbNo = 8
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen8), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen8(){
        
        print("In screen8")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.mgr_m4_e9l1.isHidden = false
        
        self.intervalVal = 13
        self.bulbNo = 9
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen9), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen9(){
        
        print("In screen9")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m4_intro4")
        
        self.mgr_m4_e4l1.isHidden = true
        self.mgr_m4_e4l2.isHidden = true
        self.mgr_m4_e4l3.isHidden = true
        self.mgr_m4_e4l4.isHidden = true
        self.mgr_m4_e4l5.isHidden = true
        self.mgr_m4_e5l1.isHidden = true
        self.mgr_m4_e6l1.isHidden = true
        self.mgr_m4_e7l1.isHidden = true
        self.mgr_m4_e8l1.isHidden = true
        self.mgr_m4_e9l1.isHidden = true
        
        self.mgr_m4_e10l1.isHidden = false
        
        self.intervalVal = 10
        self.bulbNo = 10
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen10), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen10(){
        
        print("In screen10")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m4_intro5_1")
        
        self.mgr_m4_e10l1.isHidden = true
        
        self.mgr_m4_e11l1.isHidden = false
        
        //        UIView.transition(with: self.intro_m1e3l1,
        //                          duration: 2.0,
        //                          options: [.transitionCrossDissolve],
        //                          animations: {
        //                            self.intro_m1e3l1.text = "Mgr_Intro_m4_E3L1".localized
        //            }, completion: nil)
        
        self.intervalVal = 7
        self.bulbNo = 11
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen11), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen11(){
        
        print("In screen11")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m4_intro5_2")
        
        self.mgr_m4_e12l1.isHidden = false
        
        self.intervalVal = 5
        self.bulbNo = 12
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen12), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen12(){
        
        print("In screen12")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m4_intro5_3")
        
        self.mgr_m4_e13l1.isHidden = false
        
        self.intervalVal = 8
        
        self.bulbNo = 13
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen13), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen13(){
        
        print("In screen13")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m4_intro5_4")
        
        self.mgr_m4_e14l1.isHidden = false
        
        self.intervalVal = 8
        self.bulbNo = 14
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen14), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen14(){
        
        print("In screen14")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m4_intro5_5")
        
        self.mgr_m4_e15l1.isHidden = false
        
        self.intervalVal = 10
        
        self.bulbNo = 15
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen15), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen15(){
        
        print("In screen15")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "mgr_m4_intro6")
        
        self.mgr_m4_e11l1.isHidden = true
        self.mgr_m4_e12l1.isHidden = true
        self.mgr_m4_e13l1.isHidden = true
        self.mgr_m4_e14l1.isHidden = true
        self.mgr_m4_e15l1.isHidden = true
        
        self.mgr_m4_e16l1.isHidden = false
        self.mgr_m4_e16l2.isHidden = false
        self.mgr_m4_e16l3.isHidden = false
        self.mgr_m4_e16l4.isHidden = false
        self.mgr_m4_e16l5.isHidden = false
        
        self.mgr_m4_e16b1.isHidden = false
        self.mgr_m4_e16b2.isHidden = false
        self.mgr_m4_e16b3.isHidden = false
        self.mgr_m4_e16b4.isHidden = false
        self.mgr_m4_e16b5.isHidden = false
        
        self.mgr_m4_e16b1.isEnabled = false
        self.mgr_m4_e16b2.isEnabled = false
        self.mgr_m4_e16b3.isEnabled = false
        self.mgr_m4_e16b4.isEnabled = false
        self.mgr_m4_e16b5.isEnabled = false
        
        self.intervalVal = 4
        self.bulbNo = 16
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen16), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen16(){
        
        print("In screen16")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.mgr_m4_e17l1.isHidden = false
        
        self.intervalVal = 3
        self.bulbNo = 17
        
        self.mgr_m4_e16b1.isEnabled = false
        self.mgr_m4_e16b2.isEnabled = false
        self.mgr_m4_e16b3.isEnabled = false
        self.mgr_m4_e16b4.isEnabled = false
        self.mgr_m4_e16b5.isEnabled = false
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen17), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen17(){
        
        print("In screen17")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.mgr_m4_e16b1.isEnabled = true
        self.mgr_m4_e16b2.isEnabled = true
        self.mgr_m4_e16b3.isEnabled = true
        self.mgr_m4_e16b4.isEnabled = true
        self.mgr_m4_e16b5.isEnabled = true
        
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("In Finish Playing")
        playFlagIntroAud = 3
        if let image = UIImage(named: "ic_replay_white_36pt.png") {
            multimediaIntro.setImage(image, for: .normal)
        }
    }
    
    @IBAction func multimediaIntroClicked(_ sender: AnyObject) {
        //        let layer = introductionImage.layer
        
        if (playFlagIntroAud == 3) {
            
            self.introductionImage.image = UIImage(named: "mgr_m4_intro1")
            
            self.bulbNo = 0
            
            self.mgr_m4_e1l1.isHidden = false
            self.mgr_m4_e2l1.isHidden = true
            self.mgr_m4_e2l2.isHidden = true
            self.mgr_m4_e3l1.isHidden = true
            self.mgr_m4_e4l1.isHidden = true
            self.mgr_m4_e4l2.isHidden = true
            self.mgr_m4_e4l3.isHidden = true
            self.mgr_m4_e4l4.isHidden = true
            self.mgr_m4_e4l5.isHidden = true
            self.mgr_m4_e5l1.isHidden = true
            self.mgr_m4_e6l1.isHidden = true
            self.mgr_m4_e7l1.isHidden = true
            self.mgr_m4_e8l1.isHidden = true
            self.mgr_m4_e9l1.isHidden = true
            self.mgr_m4_e10l1.isHidden = true
            self.mgr_m4_e11l1.isHidden = true
            self.mgr_m4_e12l1.isHidden = true
            self.mgr_m4_e13l1.isHidden = true
            self.mgr_m4_e14l1.isHidden = true
            self.mgr_m4_e15l1.isHidden = true
            self.mgr_m4_e16l1.isHidden = true
            self.mgr_m4_e16l2.isHidden = true
            self.mgr_m4_e16l3.isHidden = true
            self.mgr_m4_e16l4.isHidden = true
            self.mgr_m4_e16l5.isHidden = true
            self.mgr_m4_e17l1.isHidden = true
            
            self.mgr_m4_e16b1.isEnabled = false
            self.mgr_m4_e16b2.isEnabled = false
            self.mgr_m4_e16b3.isEnabled = false
            self.mgr_m4_e16b4.isEnabled = false
            self.mgr_m4_e16b5.isEnabled = false
            
            intervalVal = 5
            self.bulbNo = 1
            
            let timeInterval: TimeInterval = TimeInterval(intervalVal)
            self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen1), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
            
            playFlagIntroAud = 1
            if let image = UIImage(named: "ic_pause_white_36pt.png") {
                multimediaIntro.setImage(image, for: .normal)
                audioPlayerIntroAud?.play()
                //                resumeLayer(layer: layer)
            }
        } else if (playFlagIntroAud == 2) {
            
            let timeInterval: TimeInterval = TimeInterval(self.intervalVal)
            print("Interval: " + String(self.intervalVal))
            
            switch self.bulbNo {
            case 1: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen1), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 2: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen2), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 3: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen3), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 4: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen4), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 5: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen5), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 6: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen6), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 7: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen7), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 8: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen8), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 9: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen9), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 10: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen10), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 11: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen11), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 12: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen12), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 13: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen13), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 14: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen14), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 15: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen15), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 16: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen16), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 17: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4IntroController.screen17), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module4IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            default: break
            }
            
            playFlagIntroAud = 1
            if let image = UIImage(named: "ic_pause_white_36pt.png") {
                multimediaIntro.setImage(image, for: .normal)
                audioPlayerIntroAud?.play()
            }
        } else {
            
            self.GTTimer.invalidate()
            self.GTTimer2.invalidate()
            
            playFlagIntroAud = 2
            if let image = UIImage(named: "ic_play_arrow_white_36pt.png") {
                multimediaIntro.setImage(image, for: .normal)
                audioPlayerIntroAud?.pause()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        audioPlayerIntroAud?.stop()
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
    }
    
}
