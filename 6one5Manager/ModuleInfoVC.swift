//
//  ModuleInfoVC.swift
//  6one5Manager
//
//  Created by enyotalearning on 17/01/17.
//  Copyright © 2017 enyotalearning. All rights reserved.
//

import UIKit

class ModuleInfoVC: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    var tableViewData = ["Assessment Explanation Summary", "Welcome", "Understand", "Advise", "Close", "Thank","Handling Customer Complaints"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        /*if(searchActive) {
         return filtered.count
         }*/
        return tableViewData.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell;
        /*if(searchActive){
         cell.textLabel?.text = filtered[(indexPath as NSIndexPath).row]
         } else {
         cell.textLabel?.text = learnerDetails.learnerProfileArray[(indexPath as NSIndexPath).row];
         }*/
        
        cell.textLabel?.text = tableViewData[indexPath.row]
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        //let indexPath = tableView.indexPathForSelectedRow //optional, to get from any UIButton for example
        
        if indexPath.row == 0
        {
            if Constants.device == "iPhone" {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController:ReviewInfoViewController = storyboard.instantiateViewController(withIdentifier: "ReviewInfoViewController") as! ReviewInfoViewController
                //let viewController:ReviewModuleInfoViewController = storyboard.instantiateViewController(withIdentifier: "ReviewModuleInfoViewController") as! ReviewModuleInfoViewController
                self.navigationController?.present(viewController, animated: true, completion: nil)
            }else{
                let storyboard = UIStoryboard(name: "Storyboard_ipad", bundle: nil)
                let viewController:ReviewInfoViewController = storyboard.instantiateViewController(withIdentifier: "ReviewInfoViewController") as! ReviewInfoViewController
                //let viewController:ReviewModuleInfoViewController = storyboard.instantiateViewController(withIdentifier: "ReviewModuleInfoViewController") as! ReviewModuleInfoViewController
                self.navigationController?.present(viewController, animated: true, completion: nil)
            }
            print("Global Help is selected")
            
           
            
        }
        else
        {
            print("Help is selected")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            // let viewController:ReviewInfoViewController = storyboard.instantiateViewController(withIdentifier: "ReviewInfoViewController") as! ReviewInfoViewController
            
            let viewController:ReviewModuleInfoViewController = storyboard.instantiateViewController(withIdentifier: "ReviewModuleInfoViewController") as! ReviewModuleInfoViewController
            viewController.moduleid = indexPath.row
            self.navigationController?.present(viewController, animated: true, completion: nil)
        }
        
    }
    
    
    
    
}
