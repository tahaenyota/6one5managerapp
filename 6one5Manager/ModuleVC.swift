//
//  ModuleVC.swift
//  6one5Manager
//
//  Created by enyotalearning on 02/10/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit
import Charts
import Localize_Swift

@IBDesignable class ModuleVC: UIView
{
    var view:UIView!
    
    
    @IBOutlet weak var chart1: PieChartView!
    @IBOutlet weak var chart2: BarChartView!
    @IBOutlet weak var chart3: BarChartView!
    
    
    @IBOutlet weak var quizScoreTitle: UILabel!
    @IBOutlet weak var managerReviewTitle: UILabel!
    @IBOutlet weak var completionStatus: UILabel!
    
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    let salesTeamPerformanceData =  SalesTeamPerformanceData.sharedInstance
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setup()
    }
    
    func loadViewFromNib() -> UIView {
        let screenSize: CGRect = UIScreen.main.bounds;
        let screenWidth = screenSize.width;
        let screenHeight = screenSize.height;
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "Modules", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight - 200 )//CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        print("Here is Loading from module Nib the width : \(self.frame.size.width)   height :  \(self.frame.size.height)")
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        quizScoreTitle.text = "module_completion_status".localized()
        managerReviewTitle.text = "average_manager_review".localized()
        completionStatus.text = "average_quiz_score".localized()
        
        return view
    }
    
    
    func setup() {
        view = loadViewFromNib()
        
        let screenSize: CGRect = UIScreen.main.bounds;
        let screenWidth = screenSize.width;
        let screenHeight = screenSize.height;
        
        print("Here is Loading from Setup the width : \(self.frame.size.width)   height :  \(screenHeight)")
        
        //view.frame = bounds
        //view.frame = self.frame
        //view.autoresizingMask = [.FlexibleWidth, .FlexibleHeight] //UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing. FlexibleHeightaddSubview(view)
        view.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight - 200 )//CGRect(0, 0, self.frame.size.width, self.frame.size.height);
        addSubview(view)
        load()
        
    }
    
    /*func scrollViewDidScroll(scrollView: UIScrollView) {
     if scrollView.contentOffset.x>0 {
     scrollView.contentOffset.x = 0
     scrollView.contentOffset.y = 0
     }
     }*/
    
    func load()
    {
        //let dollars1 = [10.0, 20.0, 30.0]
        //let months = ["Module 1", "Module 2", "Module 3"]
        
        let chart = ChartConfig.sharedInstance
        
        //quiz data
        chart.setPieChartData(dataPoints: salesTeamPerformanceData.courseStatusDataPoint, values: salesTeamPerformanceData.courseStatusDataValues, chartObject: chart1, chartLabel: "Chart 1")
        chart1.chartDescription?.text = ""
        chart1.legend.horizontalAlignment = .center
        
        //chart1.noDataText = "No Data for creating chart"
        
        chart.setBarChartData(dataPoints: salesTeamPerformanceData.quizDataPoints, values: salesTeamPerformanceData.quizDataValues, chartObject: chart3, chartLabel: "Chart 3")
        chart3.chartDescription?.text = ""
        //chart3.legend.position = .rightOfChartCenter
        chart3.legend.horizontalAlignment = .center
        //chart3.noDataText = "No Data for creating chart"
        
        //BarChart
        chart.setBarChartData(dataPoints: salesTeamPerformanceData.reviewDataPoints, values: salesTeamPerformanceData.reviewDataValues, chartObject: chart2, chartLabel: "Chart 2")
        chart2.legend.horizontalAlignment = .center
        chart2.chartDescription?.text = ""
        //chart2.noDataText = "No Data for creating chart"
        chart1.dragDecelerationEnabled = false
        //chart1.rotationEnabled = false
        
        chart2.dragDecelerationEnabled = false
        // chart2.rotationEnabled = false
        
        chart3.dragDecelerationEnabled = false
        chart1.rotationEnabled = false
        chart1.drawHoleEnabled = true
        
        
        
    }
}
