//
//  DeviceUtil.swift
//  6one5Manager
//
//  Created by enyotalearning on 01/12/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation
import UIKit
class DeviceUtil
{
    class func getDeviceType() -> Int
    {
        // 1. request an UITraitCollection instance
        let deviceIdiom = UIScreen.main.traitCollection.userInterfaceIdiom
        print("Here I am in the Device type")
        // 2. check the idiom
        switch (deviceIdiom) {
            
        case .pad:
            print("--- iPad style UI")
            return 1
        case .phone:
            print("--- iPhone and iPod touch style UI")
            return 2
            
        case .tv:
            print("--- tvOS style UI")
            return 0
        default:
            print("--- Unspecified UI idiom")
            return 0
        }
    }
    
}
