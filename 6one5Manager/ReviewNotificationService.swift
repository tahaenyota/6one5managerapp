//
//  ReviewNotificationService.swift
//  6one5Manager
//
//  Created by enyotalearning on 03/10/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation

public class ReviewNotificationService : NSObject
{
    
    //using Singleton pattern for single object handling...
    class var sharedInstance: ReviewNotificationService
    {
        //2
        struct Singleton
        {
            //3
            static let instance = ReviewNotificationService()
        }
        //4
        return Singleton.instance
    }
    
    func loadData(completionHandler:@escaping (ReviewNotificationData)->())
    {
        let headers = [
            "cache-control": "no-cache",
            ]
        let login = Login.sharedInstance
        let apiURL: String = WebAPI.MANAGERGETNOTIFS + login.getManagerId()
        //"https://key2train.in/admin/api/v1/module_review_request/41" //WebAPI.MANAGERGETNOTIFS + "41" // login.getManagerId() //"
        
        let request = NSMutableURLRequest(url: NSURL(string: apiURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("Error: \(error)")
                let reviewNotificationData = ReviewNotificationData.sharedInstance
                completionHandler(reviewNotificationData)
            } else
            {
                //let httpResponse = response as? HTTPURLResponse
                print("HTTP Response for the learner list @@@@ : \(data)")
                
                do
                {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                    {
                        let reviewNotificationData = ReviewNotificationData.sharedInstance
                        
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        
                        
                        if statusVal == nil
                        {
                            print("status val is null")
                            let statusVal = convertedJsonIntoDict["status"] as? String
                            print("status valule is \(statusVal)")
                            let login = Login.sharedInstance
                            login.status = String(describing: convertedJsonIntoDict["status"] as? Bool)
                            login.message = convertedJsonIntoDict["message"] as? String
                            
                            
                        }
                        else
                        {
                            
                            if(statusVal)!
                            {
                                
                                var count = convertedJsonIntoDict["count"] as? Int
                                count = count! - 1
                                for index in 0...count!
                                {
                                    
                                    if let module1Data = convertedJsonIntoDict["i\(index)"] as? NSDictionary
                                    {
                                        
                                        //print(" --- UserName \( module1Data["user_id"])")
                                        let reviewDetails:ReviewDetails = ReviewDetails()
                                        let userId:String = (module1Data["user_id"] as? String)!
                                        let user_name:String = (module1Data["user_name"] as? String)!
                                        let module_id:String = (module1Data["module_id"] as? String)!
                                        let module_name:String = (module1Data["module_name"] as? String)!
                                        //var course_id:String = (String(module1Data["course_id"]) as? String)!
                                        
                                        //print(" &&& UserName \( userId)")
                                        reviewDetails.setUp(userId: userId,
                                                            userName: user_name,
                                                            moduleId: module_id,
                                                            moduleName: module_name,
                                                            courseId: "1" )
                                        //reviewDetails.desc();
                                        /*for (key, value) in module1Data
                                         {
                                         //print("quizData: Value: \(value) for key: \(key)")
                                         }*/
                                        reviewNotificationData.addReviewDetails(reviewDetails: reviewDetails)
                                        
                                    }
                                }
                                
                                
                                
                                
                            }// end of if
                        }
                        
                        completionHandler(reviewNotificationData)
                    }
                    
                } catch let error as NSError
                {
                    print(error)
                }
                
                
            }
            
            
        })
        
        dataTask.resume()
        
    }
    
}
