//
//  ColorConfig.swift
//  6one5Manager
//
//  Created by enyotalearning on 05/10/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation
import UIKit

public class ColorConfig
{
    var barColors = [UIColor]()
    
    //Singleton pattern
    //This is to make sure will use only one object for creating the login
    class var sharedInstance: ColorConfig
    {
        //2
        struct Singleton {
            //3
            static let instance = ColorConfig()
        }
        //4
        return Singleton.instance
    }
    
    
    init()
    {
        setBarColors()
    }
    
    func setBarColors()
    {
        let s0:UIColor = UIColor(red: 89/255, green: 204/255, blue:245/255, alpha: 1) // Light Blue
        let s1:UIColor = UIColor(red: 254/255, green: 214/255, blue: 66/255, alpha: 1)//Yellow
        let s2:UIColor = UIColor(red: 252/255, green:176/255, blue: 22/255, alpha: 1) //Light orange
        let s3:UIColor = UIColor(red: 242/255, green: 100/255, blue: 33/255, alpha: 1) //Dark orange
        let s4:UIColor = UIColor(red: 237/255, green: 33/255, blue: 124/255, alpha: 1) //Pink
        let s5:UIColor = UIColor(red: 219/255, green: 27/255, blue: 91/255, alpha: 1) //Dark pink
        barColors = [s0,s1,s2,s3,s4,s5]
        
        //I have instruction : there is only 6 color at present
    }
    
    func getBarColors() -> Array<UIColor>
    {
        return barColors
    }
    
}
