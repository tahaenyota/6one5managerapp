//
//  HomeVC.swift
//  6one5Manager
//
//  Created by enyotalearning on 29/09/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit
import Foundation
import Localize_Swift

class HomeVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    var login =  Login.sharedInstance
    var User_Status_Str = ""
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet var DashboardbackUIView: UIView!
    @IBOutlet weak var userImage: UIImageView!
    
    @IBOutlet weak var managerHomeCollectionV: UICollectionView!
    
    @IBOutlet weak var profileName: UILabel!
    var listOfImages: [UIImage] = [
        UIImage(named: "module.png")!,
        UIImage(named: "review.png")!,
        UIImage(named: "badges.png")!,
        UIImage(named: "certificate.png")!,
        UIImage(named: "profile_db.png")!,
        UIImage(named: "faqs.png")!]
    
    var progressHUD = ProgressHUD()
    //module,performance,badges,certificate,profile,faq
    
    ////tabBarController?.tabBar.items![0].title = String(format: NSLocalizedString("home_tab", comment: "any comment"), "Peter")
    var listOfDashboardItem = ["modules".localized(),"performance".localized(),"badges".localized(),"certificate".localized() ,"profile".localized(),"faq".localized()]
    
    
    //HOMEVC
    fileprivate let reuseIdentifier = "ManagerHomeGridCell"
    //    private let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! HomeVCCell
        // Configure the cell
        cell.ManagerHomeCVCellGridImg.image = listOfImages[(indexPath as NSIndexPath).row]
        cell.ManagerHomeCVCellGridLbl.text = listOfDashboardItem[(indexPath as NSIndexPath).row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        print((indexPath as NSIndexPath).row)
        switch (indexPath as NSIndexPath).row {
            
        case 0:
            if self.User_Status_Str == "inactive" {
                
            }else{
            performSegue(withIdentifier: "ModulesVwCtrlSegueID2", sender: nil)
            }
            break
        case 1:
            if self.User_Status_Str == "inactive" {
                
            }else{
            tabBarController?.selectedIndex = 1
            }
            break
        case 2:
            if self.User_Status_Str == "inactive" {
                
            }else{
            
            WebAPI.navigationforBadges = ""
            WebAPI.navigationforBadges = "homevc"
            
            var storyboard = UIStoryboard()
            
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
                storyboard = UIStoryboard(name: "Storyboard_ipad", bundle: nil)
            }else{
                storyboard = UIStoryboard(name: "Main", bundle: nil)
            }
            
            let viewController:ManagerBadgesVC = storyboard.instantiateViewController(withIdentifier: "ManagerBadgesVC") as! ManagerBadgesVC
            self.navigationController?.pushViewController(viewController, animated: true)
            }
            break
        case 3:
            performSegue(withIdentifier: "managerc", sender: nil)
            break
            
        case 4:
            if self.User_Status_Str == "inactive" {
                
            }else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController:ProfileViewController = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            self.navigationController?.pushViewController(viewController, animated: true)
            }
            break
        case 5:
            if self.User_Status_Str == "inactive" {
                
            }else{
            performSegue(withIdentifier: "FAQVwCtrlSegueID", sender: nil)
            }
            break
        default:
            print((indexPath as NSIndexPath).row)
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        let widthSize = collectionView.frame.size.width / 2
        print("Here1")
        return CGSize(width: widthSize-1, height: widthSize)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        print("Here2")
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        print("Here3")
        return 1.0
    }
    var notifCount = 0
    var manager_id = "165"
    
    override func viewDidLoad() {
        
        /*_rl = "https://key2train.in/admin/api/v1/learners/41"*/
        
        //doRequestGet(url: url)
        print("Home Screen view Load")
        
        let login =  Login.sharedInstance
        manager_id = login.getManagerId()
        
        self.navigationController?.isNavigationBarHidden = true
        // self.navigationController!.navigationBar.topItem!.title = "";
        super.viewDidLoad()
        profileName.text = login.fName + " " + login.lName
        
        let DashBlueColor = UIColor(red: 0/255.0, green: 174/255.0, blue: 239/255.0, alpha: 1.0)
        self.DashboardbackUIView.backgroundColor = DashBlueColor
        
        self.managerHomeCollectionV.delegate = self
        self.managerHomeCollectionV.dataSource = self
        self.managerHomeCollectionV!.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 1)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        //tabBarController?.tabBar.items![3].badgeValue = String(login.getNotificationcount())//String(notifCount);
    }
    
    func handleTap(sender : UIView) {
        
        print("Tap Gesture recognized")
    }
    override func viewWillAppear(_ animated: Bool) {
        self.hidesBottomBarWhenPushed = false
        self.navigationItem.title = ""
        print()
        self.navigationController?.isNavigationBarHidden = true
        print(" will apper")
        
        let device = UIDevice.current.model
        
        self.userImage.image = UIImage(named: "profile_db")
        self.userImage.layoutIfNeeded()
        self.userImage.layer.borderWidth = 2
        self.userImage.layer.masksToBounds = false
        self.userImage.layer.borderColor = UIColor.white.cgColor
        
        if(device == "iPhone") {
            self.userImage.layer.cornerRadius = 60
        } else {
            self.userImage.layer.cornerRadius = 100
        }
        self.userImage.clipsToBounds = true
        
        
        let languageUtility = LanguageUtility.sharedInstance
        languageUtility.updateLanguage()
        
        tabBarController?.tabBar.items![0].title = "home_tab".localized()
        tabBarController?.tabBar.items![1].title = "performance_tab".localized()
        tabBarController?.tabBar.items![2].title = "teams_tab".localized()
        tabBarController?.tabBar.items![3].title = "flag_tab".localized()
        tabBarController?.tabBar.items![4].title = "help_tab".localized()
        
        /*let noitf_count = self.login.notificationcount
         if noitf_count != 0 {
         tabBarController?.tabBar.items![3].badgeValue = "\(noitf_count)"
         }*/
        getProfileDetails()
        logoutButton.setTitle("logout".localized(), for: UIControlState.normal)
        
        
        
        if(device == "iPhone") {
            //Constants.fontTag = "<font face ='Helvetica Neue' size='3'>"
            //Constants.FAQfontTag = "<font face ='Helvetica Neue' size='5'>"
            Constants.DiscripterFontTag = "<font face ='Helvetica Neue' size='4'>"
        } else {
          //  Constants.fontTag = "<font face ='Helvetica Neue' size='6'>"
           // Constants.FAQfontTag = "<font face ='Helvetica Neue' size='6'>"
            Constants.DiscripterFontTag = "<font face ='Helvetica Neue' size='5'>"
        }
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func logoutTapped(_ sender: UIButton)
    {
        let appDomain = Bundle.main.bundleIdentifier
        UserDefaults.standard.removePersistentDomain(forName: appDomain!)
        
        let login = Login.sharedInstance
        login.logoutFromDeviceCache()
        print("Direct home screen")
        DispatchQueue.main.async(execute: {
            //self.imageView.image = image;
            
            let language = Language.sharedInstance
            language.setLanguage(language: login.getPraferdLanguage())
            let languageUtility = LanguageUtility.sharedInstance
            languageUtility.updateLanguage()
            self.dismiss(animated: true, completion: nil)
            /*  let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
             let initViewController: LoginVC = storyboard.instantiateViewController(withIdentifier: "LoginVC") as UIViewController as! LoginVC
             self.present(initViewController, animated: true, completion: nil)
             //self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)*/
            
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getNotifs()
    {
        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            ]
        
        let apiURL: String = WebAPI.USERNOTIFICATION + manager_id // Constants.managerGetNotifsURL + manager_id
        
        let request = NSMutableURLRequest(url: NSURL(string: apiURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        print ("Notification Count :\(apiURL)")
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("Error: \(error)")
                let alert = UIAlertController(title: "Notifications", message: "Error in connection. Please check your internet connection and try again.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                self.progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
            } else {
                let httpResponse = response as? HTTPURLResponse
                print("HTTP Response: \(httpResponse)")
                
                do {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        print("Notification result is :",convertedJsonIntoDict)
                        
                        // Get value by key
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        //print(statusVal!)
                        
                        if(statusVal != nil)
                        {
                            
                            DispatchQueue.main.sync {
                                let notifs = convertedJsonIntoDict["count"] as? Int
                                
                                //self.notifCount = notifs
                                if notifs == nil{
                                    self.tabBarController?.tabBar.items![3].badgeValue = String("0")
                                }else{
                                    self.tabBarController?.tabBar.items![3].badgeValue = String(describing: notifs!)
                                }
                                
                                self.progressHUD.hide()
                                UIApplication.shared.endIgnoringInteractionEvents()
                            }
                            
                            
                            
                        } else {
                            var error_msg:NSString
                            if convertedJsonIntoDict["message"] as? NSString != nil {
                                error_msg = convertedJsonIntoDict["message"] as! NSString
                            } else {
                                error_msg = "Unknown Error"
                            }
                            print("error_msg",error_msg)
                            self.progressHUD.hide()
                            UIApplication.shared.endIgnoringInteractionEvents()
                        }
                    }
                } catch let error as NSError {
                    print(error)
                    self.progressHUD.hide()
                    UIApplication.shared.endIgnoringInteractionEvents()
                }
            }
        })
        
        dataTask.resume()
    }
    
    func getProfileDetails()
    {
        progressHUD = ProgressHUD()
        self.view.addSubview(progressHUD)
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            ]
        
        let prefs:UserDefaults = UserDefaults.standard
        let user_id = prefs.value(forKey: "user_id") as! String!
        let login = Login.sharedInstance
        let apiURL: String = "https://key2train.in/admin/api/v1/get_user_profile_details/" + login.getManagerId()
        let request = NSMutableURLRequest(url: NSURL(string: apiURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        print("Status check \(apiURL)")
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            DispatchQueue.main.async() {
                if (error != nil) {
                    
                    
                    print("Network Error: \(error)")
                    //                    let alert = UIAlertController(title: "profileupdate".localized(), message: "profileerrordetails".localized(), preferredStyle: UIAlertControllerStyle.alert)
                    //                    alert.addAction(UIAlertAction(title: "ok".localized(), style: UIAlertActionStyle.default, handler: nil))
                    // self.present(alert, animated: true, completion: nil)
                    self.getNotifs()
                } else {
                    let httpResponse = response as? HTTPURLResponse
                    print("HTTP Response: \(httpResponse)")
                    
                    do {
                        if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                            
                            print("profile Api call :\(apiURL)" +  "Print Profile details ",convertedJsonIntoDict)
                            
                            // Get value by key
                            // let statusVal = convertedJsonIntoDict["status"] as? String
                            let statusVal = convertedJsonIntoDict["status"] as? Bool
                           // self.User_Status_Str = (convertedJsonIntoDict["status"] as? String)!
                            print("status value is \(statusVal!)")
                            
                            if(statusVal == true)
                            {
                                DispatchQueue.main.async(){
                                    
                                    let details = convertedJsonIntoDict["details"] as! NSDictionary
                                    
                                    // let userId = details["l_id"] as? String
                                    let lFName = details["l_fname"] as? String
                                    let lLName = details["last_name"] as? String
                                    // let pEmail = details["personal_email"] as? String
                                    //let pPhone = details["personal_phone"] as? String
                                    let profilePic = details["profile_pic_link"] as? String
                                    let prefs:UserDefaults = UserDefaults.standard
                                    prefs.set(profilePic, forKey:"profile_pic")
                                    prefs.set(lFName! + " " + lLName! , forKey: "username")
                                    prefs.synchronize()
                                    
                                    let username = prefs.value(forKey: "username") as! String!
                                    self.profileName.text = username
                                    
                                    if profilePic != ""
                                    {
                                        print("Here is the profile pic link : \(profilePic)")
                                        
                                        let url = URL(string: profilePic!)// + "1617_1485337054.png")
                                        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                                        self.userImage.image = UIImage(data: data!)
                                    }
                                    
                                    let image2 = self.userImage.image?.resizeWith(width: Constants.profile_image_size)
                                    
                                    self.userImage.image = image2
                                    
                                }
                                //                            }else if(statusVal == "inactive"){
                                //                               self.progressHUD.hide()
                                //                                self.navigationController?.popToRootViewController(animated: true)
                                //                            }
                            }else{
                                
                                
                                /*var error_msg:NSString
                                 if convertedJsonIntoDict["message"] as? NSString != nil
                                 {
                                 error_msg = convertedJsonIntoDict["message"] as! NSString
                                 } else {
                                 error_msg = "Unknown Error"
                                 }
                                 print("error_msg",error_msg)*/
                                
                                /*progressHUD.hide();
                                 
                                 
                                 let alertController = UIAlertController(title: "profileupdate", message: "No review request" , preferredStyle: .alert)
                                 let OKAction = UIAlertAction(title: "ok".localized, style: .default) { (action:UIAlertAction!) in
                                 //self.navigationController?.popViewController(animated: true)
                                 self.navigationController?.isNavigationBarHidden = false
                                 self.view.removeFromSuperview()
                                 }
                                 alertController.addAction(OKAction)
                                 self.present(alertController, animated: true, completion:nil)*/
                                
                            }
                            self.getNotifs()
                        }
                    } catch let error as NSError {
                        print(error)
                        self.getNotifs()
                        
                        
                    }
                    
                    
                }
            }
            
        })
        
        dataTask.resume()
    }
    
}
