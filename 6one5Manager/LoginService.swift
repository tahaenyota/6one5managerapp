//
//  LoginService.swift
//  6one5Manager
//
//  Created by enyotalearning on 29/09/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit
public class LoginService : NSObject
{
    //using Singleton pattern for single object handling...
    class var sharedInstance: LoginService
    {
        //2
        struct Singleton {
            //3
            static let instance = LoginService()
        }
        //4
        return Singleton.instance
    }
    
    func validateLoginDetailsOnline(userName:String, passwd:String, preferdLanguage:String, completionHandler:@escaping (Login)->())
    {
        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            ]
        let login = Login.sharedInstance
        let dataStr: String = "data={ \"uname\" : \"\(userName as NSString)\" , \"upass\" : \"\(passwd as NSString)\" }"
        //print("POST Params: \(dataStr)")
        let postData = NSMutableData(data: dataStr.data(using: String.Encoding.utf8)!)
        
        let loginURL: String = WebAPI.LOGIN
        let request = NSMutableURLRequest(url: NSURL(string: loginURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("Error: \(error)")
                DispatchQueue.main.async {
                    /*let alert = UIAlertController(title: "Login Error", message: "Error in connection. Please check your internet connection and try again.", preferredStyle: UIAlertControllerStyle.alert)
                     alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                     self.present(alert, animated: true, completion: nil)*/
                    
                    login.setErrorMessage(msg: "Server Error Please check the server")
                    login.setIsLogin(islogin: "0")
                    completionHandler(login)
                }
            } else {
                let httpResponse = response as? HTTPURLResponse
                //var errorFlag = 0
                _ = "Invalid Credentials"
                print("Response: \(httpResponse)")
                do {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        print("@@@Print converted dictionary",convertedJsonIntoDict)
                        
                        // Get value by key
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        let user_id  = convertedJsonIntoDict["user_id"] as? String
                        let course_id  = convertedJsonIntoDict["course_id"] as? String
                        let fName  = convertedJsonIntoDict["first_name"] as? String
                        let lName  = convertedJsonIntoDict["last_name"] as? String
                        let lemail  = convertedJsonIntoDict["email"] as? String
                        let lphone  = convertedJsonIntoDict["phone"] as? String
                        let notificationcount = convertedJsonIntoDict ["notification_count"] as? Int
                        let termsAccepted = convertedJsonIntoDict["terms_accepted"] as? String
                        //  let profilepiclLink = convertedJsonIntoDict["profile_pic_link"]as? String
                        //print("User Id will set to \(user_id)")
                        //let UserRole = convertedJsonIntoDict["user_role"] as? String
                        // if(statusVal == true && UserRole == "LM")
                        
                        //Login service
                        
                        
                        if(statusVal == true)
                        {
                            login.setup(userName: userName, password: passwd, id: user_id!, courseId: course_id!, isLoggedIn: "1", fname:fName!, lName:lName!,language: preferdLanguage, notification_count: notificationcount! as Int, termAccepted: termsAccepted!)
                            login.setErrorMessage(msg: "")
                            login.saveLoginToDevice()
                            login.fName = fName
                            login.lName = lName
                            
                            let Module1 = convertedJsonIntoDict["Leadership"] as! NSDictionary
                            let Module2 = convertedJsonIntoDict["Effective Communication"] as! NSDictionary
                            let Module3 = convertedJsonIntoDict["High Performance Culture"] as! NSDictionary
                            let Module4 = convertedJsonIntoDict["Manager - Coaching"] as! NSDictionary
                            
                            
                            let prefs = UserDefaults.standard
                            prefs.set(Module1, forKey:"Module1")
                            prefs.set(Module2, forKey:"Module2")
                            prefs.set(Module3, forKey:"Module3")
                            prefs.set(Module4, forKey:"Module4")
                            
                            
                            //login.profile_pic_link = profilepiclLink
                            completionHandler(login)
                        }
                        else
                        {
                            var error_msg:NSString
                            if convertedJsonIntoDict["message"] as? NSString != nil {
                                error_msg = convertedJsonIntoDict["message"] as! NSString
                            } else {
                                error_msg = "Unknown Error"
                            }
                            //errorFlag = 1
                            print("error_msg",error_msg)
                            login.setErrorMessage(msg: error_msg as String)
                            login.setIsLogin(islogin: "0")
                            completionHandler(login)
                        }
                    }
                } catch let error as NSError
                {
                    print(error)
                }
                
            }
        })
        
        dataTask.resume()
    }//end of validation
}
