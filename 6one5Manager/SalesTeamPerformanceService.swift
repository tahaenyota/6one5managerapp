//
//  SalesTeamPerformanceService.swift
//  6one5Manager
//
//  Created by enyotalearning on 02/10/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation

public class SalesTeamPerformanceService : NSObject
{
    
    //using Singleton pattern for single object handling...
    class var sharedInstance: SalesTeamPerformanceService
    {
        //2
        struct Singleton
        {
            //3
            static let instance = SalesTeamPerformanceService()
        }
        //4
        return Singleton.instance
    }
    
    func loadData(completionHandler:@escaping (SalesTeamPerformanceData)->())
    {
        let headers = [
            "cache-control": "no-cache",
            ]
        let login = Login.sharedInstance
        let apiURL: String = WebAPI.TEAMPERFORMANCE +  login.getManagerId()
        //"https://key2train.in/admin/api/v1/team_performance/41"
        
        let request = NSMutableURLRequest(url: NSURL(string: apiURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("Error: \(error)")
                let salesTeamPerformance = SalesTeamPerformanceData.sharedInstance
                completionHandler(salesTeamPerformance)
            } else
            {
                //let httpResponse = response as? HTTPURLResponse
                print("HTTP Response for the learner list @@@@ : \(data)")
                
                do
                {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                    {
                        let salesTeamPerformanceData = SalesTeamPerformanceData.sharedInstance
                        
                        let json = JSON(convertedJsonIntoDict)
                        
                        //let statusVal = convertedJsonIntoDict["status"] as? Bool
                        
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        if statusVal == nil
                        {
                            print("status val is null")
                            let statusVal = convertedJsonIntoDict["status"] as? String
                            print("status valule is \(statusVal)")
                            let login = Login.sharedInstance
                            login.status = String(describing: convertedJsonIntoDict["status"] as? Bool)
                            login.message = convertedJsonIntoDict["message"] as? String
                            
                            
                        }
                        else
                        {
                            
                            if json["quiz"].dictionary != nil
                            {
                                
                                let quizData = convertedJsonIntoDict["quiz"] as! NSDictionary
                                
                                salesTeamPerformanceData.addQuizDataPoints(dataPoint: "Welcome")
                                salesTeamPerformanceData.addQuizDataValues(dataValue: quizData["Welcome"]! as! Double)
                                
                                salesTeamPerformanceData.addQuizDataPoints(dataPoint: "Understand")
                                salesTeamPerformanceData.addQuizDataValues(dataValue: quizData["Understand"]! as! Double)
                                
                                salesTeamPerformanceData.addQuizDataPoints(dataPoint: "Advise")
                                salesTeamPerformanceData.addQuizDataValues(dataValue: quizData["Advise"]! as! Double)
                                
                                salesTeamPerformanceData.addQuizDataPoints(dataPoint: "Close")
                                salesTeamPerformanceData.addQuizDataValues(dataValue: quizData["Close"]! as! Double)
                                
                                salesTeamPerformanceData.addQuizDataPoints(dataPoint: "Thank")
                                salesTeamPerformanceData.addQuizDataValues(dataValue: quizData["Thank"]! as! Double)
                                
                                salesTeamPerformanceData.addQuizDataPoints(dataPoint: "Handling Customer Complaints")
                                salesTeamPerformanceData.addQuizDataValues(dataValue: quizData["Handling Customer Complaints"]! as! Double)
                            }
                            else
                            {
                                print("Error Here I am in the quiz");
                            }
                            
                            
                            if json["review"].dictionary != nil
                            {
                                print("Here I am in the review");
                                
                                let reviewData = convertedJsonIntoDict["review"] as! NSDictionary
                                
                                salesTeamPerformanceData.addReviewDataPoints(dataPoint: "Welcome")
                                salesTeamPerformanceData.addReviewDataValues(dataValue: reviewData["Welcome"]! as! Double)
                                
                                salesTeamPerformanceData.addReviewDataPoints(dataPoint: "Understand")
                                salesTeamPerformanceData.addReviewDataValues(dataValue: reviewData["Understand"]! as! Double)
                                
                                salesTeamPerformanceData.addReviewDataPoints(dataPoint: "Advise")
                                salesTeamPerformanceData.addReviewDataValues(dataValue: reviewData["Advise"]! as! Double)
                                
                                salesTeamPerformanceData.addReviewDataPoints(dataPoint: "Close")
                                salesTeamPerformanceData.addReviewDataValues(dataValue: reviewData["Close"]! as! Double)
                                
                                salesTeamPerformanceData.addReviewDataPoints(dataPoint: "Thank")
                                salesTeamPerformanceData.addReviewDataValues(dataValue: reviewData["Thank"]! as! Double)
                                
                                salesTeamPerformanceData.addReviewDataPoints(dataPoint: "Handling Customer Complaints")
                                salesTeamPerformanceData.addReviewDataValues(dataValue: reviewData["Handling Customer Complaints"]! as! Double)
                            }
                            else
                            {
                                print("Error Here I am in the review");
                                
                            }
                            
                            if json["course_status"].dictionary != nil
                            {
                                let courseStatusData = convertedJsonIntoDict["course_status"] as! NSDictionary
                                
                                print("Here I am in the course_status");
                                
                                salesTeamPerformanceData.addCourseStatusDataPoints(dataPoint: "Not Started")
                                salesTeamPerformanceData.addCourseStatusDataValues(dataValue: courseStatusData["Not Started"]! as! Double)
                                
                                salesTeamPerformanceData.addCourseStatusDataPoints(dataPoint: "In Progress")
                                salesTeamPerformanceData.addCourseStatusDataValues(dataValue: courseStatusData["In Progress"]! as! Double)
                                
                                salesTeamPerformanceData.addCourseStatusDataPoints(dataPoint: "Completed")
                                salesTeamPerformanceData.addCourseStatusDataValues(dataValue: courseStatusData["Completed"]! as! Double)
                            }
                            else
                            {
                                print("Error Here I am in the course_status");
                            }
                            
                            
                            
                            
                            if json["level_performance"].dictionary != nil
                            {
                                print("Error Here I am in the level_performance");
                                
                                let levelPerformanceData = convertedJsonIntoDict["level_performance"] as! NSDictionary
                                
                                
                                
                                salesTeamPerformanceData.addLevelPerformanceDataPoints(dataPoint: "Beginner")
                                salesTeamPerformanceData.addLevelPerformanceDataValues(dataValue: levelPerformanceData["Beginner"]! as! Double)
                                
                                salesTeamPerformanceData.addLevelPerformanceDataPoints(dataPoint: "Learner")
                                salesTeamPerformanceData.addLevelPerformanceDataValues(dataValue: levelPerformanceData["Learner"]! as! Double)
                                
                                salesTeamPerformanceData.addLevelPerformanceDataPoints(dataPoint: "Professional")
                                salesTeamPerformanceData.addLevelPerformanceDataValues(dataValue: levelPerformanceData["Professional"]! as! Double)
                                
                                salesTeamPerformanceData.addLevelPerformanceDataPoints(dataPoint: "High Performer")
                                salesTeamPerformanceData.addLevelPerformanceDataValues(dataValue: levelPerformanceData["High Performer"]! as! Double)
                                
                                salesTeamPerformanceData.addLevelPerformanceDataPoints(dataPoint: "Expert")
                                salesTeamPerformanceData.addLevelPerformanceDataValues(dataValue: levelPerformanceData["Expert"]! as! Double)
                            }
                            else{
                                print("Error Here I am in the level_performance");
                            }//
                        }
                        completionHandler(salesTeamPerformanceData)
                    }
                    
                } catch let error as NSError
                {
                    print(error)
                }
                
                
            }
            
            
        })
        
        dataTask.resume()
        
    }
    
}
