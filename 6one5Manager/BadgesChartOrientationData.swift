//
//  BadgesChartData.swift
//  6one5Manager
//
//  Created by enyotalearning on 02/12/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation

class BadgesChartOrientaionData
{
    var badgesOrientationArray = [BadgesOrientationData]()
    var idValues = [String]()
    var countValues = [Double]()
    var message:String!
    var status:String!
    
    
    class var sharedInstance: BadgesChartOrientaionData
    {
        //2
        struct Singleton {
            //3
            static let instance = BadgesChartOrientaionData()
        }
        //4
        return Singleton.instance
    }
    
    func addBadgesOrientationArray(badgeOrientationData:BadgesOrientationData)
    {
        badgesOrientationArray.append(badgeOrientationData)
        idValues.append(badgeOrientationData.id)
        countValues.append(Double(badgeOrientationData.cnt)!)
        print("Here is the id values \(Double(badgeOrientationData.cnt)!)")
    }
    
    func getBadgesOrientationArray(index:Int) -> BadgesOrientationData
    {
        let rv:BadgesOrientationData = badgesOrientationArray[index];
        
        return rv
    }
    
    
    func freeBadgesOrientationData()
    {
        badgesOrientationArray = [BadgesOrientationData]()
        idValues = [String]()
        countValues = [Double]()
    }
    
}


class BadgesOrientationData
{
    var id:String!
    var cnt:String!
}

