//
//  Module4IntroPopupsController.swift
//  6one5Manager
//
//  Created by Sumit More on 12/22/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation
import UIKit

class Module4IntroPopupsController: UIViewController {
    
    @IBOutlet weak var questionsExplanationLbl: UILabel!
    @IBOutlet weak var questionLbl: UILabel!
    @IBOutlet weak var bottomBar: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        let introductionScreenView = self.navigationController!.viewControllers[0] as! Module4IntroController
        switch introductionScreenView.bulbNo {
        case 1:
            self.questionLbl.text = "Mgr_Intro_M4_B1".localized()
            self.questionsExplanationLbl.text = "Mgr_Intro_M4_B1_Explanation".localized()
            self.bottomBar.backgroundColor = UIColor.init(red: 238.0/255.0, green: 146.0/255.0, blue: 48.0/255.0, alpha: 1.0)
            break
        case 2:
            self.questionLbl.text = "Mgr_Intro_M4_B2".localized()
            self.questionsExplanationLbl.text = "Mgr_Intro_M4_B2_Explanation".localized()
            self.bottomBar.backgroundColor = UIColor.init(red: 237.0/255.0, green: 108.0/255.0, blue: 48.0/255.0, alpha: 1.0)
            break
        case 3:
            self.questionLbl.text = "Mgr_Intro_M4_B3".localized()
            self.questionsExplanationLbl.text = "Mgr_Intro_M4_B3_Explanation".localized()
            self.bottomBar.backgroundColor = UIColor.init(red: 179.0/255.0, green: 214.0/255.0, blue: 74.0/255.0, alpha: 1.0)
            break
        case 4:
            self.questionLbl.text = "Mgr_Intro_M4_B4".localized()
            self.questionsExplanationLbl.text = "Mgr_Intro_M4_B4_Explanation".localized()
            self.bottomBar.backgroundColor = UIColor.init(red: 9.0/255.0, green: 162.0/255.0, blue: 193.0/255.0, alpha: 1.0)
            break
        case 5:
            self.questionLbl.text = "Mgr_Intro_M4_B5".localized()
            self.questionsExplanationLbl.text = "Mgr_Intro_M4_B5_Explanation".localized()
            self.bottomBar.backgroundColor = UIColor.init(red: 102.0/255.0, green: 0.0/255.0, blue: 153.0/255.0, alpha: 1.0)
            break
        default:
            break
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func closeClick(_ sender: AnyObject) {
        self.view.removeFromSuperview()
    }
}
