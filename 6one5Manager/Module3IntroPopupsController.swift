//
//  Module3IntroPopupsController.swift
//  6one5Manager
//
//  Created by Sumit More on 12/21/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation
import UIKit

class Module3IntroPopupsController: UIViewController {
    
    @IBOutlet weak var questionsExplanationLbl: UILabel!
    @IBOutlet weak var questionLbl: UILabel!
    @IBOutlet weak var bottomBar: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        let introductionScreenView = self.navigationController!.viewControllers[0] as! Module3IntroController
        switch introductionScreenView.bulbNo {
        case 1:
            self.questionLbl.text = "Mgr_Intro_M3_B1".localized()
            self.questionsExplanationLbl.text = "Mgr_Intro_M3_B1_Explanation".localized()
            self.bottomBar.backgroundColor = UIColor.init(red: 85.0/255.0, green: 59.0/255.0, blue: 125.0/255.0, alpha: 1.0)
            break
        case 2:
            self.questionLbl.text = "Mgr_Intro_M3_B2".localized()
            self.questionsExplanationLbl.text = "Mgr_Intro_M3_B2_Explanation".localized()
            self.bottomBar.backgroundColor = UIColor.init(red: 240.0/255.0, green: 199.0/255.0, blue: 42.0/255.0, alpha: 1.0)
            break
        default:
            break
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func closeClick(_ sender: AnyObject) {
        self.view.removeFromSuperview()
    }
}
