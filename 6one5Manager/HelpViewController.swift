//
//  HelpViewController.swift
//  6one5Manager
//
//  Created by enyotalearning on 09/01/17.
//  Copyright © 2017 enyotalearning. All rights reserved.
//

import UIKit

class HelpViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    var sectionTitleArray = [String]()
    var sectionDetailArray = [String]()
    var sectionImageArray = [String]()
    @IBOutlet weak var tableHeightConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = 20.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.tableFooterView = UIView()
        self.tableView.setContentOffset(CGPoint.zero, animated: true)
        //tableHeightConstraint.constant = 3000.0
        getTableData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //tableView = UITableView(frame: UIScreen.main.bounds, style: UITableViewStyle.plain)
    }
    
    func getTableData()
    {
        sectionTitleArray  = ["","HelpSectionTitle1".localized() ,"HelpSectionTitle2".localized() ,"HelpSectionTitle3".localized(),"HelpSectionTitle4".localized(),"HelpSectionTitle5".localized(),"HelpSectionTitle6".localized(),"HelpSectionTitle7".localized() ,"HelpSectionTitle8".localized() ,"HelpSectionTitle9".localized(),"HelpSectionTitle10".localized(),"HelpSectionTitle11".localized(),"HelpSectionTitle12".localized(),"HelpSectionTitle13".localized(),"HelpSectionTitle14".localized(),"","","","","","",""]
        
        sectionDetailArray = ["Header_help".localized(),"HelpSectionDetail1".localized() ,"HelpSectionDetail2".localized() ,"HelpSectionDetail3".localized(),"HelpSectionDetail4".localized(),"HelpSectionDetail5".localized(),"HelpSectionDetail6".localized(),"HelpSectionDetail7".localized() ,"HelpSectionDetail8".localized() , "HelpSectionDetail9".localized(),"HelpSectionDetail10".localized(),"HelpSectionDetail11".localized(),"HelpSectionDetail12".localized(),"HelpSectionDetail13".localized(),"HelpSectionDetail14".localized(),"","","","","","",""]
        
        sectionImageArray = ["","help_login","Help_Dashboard","Help_Modulemenu","Help_Module","Help_cert","Help_performance","Help_Faq","Help_badges","Help_Profile","Help_Team","Help_Notifi","Help_help","Help_PerDet","Help_T&C","","","","","","",""]
    }
    
    
    // TableView DataSource methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sectionTitleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ExpandableCell = tableView.dequeueReusableCell(withIdentifier: "ExpandableCell") as! ExpandableCell
        cell.cellIndex = indexPath.row
        
        cell.selectionStyle = .none
        if indexPath.row >= 15{
            cell.arrowImage .image = nil
            cell.isHidden = true
            
        }else if indexPath.row == 0 {
            
            cell.arrowImage.isHidden = true
            cell.img.isHidden = true
            cell.textview.isHidden = true
            var detailText = sectionDetailArray [indexPath.row]
            if (Constants.device == "iPhone") {
                
                detailText = Constants.Font_iPHONE_Tag + detailText + Constants.Font_CLOSE_Tag
                let theAttributedString = try! NSAttributedString(data:detailText.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                                  options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                                  documentAttributes: nil)
                
                cell.sectionTitleLabel.attributedText = theAttributedString
                
            }else{
                detailText = Constants.Font_iPAD_Tag + detailText + Constants.Font_CLOSE_Tag
                let theAttributedString1 = try! NSAttributedString(data: detailText.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                               documentAttributes: nil)
                
                cell.sectionTitleLabel.attributedText  = theAttributedString1
                // self.message.font = UIFont.systemFont(ofSize: 22)
            }
            cell.isExpanded = false
        }
        else{
            cell.arrowImage.isHidden = false
            cell.img.isHidden = false
            cell.textview.isHidden = false
            
            cell.sectionTitleLabel.isHidden = false
            cell.arrowImage .image = UIImage(named:"arrow_side1")
            cell.img.image = UIImage (named: sectionImageArray[indexPath.row])
            cell.sectionTitleLabel .text = sectionTitleArray [indexPath.row]
            var detailText = sectionDetailArray [indexPath.row]
            if (Constants.device == "iPhone") {
                cell.sectionTitleLabel.font = UIFont.boldSystemFont(ofSize: 16)
                detailText = Constants.Font_iPHONE_Tag + detailText + Constants.Font_CLOSE_Tag
                let theAttributedString = try! NSAttributedString(data:detailText.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                                  options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                                  documentAttributes: nil)
                
                cell.textview.attributedText = theAttributedString
                
            }else{
                cell.sectionTitleLabel.font = UIFont.boldSystemFont(ofSize: 25)
                detailText = Constants.Font_iPAD_Tag + detailText + Constants.Font_CLOSE_Tag
                let theAttributedString1 = try! NSAttributedString(data: detailText.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                                   options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                                   documentAttributes: nil)
                
                cell.textview.attributedText  = theAttributedString1
                // self.message.font = UIFont.systemFont(ofSize: 22)
            }
            cell.isExpanded = false
        }
        return cell
    }
    
    // TableView Delegate methods
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            return
        }
        guard let cell = tableView.cellForRow(at: indexPath) as? ExpandableCell
            else {
                return
                
        }
        
        if cell.arrowImage.image == UIImage(named:"arrow-down_black")  {
            cell.arrowImage.image = UIImage(named:"arrow_side1")
            
        }else{
            cell.arrowImage.image = UIImage(named:"arrow-down_black")
        }
        
       // UIView.animate(withDuration: 0.2, animations: {
            tableView.beginUpdates()
            cell.isExpanded = !cell.isExpanded
            //tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.none, animated: false)
            tableView.endUpdates()
      //  })
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? ExpandableCell
            else {
                return
        }
        // UIView.animate(withDuration: 0.5, animations: {
        //tableView.beginUpdates()
        cell.isExpanded = false
        //tableView.endUpdates()
        //})
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 60.0
        default:
            return UITableViewAutomaticDimension;
        }
    }
}


