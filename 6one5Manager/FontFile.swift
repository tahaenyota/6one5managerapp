//
//  FontFile.swift
//  6one5Manager
//
//  Created by enyotalearning on 21/01/17.
//  Copyright © 2017 enyotalearning. All rights reserved.
//

import Foundation

extension NSMutableAttributedString {
    
    func bold(_ text:String) -> NSMutableAttributedString {
        let attrs:[String:AnyObject] = [NSFontAttributeName : UIFont(name: "AvenirNext-Medium", size: 12)!]
        let boldString = NSMutableAttributedString(string:"\(text)", attributes:attrs)
        self.append(boldString)
        return self
    }
    
    func normal(_ text:String)->NSMutableAttributedString {
        let normal =  NSAttributedString(string: text)
        self.append(normal)
        return self
    }
}
