//
//  Module3ReviewSessionController.swift
//  6one5Manager
//
//  Created by Sumit More on 12/21/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import AVFoundation

class Module3ReviewSessionController: UIViewController, AVAudioPlayerDelegate {
    
    
    @IBOutlet weak var menuButton: UIButton!
    
    @IBOutlet weak var multimediaPWM: UIButton!
    
    @IBOutlet weak var reviewMgrLbl: UILabel!
    
    @IBOutlet weak var reviewMgrBtmTitle: UILabel!
    
    @IBOutlet weak var reviewWithMgrImg: UIImageView!
    
    var playFlagPWM = 1
    var audioPlayerPWM: AVAudioPlayer?
    
    override func viewDidLoad() {
        
        let device = UIDevice.current.model
        if(device == "iPhone") {
            print("Changing size to 16")
            let attrs = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont.systemFont(ofSize: 16.0)
            ]
            self.navigationController!.navigationBar.titleTextAttributes = attrs
            
            let button: UIButton = UIButton(type: UIButtonType.custom)
            button.setImage(UIImage(named: "sof.png"), for: UIControlState.normal)
            button.backgroundColor = UIColor.white
            //set frame
            button.frame = CGRect(x: 0, y: 0, width: 56, height: 45)
            let barButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            
            
            let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            negativeSpacer.width = -17;
            
            self.navigationItem.setRightBarButtonItems([negativeSpacer, barButton ], animated: false)
            
        } else {
            print("Changing size to 32")
            let attrs = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont.systemFont(ofSize: 32.0)
            ]
            self.navigationController!.navigationBar.titleTextAttributes = attrs
            
            let button: UIButton = UIButton(type: UIButtonType.custom)
            button.setImage(UIImage(named: "sof.png"), for: UIControlState.normal)
            button.backgroundColor = UIColor.white
            //set frame
            button.frame = CGRect(x: 0, y: 0, width: 56, height: 45)
            let barButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            
            
            let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            negativeSpacer.width = -21;
            
            self.navigationItem.setRightBarButtonItems([negativeSpacer, barButton ], animated: false)
        }
        
        self.reviewWithMgrImg.image = UIImage(named: "practice_with_customer2")
        
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 36.0/255.0, green: 184.0/255.0, blue: 239.0/255.0, alpha: 1.0)
        
        self.navigationItem.title = "Mgr_M3_ModuleName".localized()
        
        self.reviewMgrBtmTitle.text = "Mgr_M3_ReviewBtmTitle".localized()
        
        self.reviewMgrLbl.text = "Mgr_M3_ReviewSessionLbl".localized()
        
        menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: UIControlEvents.touchUpInside)
        
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        do{
            let urlVideo = Bundle.main.url(forResource: "MGR_M03_S03", withExtension: "mp3")
            audioPlayerPWM = try AVAudioPlayer(contentsOf: urlVideo!)
            audioPlayerPWM!.delegate = self
            audioPlayerPWM!.prepareToPlay()
            audioPlayerPWM!.play()
        } catch {
            print("Error getting the audio file")
        }
        
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("In Finish Playing")
        playFlagPWM = 2
        if let image = UIImage(named: "ic_replay_white_36pt.png") {
            multimediaPWM.setImage(image, for: .normal)
        }
    }
    
    @IBAction func multimediaPWMClicked(_ sender: AnyObject) {
        if (playFlagPWM == 2) {
            playFlagPWM = 1
            if let image = UIImage(named: "ic_pause_white_36pt.png") {
                multimediaPWM.setImage(image, for: .normal)
                audioPlayerPWM?.play()
            }
        } else {
            playFlagPWM = 2
            if let image = UIImage(named: "ic_play_arrow_white_36pt.png") {
                multimediaPWM.setImage(image, for: .normal)
                audioPlayerPWM?.pause()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        audioPlayerPWM?.stop()
    }
    
}
