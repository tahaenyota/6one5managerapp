//
//  ProfileController.swift
//  6one5Manager
//
//  Created by enyotalearning on 18/01/17.
//  Copyright © 2017 enyotalearning. All rights reserved.
//

import Foundation
import UIKit
import libPhoneNumber_iOS

class ProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIPopoverControllerDelegate {
    
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var editPhotoBtn: UIButton!
    @IBOutlet weak var fnameLbl: UILabel!
    @IBOutlet weak var fnameTxt: UITextField!
    @IBOutlet weak var lnameLbl: UILabel!
    @IBOutlet weak var lnameTxt: UITextField!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordLbl: UILabel!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var phoneTxt: UITextField!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    let ImagePicker = UIImagePickerController()
    var popover:UIPopoverController?=nil
    
    @IBOutlet weak var editView: UIView!
    
    @IBAction func editPhotoClick(_ sender: AnyObject) {
        
        
        selectProfPic()
    }
    
    func selectProfPic()
    {
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallary", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        // Add the actions
        ImagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        // Present the controller
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            popover=UIPopoverController(contentViewController: alert)
            popover!.present(from: self.editPhotoBtn.frame, in: self.view, permittedArrowDirections: UIPopoverArrowDirection.any, animated: true)
        }
    }
  
    
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            ImagePicker.sourceType = UIImagePickerControllerSourceType.camera
            self .present(ImagePicker, animated: true, completion: nil)
        }else{
            let alert = UIAlertView()
            alert.title = "Warning"
            alert.message = "You don't have camera"
            alert.addButton(withTitle: "OK")
            alert.show()
        }
    }
    func openGallary(){
        ImagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(ImagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        print("Hi there")
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let profileImage = image
            //            let image2 = profileImage.resize(CGSize(width: Constants.profile_image_size, height: Constants.profile_image_size))
            let image2 = profileImage.resizeWith(width: Constants.profile_image_size)
            self.profileImg.image = image2
            //            self.profileImg.transform = CGAffineTransform.init(rotationAngle: 360.0)
            //
            //            self.profileImg.image = image
            
            if let data = UIImagePNGRepresentation(image) {
                print("Writing image to directory")
                let filename = getDocumentsDirectory().appendingPathComponent("profile_img_6one5.png")
                try? data.write(to: filename)
            }
            //
            //        } else{
            //            print("Something went wrong")
            //        }
            
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    //    private func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
    //        self.profileImg.image = info[UIImagePickerControllerOriginalImage] as? UIImage
    //        self.dismiss(animated: true, completion: nil)
    //    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    @IBAction func saveClick(_ sender: AnyObject) {
        checkAndSendData()
    }
    
    func checkAndSendData() {
        let phoneUtil = NBPhoneNumberUtil()
        do {
            let phoneNumber: NBPhoneNumber = try phoneUtil.parse(self.phoneTxt.text, defaultRegion: "AU")
            if (phoneUtil.isValidNumber(phoneNumber)) || (self.phoneTxt.text == "") {
                let company_email = "profile_company_email_extension".localized()
                print("Company email: \(company_email)")
                let emailTxt_content = self.emailTxt.text
                print("Email Content: \(emailTxt_content)")
                
                if (emailTxt_content?.contains(company_email))! {
                    let alert = UIAlertController(title: "profile_update_email".localized(), message: "profile_update_email_error".localized(), preferredStyle: UIAlertControllerStyle.alert)
                    let OKAction = UIAlertAction(title: "profile_update_ok".localized(), style: .default) { (action:UIAlertAction!) in
                        self.emailTxt.text = ""
                        self.emailTxt.becomeFirstResponder()
                    }
                    alert.addAction(OKAction)
                    self.present(alert, animated: true, completion: nil)
                } else {
                    if isValidEmail(testStr: emailTxt_content!) {
                        sendData()
                    } else {
                        let alert = UIAlertController(title: "profile_update_email".localized(), message: "profile_update_email_valid_error".localized(), preferredStyle: UIAlertControllerStyle.alert)
                        let OKAction = UIAlertAction(title: "profile_update_ok".localized(), style: .default) { (action:UIAlertAction!) in
                            self.emailTxt.becomeFirstResponder()
                        }
                        alert.addAction(OKAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            } else {
                let alert = UIAlertController(title: "profile_update_phone".localized(), message: "profile_update_phone_error".localized(), preferredStyle: UIAlertControllerStyle.alert)
                let OKAction = UIAlertAction(title: "profile_update_ok".localized(), style: .default) { (action:UIAlertAction!) in
                    self.phoneTxt.becomeFirstResponder()
                }
                alert.addAction(OKAction)
                self.present(alert, animated: true, completion: nil)
            }
        } catch let error as NSError
        {
            if(self.phoneTxt.text != "") {
                print("Phone error: \(error)")
                let alert = UIAlertController(title: "profile_update_phone".localized(), message: "profile_update_phone_parse_error".localized(), preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "profile_update_ok".localized(), style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            } else {
                let company_email = "profile_company_email_extension".localized()
                let emailTxt_content = self.emailTxt.text
                
                if (emailTxt_content?.contains(company_email))! {
                    let alert = UIAlertController(title: "profile_update_email".localized(), message: "profile_update_email_error".localized(), preferredStyle: UIAlertControllerStyle.alert)
                    let OKAction = UIAlertAction(title: "profile_update_ok".localized(), style: .default) { (action:UIAlertAction!) in
                        self.emailTxt.becomeFirstResponder()
                    }
                    alert.addAction(OKAction)
                    self.present(alert, animated: true, completion: nil)
                } else {
                    if isValidEmail(testStr: emailTxt_content!) {
                        sendData()
                    } else {
                        let alert = UIAlertController(title: "profile_update_email".localized(), message: "profile_update_email_valid_error".localized(), preferredStyle: UIAlertControllerStyle.alert)
                        let OKAction = UIAlertAction(title: "profile_update_ok".localized(), style: .default) { (action:UIAlertAction!) in
                            self.emailTxt.becomeFirstResponder()
                        }
                        alert.addAction(OKAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    func sendData() {
        /*var boxView = UIView()
         boxView = UIView(frame: CGRect(x: view.frame.midX - 110, y: view.frame.midY - 25, width: 220, height: 50))
         boxView.backgroundColor = UIColor.white
         boxView.alpha = 1
         boxView.layer.cornerRadius = 10
         let activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
         activityView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
         activityView.startAnimating()
         let textLabel = UILabel(frame: CGRect(x: 60, y: 0, width: 200, height: 50))
         textLabel.textColor = UIColor.gray
         textLabel.text = "profile_update_progress".localized()
         boxView.addSubview(activityView)
         boxView.addSubview(textLabel)
         
         view.addSubview(boxView)
         
         UIApplication.shared.beginIgnoringInteractionEvents()*/
        
        let progressHUD = ProgressHUD()
        self.view.addSubview(progressHUD)
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        let boundary = generateBoundaryString()
        
        let headers = [
            "boundary": boundary,
            "content-type": "application/x-www-form-urlencoded",
            ]
        
        let passwordVal = self.passwordTxt.text!
        let firstnameVal = self.fnameTxt.text!
        let lastnameVal = self.lnameTxt.text!
        let emailVal = self.emailTxt.text!
        let phoneVal = self.phoneTxt.text!
        
        let login = Login.sharedInstance
        //let prefs = UserDefaults.standard
        let userIdVal = login.getManagerId()//prefs.string( forKey: "user_id")!
        
        //let base64 =  self.profileImg.base64EncodedStringWithOptions(.allZeros)
        //let imageData:NSData = UIImagePNGRepresentation(self.profileImg.image!)! as NSData
        //var base:String!
        //        if(profileImg.image != nil)
        //        {
       // let base = convertImageToBase64(image: self.profileImg.image!)
        //        }
        
        var base:String = convertImageToBase64(image: UIImage(named: "profile_db")!)
        if(profileImg.image != nil)
        {
            base = convertImageToBase64(image: self.profileImg.image!)
        }
        
        
        let dataStr: String = "data={\"user_id\": \"\(userIdVal)\", \"first_name\": \"\(firstnameVal)\",  \"password\":\"\(passwordVal)\", \"last_name\": \"\(lastnameVal)\", \"personal_email\": \"\(emailVal)\",\"personal_phone\": \"\(phoneVal)\",\"profile_pic\": \"\(base)\" }"
        print("POST Params: \(dataStr)")
        let postData = NSMutableData(data: dataStr.data(using: String.Encoding.utf8)!)
        
        
       // NSString *stringWithSpecialCharacters = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];

        
        let updateURL: String = Constants.salesRepUpdateProfileURL
        let request = NSMutableURLRequest(url: NSURL(string: updateURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                
                print(error)
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "profile_update_title".localized(), message: "profile_update_error".localized(), preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "profile_update_ok".localized(), style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
            } else {
                let httpResponse = response as? HTTPURLResponse
                print("Response: \(httpResponse)")
                do {
                    print("Data: \(data)")
                    
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        print("@@@Print converted dictionary",convertedJsonIntoDict)
                        
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        
                        if(statusVal == true)
                        {
                            
                            let setprefs = UserDefaults.standard
                            let usernameV = firstnameVal + " " + lastnameVal
                            setprefs.set(firstnameVal, forKey: "fName")
                            setprefs.set(lastnameVal, forKey: "lName")
                            setprefs.set(emailVal, forKey: "personal_email")
                            setprefs.set(phoneVal, forKey:"+"+"personal_phone")
                            setprefs.set(passwordVal, forKey: "upass")
                            setprefs.set(usernameV, forKey: "username")
                            setprefs.synchronize()
                            
                            
                            let login = Login.sharedInstance
                            
                            login.setup(userName: usernameV, password: passwordVal, id: login.getManagerId(), courseId: login.courseId, isLoggedIn: login.isLoggedIn, fname: firstnameVal, lName: lastnameVal, language: "en", notification_count: login.getNotificationcount(), termAccepted: login.termAccepted)
                            login.saveLoginToDevice()
                            
                            // let defaults = UserDefaults.standard // This is just to set this is device cache.
                            //print("login details saved in cache \(self.isLoggedIn)")
                            //defaults.set("Coding Explorer", forKey: "userNameKey")
                            //                            defaults.set(emailVal, forKey: "emailid")
                            //                            defaults.set(phoneVal, forKey: "phoneno")
                            //                            defaults.synchronize()
                            DispatchQueue.main.async {
                                progressHUD.hide()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                let alert = UIAlertController(title: "profile_update_title".localized(), message: "profile_update_success".localized(), preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "profile_update_ok".localized(), style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                
                            }
                        }
                        else
                        {
                            progressHUD.hide()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            DispatchQueue.main.async {
                                let alert = UIAlertController(title: "profile_update_title".localized(), message: "profile_update_error".localized(), preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "profile_update_ok".localized(), style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                
                            }
                        }
                    }
                    
                } catch let error as NSError
                {
                    print(error)
                    let alert = UIAlertController(title: "profile_update_title".localized(), message: "profile_update_error".localized(), preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "profile_update_ok".localized(), style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    progressHUD.hide()
                    UIApplication.shared.endIgnoringInteractionEvents()
                }
            }
        })
        
        dataTask.resume()
    }
    
    @IBAction func cancelClick(_ sender: AnyObject) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        self.view.frame.origin.y = 0
        super.touchesBegan(touches, with: event)
    }
    
    func keyboardWillShow(sender: NSNotification) {
        self.view.frame.origin.y = -150
    }
    
    func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        
        let device = UIDevice.current.model
        ImagePicker.delegate = self
        
        self.profileImg.layoutIfNeeded()
        self.profileImg.layer.borderWidth = 2
        self.profileImg.layer.masksToBounds = false
        self.profileImg.layer.borderColor = UIColor.white.cgColor
        if(device == "iPhone") {
            self.profileImg.layer.cornerRadius = 50
        } else {
            self.profileImg.layer.cornerRadius = 100
        }
        self.profileImg.clipsToBounds = true
        self.profileImg.image = UIImage(named: "profile_db.png")
        
        fnameLbl.text = "firstname_lbl".localized()
        lnameLbl.text = "lastname_lbl".localized()
        emailLbl.text = "email_lbl".localized()
        passwordLbl.text = "password_lbl".localized()
        phoneLbl.text = "phone_lbl".localized()
        saveBtn.titleLabel?.text = "save_title".localized()
        cancelBtn.titleLabel?.text = "cancel_title".localized()
        
        let prefs:UserDefaults = UserDefaults.standard
        /* let firstName = prefs.value(forKey: "fName") as! String!
         let lastName = prefs.value(forKey: "lName") as! String!
         let email = prefs.value(forKey: "personal_email") as! String!
         let password = prefs.value(forKey: "upass") as! String!
         let phone = prefs.value(forKey: "personal_phone") as! String!
         */
        /* let login = Login.sharedInstance
         fnameTxt.text = login.fName
         lnameTxt.text = login.lName
         passwordTxt.text = login.password
         //profileImg.image = log
         let prefs:UserDefaults = UserDefaults.standard
         let emailId = prefs.value(forKey: "personal_email") as! String!
         let phoneNo = prefs.value(forKey: "personal_phone") as! String!
         if emailId != nil
         {
         emailTxt.text = emailId
         }
         if phoneNo != nil
         {
         phoneTxt.text = phoneNo
         }*/
        
        
        let tapEditProf = UITapGestureRecognizer(target: self, action: #selector(ProfileViewController.selectProfPic))
        self.editView.addGestureRecognizer(tapEditProf)
        
        
        if (prefs.value(forKey: "profile_pic") != nil)
        {
            let profile_pic:String = prefs.value(forKey: "profile_pic") as! String!
            
            if profile_pic != ""
            {
                let url = URL(string: profile_pic)
                let data = try? Data(contentsOf: url!)
                self.profileImg.image = UIImage(data:data!)
            }
        }
        
    }
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        if(testStr == "") {
            return true
        } else {
            let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
            
            let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            return emailTest.evaluate(with: testStr)
        }
    }
    
    func convertImageToBase64(image: UIImage) -> String {
        
        let imageData = UIImagePNGRepresentation(image)
        let base64String = imageData?.base64EncodedString(options: .lineLength64Characters)//  .base64EncodedStringWithOptions(.allZeros)
        
        print("Here we are doing the base 64 \(base64String)")
        return base64String!
        
    }
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            self.getProfileData()
        }
    }
    func getProfileData()
    {
        let progressHUD = ProgressHUD()
        self.view.addSubview(progressHUD)
        
        
        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            ]
        let login = Login.sharedInstance
        let apiURL: String = "https://key2train.in/admin/api/v1/get_user_profile_details/" + login.getManagerId()
        
        let request = NSMutableURLRequest(url: NSURL(string: apiURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            DispatchQueue.main.async() {
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
            }
            if (error != nil) {
                print("Network Error: \(error)")
                let alert = UIAlertController(title: "modelreviewsubmission".localized(), message: "errordetails".localized(), preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "ok".localized(), style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            } else {
                let httpResponse = response as? HTTPURLResponse
                print("HTTP Response: \(httpResponse)")
                
                do {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        print("Print converted dictionary 111 ",convertedJsonIntoDict)
                        
                        // Get value by key
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        print("status value is \(statusVal!)")
                        
                        if(statusVal! == true)
                        {
                            OperationQueue.main.addOperation{
                                let tempDict = convertedJsonIntoDict ["details"] as? NSDictionary
                                let personal_email = tempDict?["personal_email"] as? String
                                let personal_phone  = tempDict?["personal_phone"] as? String
                                let l_fname  = tempDict?["l_fname"] as? String
                                let last_name  = tempDict?["last_name"] as? String
                                
                                
                                self.emailTxt.text = personal_email
                                var trimmedString = personal_phone?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                              
                                if trimmedString == ""{
                                    self.phoneTxt.text = trimmedString!

                                }else{
                                    self.phoneTxt.text = "+"+trimmedString!

                                }
                                self.fnameTxt.text = l_fname
                                self.lnameTxt.text = last_name
                                
                                progressHUD.hide()
                            }
                        }                     }
                } catch let error as NSError {
                    print(error)
                    progressHUD.hide()
                    
                    
                }
                
                
            }
        })
        
        dataTask.resume()
    }
    
}





/*extension UIImage {
 func resizeWith(percentage: CGFloat) -> UIImage? {
 let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: size.width * percentage, height: size.height * percentage)))
 imageView.contentMode = .scaleAspectFit
 imageView.image = self
 UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
 guard let context = UIGraphicsGetCurrentContext() else { return nil }
 imageView.layer.render(in: context)
 guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
 UIGraphicsEndImageContext()
 return result
 }
 func resizeWith(width: CGFloat) -> UIImage? {
 let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))))
 imageView.contentMode = .scaleAspectFit
 imageView.image = self
 UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
 guard let context = UIGraphicsGetCurrentContext() else { return nil }
 imageView.layer.render(in: context)
 guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
 UIGraphicsEndImageContext()
 return result
 }
 }*/




