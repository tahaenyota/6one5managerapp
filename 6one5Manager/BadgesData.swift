//
//  BadgesData.swift
//  6one5Manager
//
//  Created by enyotalearning on 02/12/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation

class BadgesData
{
    var repBadgesDetailsArray = [RepresentativeBadges]()
    var message:String!
    var status:String!
    
    
    class var sharedInstance: BadgesData
    {
        //2
        struct Singleton {
            //3
            static let instance = BadgesData()
        }
        //4
        return Singleton.instance
    }
    
    func addRepBadgesDetails(repBadgesDetails:RepresentativeBadges)
    {
        repBadgesDetailsArray.append(repBadgesDetails)
    }
    
    func getBadgesDetails(index:Int) -> RepresentativeBadges
    {
        let rv:RepresentativeBadges = repBadgesDetailsArray[index];
        
        return rv
    }
    
    
    func freeBadgesData()
    {
        repBadgesDetailsArray = [RepresentativeBadges]()
    }
}

class RepresentativeBadges
{
    var id:String!
    var badgeId:String!
    var fristName:String!
    var lastName:String!
}
