//
//  ExpandableCell.swift
//  ExpandableCellsExample
//
//  Created by DC on 28.08.2016.
//  Copyright © 2016 Dawid Cedrych. All rights reserved.
//

import UIKit

class ExpandableCell: UITableViewCell {
    
    @IBOutlet weak var img: UIImageView!
    
    
    @IBOutlet weak var sectionTitleLabel: UILabel!
    @IBOutlet weak var imgHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var arrowImage: UIImageView!
    @IBOutlet weak var textviewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var textview: UITextView!
    var cellIndex:Int = 0
    var isExpanded:Bool = false
        {
        didSet
        {
            if !isExpanded {
                self.imgHeightConstraint.constant = 0.0
                self.textviewHeightConstraint.constant = 0.0
            } else {
                
                if (Constants.device == "iPhone"){
                    self.imgHeightConstraint.constant = 400.0
                }else{
                    self.imgHeightConstraint.constant = (Constants.screenSize.height) - 400
                    //self.imgHeightConstraint.constant = 400.0
                }
                
            switch cellIndex {
                case 0:
                    self.textviewHeightConstraint.constant = 300.0
                    break
                case 1:
                    if (Constants.device == "iPhone"){
                        self.textviewHeightConstraint.constant = 450.0
                    }else{
                       // self.textviewHeightConstraint.constant = (Constants.screenSize.height) - 400
                       self.textviewHeightConstraint.constant =  500.0
                    }
                    break
                case 2:
                    if (Constants.device == "iPhone"){
                        self.textviewHeightConstraint.constant = 1250.0
                    }else{
                        // self.textviewHeightConstraint.constant = (Constants.screenSize.height) - 400
                        self.textviewHeightConstraint.constant =  1300.0
                    }
                    break
                case 3:
                    if (Constants.device == "iPhone"){
                        self.textviewHeightConstraint.constant = 200.0
                    }else{
                        // self.textviewHeightConstraint.constant = (Constants.screenSize.height) - 400
                        self.textviewHeightConstraint.constant =  230.0
                    }

                    
                    break
                case 4:
                    if (Constants.device == "iPhone"){
                        self.textviewHeightConstraint.constant = 500.0
                    }else{
                        // self.textviewHeightConstraint.constant = (Constants.screenSize.height) - 400
                        self.textviewHeightConstraint.constant =  530.0
                    }
                    
                    break
                case 5:
                    if (Constants.device == "iPhone"){
                        self.textviewHeightConstraint.constant = 200.0
                    }else{
                        // self.textviewHeightConstraint.constant = (Constants.screenSize.height) - 400
                        self.textviewHeightConstraint.constant =  250.0
                    }
                    
                    break
                case 6:
                    if (Constants.device == "iPhone"){
                        self.textviewHeightConstraint.constant = 480.0
                    }else{
                        // self.textviewHeightConstraint.constant = (Constants.screenSize.height) - 400
                        self.textviewHeightConstraint.constant = 500.0
                    }
                    
                    break
                case 7:
                    if (Constants.device == "iPhone"){
                        self.textviewHeightConstraint.constant = 100.0
                    }else{
                        // self.textviewHeightConstraint.constant = (Constants.screenSize.height) - 400
                        self.textviewHeightConstraint.constant =  130.0
                    }
                    
                    break
                case 8:
                    if (Constants.device == "iPhone"){
                        self.textviewHeightConstraint.constant = 200.0
                    }else{
                        // self.textviewHeightConstraint.constant = (Constants.screenSize.height) - 400
                        self.textviewHeightConstraint.constant =  230.0
                    }
                    
                    break
                case 9:
                    if (Constants.device == "iPhone"){
                        self.textviewHeightConstraint.constant = 400.0
                    }else{
                        // self.textviewHeightConstraint.constant = (Constants.screenSize.height) - 400
                        self.textviewHeightConstraint.constant =  410.0
                    }
                    
                    break
                case 10:
                    if (Constants.device == "iPhone"){
                        self.textviewHeightConstraint.constant = 550.0
                    }else{
                        // self.textviewHeightConstraint.constant = (Constants.screenSize.height) - 400
                        self.textviewHeightConstraint.constant =  590.0
                    }
                    
                    break
                case 11:
                    if (Constants.device == "iPhone"){
                        self.textviewHeightConstraint.constant = 250.0
                    }else{
                        // self.textviewHeightConstraint.constant = (Constants.screenSize.height) - 400
                        self.textviewHeightConstraint.constant =  280.0
                    }
                    
                    break
                case 12:
                    if (Constants.device == "iPhone"){
                        self.textviewHeightConstraint.constant = 100.0
                    }else{
                        // self.textviewHeightConstraint.constant = (Constants.screenSize.height) - 400
                        self.textviewHeightConstraint.constant =  130.0
                    }
                    
                    break
                case 13:
                    if (Constants.device == "iPhone"){
                        self.textviewHeightConstraint.constant = 300.0
                    }else{
                        self.textviewHeightConstraint.constant =  330.0
                    }
                    
                    break
                case 14:
                    if (Constants.device == "iPhone"){
                        self.textviewHeightConstraint.constant = 100.0
                    }else{
                        self.textviewHeightConstraint.constant =  140.0
                    }
                    
                    break
                default:
                    break
                }
                
                
            }
        }
    }
    
}
