//
//  Module1IntroPopupsController.swift
//  6one5Manager
//
//  Created by Sumit More on 12/20/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation
import UIKit

class Module1IntroPopupsController: UIViewController {
    
    @IBOutlet weak var questionsExplanationLbl: UILabel!
    @IBOutlet weak var questionLbl: UILabel!
    @IBOutlet weak var bottomBar: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        let introductionScreenView = self.navigationController!.viewControllers[0] as! Module1IntroController
        switch introductionScreenView.bulbNo {
        case 1:
            self.questionLbl.text = "Mgr_Intro_M1_B1".localized()
            self.questionsExplanationLbl.text = "Mgr_Intro_M1_B1_Explanation".localized()
            self.bottomBar.backgroundColor = UIColor.init(red: 255.0/255.0, green: 90.0/255.0, blue: 48.0/255.0, alpha: 1.0)
            break
        case 2:
            self.questionLbl.text = "Mgr_Intro_M1_B2".localized()
            self.questionsExplanationLbl.text = "Mgr_Intro_M1_B2_Explanation".localized()
            self.bottomBar.backgroundColor = UIColor.init(red: 247.0/255.0, green: 165.0/255.0, blue: 1.0/255.0, alpha: 1.0)
            break
        case 3:
            self.questionLbl.text = "Mgr_Intro_M1_B3".localized()
            self.questionsExplanationLbl.text = "Mgr_Intro_M1_B3_Explanation".localized()
            self.bottomBar.backgroundColor = UIColor.init(red: 155.0/255.0, green: 201.0/255.0, blue: 33.0/255.0, alpha: 1.0)
            break
        case 4:
            self.questionLbl.text = "Mgr_Intro_M1_B4".localized()
            self.questionsExplanationLbl.text = "Mgr_Intro_M1_B4_Explanation".localized()
            self.bottomBar.backgroundColor = UIColor.init(red: 13.0/255.0, green: 174.0/255.0, blue: 172.0/255.0, alpha: 1.0)
            break
        default:
            break
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func closeClick(_ sender: AnyObject) {
        self.view.removeFromSuperview()
    }
}
