//
//  ReviewNotificationData.swift
//  6one5Manager
//
//  Created by enyotalearning on 03/10/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation
public class ReviewNotificationData: NSObject
{
    
    var reiviewDetailsArray = [ReviewDetails]()
    var currentUerId:String!
    var message:String!
    var status:String!
    
    class var sharedInstance: ReviewNotificationData
    {
        //2
        struct Singleton {
            //3
            static let instance = ReviewNotificationData()
        }
        //4
        return Singleton.instance
    }
    
    func addReviewDetails(reviewDetails:ReviewDetails)
    {
        reiviewDetailsArray.append(reviewDetails)
    }
    
    func getreiviewDetailsCount() -> Int{
        return reiviewDetailsArray.count
    }
    
    func getUserName(index:Int) ->String
    {
        let rv:ReviewDetails = reiviewDetailsArray[index];
        
        return rv.userName
    }
    
    func getModuleName(index:Int) ->String
    {
        let rv:ReviewDetails = reiviewDetailsArray[index];
        
        return rv.moduleName
    }
    func getId(index:Int) -> String
    {
        let rv:ReviewDetails = reiviewDetailsArray[index];
        
        return rv.userId
    }
    
    func setCureentUserId(id:String)
    {
        currentUerId = id
    }
    
    func getCureentReviewData(index:Int) -> ReviewDetails
    {
        return reiviewDetailsArray[index];
    }
    /*func desc()
     {
     let count = reiviewDetailsArray.count - 1
     for index in 0...
     {
     let st = getModuleName(index: index)
     print (" *** user name \(st)")
     }
     }*/
    
}
