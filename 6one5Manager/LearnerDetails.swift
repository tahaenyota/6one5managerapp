//
//  LearnerDetails.swift
//  6one5Manager
//
//  Created by enyotalearning on 01/10/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation
public class LearnerDetails: NSObject
{
    
    var learnerProfileArray = [String]()
    var learnerId = [String]()
    var selectedLearner:Int!
    var performanceData = [IndividualPerformanceData]()
    var selectedLearnerId:String!
    var navigationPath:String!
    var notificationIndex:Int!
    var message:String!
    var status:String!
    
    
    class var sharedInstance: LearnerDetails
    {
        //2
        struct Singleton {
            //3
            static let instance = LearnerDetails()
        }
        //4
        return Singleton.instance
    }
    
    
    func addPerformance(object:IndividualPerformanceData)
    {
        performanceData.append(object)
    }
    
    
    
    func getPerformanceDataAtIndex(index:Int) -> IndividualPerformanceData
    {
        
        print("Perfromance data array count \(performanceData.count)")
        
        /*for index in 0...performanceData.count - 1
         {
         performanceData[index].quizDataPoints.count
         }*/
        
        
        let currentLearnerId = learnerId[index]
        let per = performanceData[getIndexforLearnerid(lId: currentLearnerId)]
        print("Data for sss \(per.moduleStatusValues.count)")
        return performanceData[getIndexforLearnerid(lId: currentLearnerId)]
    }
    
    func addLearnerProfile(learnerProfile:String, lId:String)
    {
        learnerProfileArray.append(learnerProfile)
        learnerId.append(lId)
    }
    
    func getLearnerCount() -> Int
    {
        return learnerProfileArray.count
    }
    
    func getLearnerProfile(index:Int) -> String
    {
        return learnerProfileArray[index]
    }
    
    func getLearnerId(index:Int) -> String
    {
        return learnerId[index]
    }
    
    func setSelectedLearner(index:Int)
    {
        selectedLearner = index
    }
    
    func setSelectedLearnerId(id:String)
    {
        selectedLearnerId = id
    }
    
    func getSelectedLearner() -> Int{
        return selectedLearner
    }
    
    
    func printLearnerid()
    {
        print("Learner id count \(learnerId.count)")
        
        for i in 0 ... learnerId.count-1
        {
            print("Learner id : ----->   --  \(learnerId[i]) ")
            
        }
    }
    func getIndexforLearnerid(lId:String) -> Int
    {
        print("!!!! Learner id is \(lId)")
        for index in 0...learnerId.count - 1
        {
            print("!!!! Learner id is \(lId) == \(learnerId[index])")
            if learnerId[index] == lId
            {
                return index
            }
        }
        
        return -1;
    }
}
